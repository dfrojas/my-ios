//
//  EventTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 9/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class EventTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageCell        : UIImageView!
    @IBOutlet weak var name             : UILabel!
    @IBOutlet weak var dateTime         : UILabel!
    @IBOutlet weak var eventDescription : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
