//
//  Commonds.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import Alamofire
import SystemConfiguration
import FirebaseInstanceID

class Commonds {
    static let URL = "https://approachevents.herokuapp.com/api"
    
    
    
    class func showMessage(msj: String, title: String, content: AnyObject) {
        let alert = UIAlertController(title: title, message: msj, preferredStyle: .Alert)
        let action = UIAlertAction(title: "ok", style: .Default){
            _ in
            alert.navigationController?.popViewControllerAnimated(true)
        }
        
        alert.addAction(action)
        content.presentViewController(alert, animated: true, completion: nil)
    }
    
    /*class func downloadImage(url: String)-> Request{
        return Alamofire.request(.GET, url)
    }*/
    
    class func getIdEvent() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.objectForKey("id_event") != nil {
            return defaults.objectForKey("id_event") as! Int
        }
        return 0
    }
    
    // Notificaioton badge
    
    class func setNotificationMessages(param: Int, plus: Bool){
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if plus == true {
            let totalCountMessagesNotifications = Commonds.getNotificationMessages() + param
            defaults.setObject(totalCountMessagesNotifications, forKey: "not_Messages")
            
            UIApplication.sharedApplication().applicationIconBadgeNumber = Commonds.getNotificationMessages() + Commonds.getNotification() + Commonds.getNotificationMatch()
            
            
        }else {
            if param >= UIApplication.sharedApplication().applicationIconBadgeNumber{
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                defaults.setObject(0, forKey: "not_Messages")
            }else {
                let currentNotificationsMessages = Commonds.getNotificationMessages()
                
                if param > currentNotificationsMessages {
                    defaults.setObject(0, forKey: "not_Messages")
                }else {
                    defaults.setObject(currentNotificationsMessages - param, forKey: "not_Messages")
                }
                UIApplication.sharedApplication().applicationIconBadgeNumber = Commonds.getNotificationMessages() + Commonds.getNotification() + Commonds.getNotificationMatch()
            }
        }
    }
    
    class func getNotificationMessages() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let num = defaults.objectForKey("not_Messages") as? Int {
            
            if num <= 0 {
                defaults.setObject(0, forKey: "not_Messages")
                return 0
            }
            return num
        }else {
            defaults.setObject(0, forKey: "not_Messages")
        }
        return 0
    }
    
    
    class func setNotificationMatch(param: Int){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(param, forKey: "not_match")
    }
    
    class func getNotificationMatch() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if defaults.objectForKey("not_match") != nil {
            return defaults.objectForKey("not_match") as! Int
        }
        return 0
    }
    
    class func setNotification(param: Int, plus: Bool){
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if plus == true {
            let result = Commonds.getNotification() + param
            defaults.setObject(result, forKey: "notitifications")
            
            UIApplication.sharedApplication().applicationIconBadgeNumber = Commonds.getNotificationMessages() + Commonds.getNotification() + Commonds.getNotificationMatch()
            
        }else {
            if param >= UIApplication.sharedApplication().applicationIconBadgeNumber{
                UIApplication.sharedApplication().applicationIconBadgeNumber = 0
                defaults.setObject(0, forKey: "notitifications")
            }else {
                
                let currentNotificationsMessages = Commonds.getNotification()
                if param > currentNotificationsMessages {
                    defaults.setObject(0, forKey: "notitifications")
                }else {
                    defaults.setObject(currentNotificationsMessages - param, forKey: "notitifications")
                }
                
                UIApplication.sharedApplication().applicationIconBadgeNumber = Commonds.getNotificationMessages() + Commonds.getNotification() + Commonds.getNotificationMatch()
            }
        }
    }
    
    class func getNotification() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let num = defaults.objectForKey("notitifications") as? Int {
            
            if num <= 0 {
                defaults.setObject(0, forKey: "notitifications")
                return 0
            }
            
            return num
        }else {
            defaults.setObject(0, forKey: "notitifications")
        }
        return 0
    }
    
    // end notificaitons
    
    class func getIdAgenda() -> Int {
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("id_agenda") != nil {
            return defaults.objectForKey("id_agenda") as! Int
        }
        return 0
    }
    
    class func getNameEvent() -> String? {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let ne = defaults.objectForKey("name_event") as? String{
            return ne
        }
        return nil
    }
    
    class func getFechaVidaChat() -> String? {
        let defaults = NSUserDefaults.standardUserDefaults()
        if let fvc = defaults.objectForKey("fecha_vida_chat") as? String {
            return fvc
        }
        return nil
    }
    
    class func getUser() -> User? {
        if let item = NSKeyedUnarchiver.unarchiveObjectWithFile(User.fileUrl.path!), resUser =  item as? User{
            if (resUser.email != nil && resUser.id != nil){
                return resUser
            }
        }
        return nil
    }
    
    class func loading() -> UIAlertController{
        let alert = UIAlertController(title: nil, message: "Por favor espere...", preferredStyle: .Alert)
        
        alert.view.tintColor = UIColor.blackColor()
        let loadingIndicator: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRectMake(10, 5, 50, 50)) as UIActivityIndicatorView
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.Gray
        loadingIndicator.startAnimating();
        alert.view.addSubview(loadingIndicator)
        return alert
    }
    
    class func closeAlert(alert:UIAlertController) {
        alert.dismissViewControllerAnimated(false, completion: nil)
    }
    
    class func formateDate(pDate: NSDate, format: String) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter.stringFromDate(pDate)
    }
    
    class func parseDate(string: String, format: String) -> NSDate?{
        let parse = NSDateFormatter()
        parse.dateFormat = format
        return parse.dateFromString(string)
    }
    
    class func md5(string string: String) -> String {
        var digest = [UInt8](count: Int(CC_MD5_DIGEST_LENGTH), repeatedValue: 0)
        if let data = string.dataUsingEncoding(NSUTF8StringEncoding) {
            CC_MD5(data.bytes, CC_LONG(data.length), &digest)
        }
        
        var digestHex = ""
        for index in 0..<Int(CC_MD5_DIGEST_LENGTH) {
            digestHex += String(format: "%02x", digest[index])
        }
        return digestHex
    }
    
    class func getToken() -> String? {
        
        if let refreshedToken = FIRInstanceID.instanceID().token(){
            return refreshedToken
        }
        return nil
    }
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(sizeofValue(zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(&zeroAddress) {
            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}