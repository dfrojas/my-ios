//
//  TableEventViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class TableEventViewController: UIViewController{
    
    // ------------------------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------------------------
    
    let refresController        = UIRefreshControl()
    
    // ------------------------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------------------------
    
    @IBOutlet weak var constraintHeigtBanner: NSLayoutConstraint!
    @IBOutlet weak var eventTable: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var sections                = ["", "EVENTOS PASADOS"]
    var event                   = Event()
    var events                  = [Event]()
    var eventsOld               = [Event]()
    var eventsNews              = [Event]()
    var filteredEventsOld       = [Event]()
    var filteredEventsNews      = [Event]()
    var resultSearchController  = UISearchController()
    var statePerfil             = false
    var itemSelected:Event?
    var alert       :UIAlertController?
    
    
    // ------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------

    override func viewDidLoad() {
        super.viewDidLoad()
        
    
        self.refresController.backgroundColor = UIColor.whiteColor()
        
        self.navigationController?.navigationBarHidden = false
        self.resultSearchController                    = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.hidesNavigationBarDuringPresentation = false
        self.resultSearchController.searchBar.placeholder = "Buscar"
        definesPresentationContext = true
        self.resultSearchController.searchBar.setValue("Cancelar", forKey: "_cancelButtonText")
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.backgroundColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.sizeToFit()
        self.eventTable.tableHeaderView = self.resultSearchController.searchBar
        
        self.eventTable.separatorColor = UIColor(rgba: "#d3d3d3")
        self.view.backgroundColor =  UIColor.whiteColor()
        
        self.eventTable.layoutMargins = UIEdgeInsetsZero
        //self.eventTable.separatorInset = UIEdgeInsetsZero
        self.eventTable.backgroundColor = UIColor.whiteColor()
        
        refresController.addTarget(self, action: #selector(loadInfo), forControlEvents: .ValueChanged)
        self.eventTable.addSubview(refresController)
        
        self.indicator.startAnimating()
        self.eventTable.hidden = false
        loadInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden =  true
        rotated()
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func rotated(){
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
            UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        }
    }
    
    func loadInfo(){
        FactoryEvent.events().responseJSON { (response) in
            if let JSON = response.result.value, data = JSON as? [Dictionary<String, AnyObject>]{
                self.indicator.stopAnimating()
                self.refresController.endRefreshing()
                
                self.events = self.event.addItems(data)
                self.eventsOld = self.events.filter({ (pEvent) -> Bool in
                    if let fecha_fin = self.parseDate(pEvent.fecha_fin!){
                        return fecha_fin.compare(NSDate()) == NSComparisonResult.OrderedAscending
                    }else {
                        return false
                    }
                })
                
                self.eventsNews = self.events.filter({ (pEvent) -> Bool in
                    if let fecha_fin = self.parseDate(pEvent.fecha_fin!){
                        return fecha_fin.compare(NSDate()) == NSComparisonResult.OrderedDescending
                    }else {
                        return false
                    }
                })
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.eventTable.hidden = false
                    self.eventTable.reloadData()
                })
                
            }else {
                self.refresController.endRefreshing()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.indicator.stopAnimating()
                    Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
                    self.eventTable.reloadData()
                })
            }
        }
    }
    
    func parseDate(string: String) -> NSDate?{
        let parse = NSDateFormatter()
        parse.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        parse.dateFormat = "MMMM d, yyyy"
        return parse.dateFromString(string)
    }
    
    func getPin() {
        let alertController = UIAlertController(title: self.itemSelected?.nombre_evento!, message: "Ingrese la contraseña para entrar a este evento", preferredStyle: .Alert)
        let updateAction = UIAlertAction(title: "Ingresar", style: .Default) { (action) in
            
            let txtIn = alertController.textFields![0] as UITextField
            
            if let pin = txtIn.text {
                if Commonds.md5(string: pin) == self.itemSelected?.pin! {
                    let defaults = NSUserDefaults.standardUserDefaults()
                    defaults.setObject(self.itemSelected?.id!, forKey: "id_event")
                    defaults.setObject(self.itemSelected?.nombre_evento, forKey: "name_event")
                    defaults.setObject(self.itemSelected?.fecha_vida_chat, forKey: "fecha_vida_chat")
                    self.verificationSession({
                        self.statePerfil = false
                        self.chageHome()
                    }, andFailure: {
                        self.performSegueWithIdentifier("sw_login", sender: self)
                    })
                }else {
                    Commonds.showMessage("Contraseña incorrecta", title: "Lo sentimos", content: self)
                }
            }
        }
        
        updateAction.enabled = false
        
        alertController.addTextFieldWithConfigurationHandler { (textField) in
            textField.placeholder = "Ingresar contraseña"
            textField.secureTextEntry = true
            NSNotificationCenter
                .defaultCenter()
                .addObserverForName(UITextFieldTextDidChangeNotification, object: textField, queue: NSOperationQueue.mainQueue()) { (notification) in
                    updateAction.enabled = textField.text != ""
            }
        }
        
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (_) in }
        alertController.addAction(updateAction)
        alertController.addAction(cancelAction)
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    func verificationSession(successBlock: () -> (), andFailure failureBlock: () -> ()) {
        if let user = Commonds.getUser() where user.evento == Commonds.getIdEvent(){
            successBlock()
            
        } else if let user = Commonds.getUser() {
            self.alert = Commonds.loading();
            
            FactoryUser.isRegister({ (state) in
                
                if state == true{
                    var txt = "nombres=\(user.nombres!)&apellidos=\(user.apellidos!)&email=\(user.email!)&id_evento=\(Commonds.getIdEvent())&platform=ios&contrasena=&fcm_tok="
                    
                    if let fcm = Commonds.getToken() {
                        txt = txt + fcm
                    }
                    
                    if let pphoto = user.foto{
                        txt = txt + "&foto=\(pphoto)"
                    }
                    
                    FactoryUser.register(txt, successBlock: { (user) in
                        Commonds.closeAlert(self.alert!)
                        successBlock()
                        }, andFailure: { (error) in
                            failureBlock()
                    })
                    
                } else {
                    self.statePerfil = true
                    failureBlock()
                }
            }, andFailure: { (error) in
                failureBlock()
            })
        } else {
            failureBlock()
        }
    }
    
    func chageHome(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
            
            mainViewController.view.frame = UIScreen.mainScreen().bounds
            let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
            let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
            
            SlideMenuOptions.contentViewScale = 1
            //SlideMenuOptions.hideStatusBar = false;
            appDelegate.window?.rootViewController = slideMenuController
        })
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: - Navigation
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        let destination = segue.destinationViewController as! UINavigationController
        if let modePayInViewController = destination.topViewController as? ModePayInViewController {
            modePayInViewController.event = self.itemSelected
            modePayInViewController.statePerfil = self.statePerfil
        }
    }
}

extension TableEventViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return sections.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            
            if indexPath.section == 0 {
                itemSelected = self.filteredEventsNews[indexPath.row]
            }
            else if indexPath.section == 1{
                itemSelected = self.filteredEventsOld[indexPath.row]
            }
            self.getPin()
            
        }else if indexPath.section == 0{
            itemSelected = self.eventsNews[indexPath.row]
            self.getPin()
        }
        else if indexPath.section == 1{
            itemSelected = self.eventsOld[indexPath.row]
            self.getPin()
        }
        self.eventTable.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.resultSearchController.active{
            if section == 0{
                return self.filteredEventsNews.count
            }else {
                return self.filteredEventsOld.count
            }
        }
        else if section == 0{
            return self.eventsNews.count
        }else {
            return self.eventsOld.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! EventTableViewCell
        
        cell.layoutMargins = UIEdgeInsetsZero
        
        if indexPath.section == 0 {
            
            var entry = eventsNews[indexPath.row]
            
            if self.resultSearchController.active{
                entry = self.filteredEventsNews[indexPath.row]
            }else {
                entry = self.eventsNews[indexPath.row]
            }
            
            cell.name.text = entry.nombre_evento
            
            if let fi = entry.fecha_inicio,ff = entry.fecha_fin {
                cell.dateTime.text  = "\(fi) - \(ff)"
            }
            
            if let desc = entry.lugar{
                cell.eventDescription.text = desc
            }
            
            
            if let img = entry.avatar{
                cell.imageCell.loadImageUsingCacheWithUrlString(img)
            }
            
            cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
            UIView.animateWithDuration(0.3, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
                },completion: { finished in
                    UIView.animateWithDuration(0.1, animations: {
                        cell.layer.transform = CATransform3DMakeScale(1,1,1)
                    })
            })
            
            return cell
        }else {
            
            var entry = eventsOld[indexPath.row]
            
            if self.resultSearchController.active{
                entry = self.filteredEventsOld[indexPath.row]
            }else {
                entry = self.eventsOld[indexPath.row]
            }
            
            cell.name.text     = entry.nombre_evento
            
            if let fi = entry.fecha_inicio,ff = entry.fecha_fin {
                cell.dateTime.text  = "\(fi) - \(ff)"
            }
            
            if let desc = entry.lugar{
                cell.eventDescription.text = desc
            }
            
            
            if let img = entry.avatar{
                cell.imageCell.loadImageUsingCacheWithUrlString(img)
            }
            
            cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
            UIView.animateWithDuration(0.3, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
                },completion: { finished in
                    UIView.animateWithDuration(0.1, animations: {
                        cell.layer.transform = CATransform3DMakeScale(1,1,1)
                    })
            })
            
            return cell
        }
        
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.eventsOld.count > 0 && !self.resultSearchController.active{
            return self.sections[section]
        } else if self.resultSearchController.active{
            if self.filteredEventsOld.count > 0 {
                return self.sections[section]
            }else {
                return nil
            }
        }
        
        return nil
    }
    
    func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.whiteColor()
        
        
        let title = UILabel()
        title.font = UIFont(name: "Futura", size: 17)!
        title.textColor = UIColor(rgba: "#6F7179")
        
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.font=title.font
        header.textLabel?.textColor=title.textColor
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
}


extension TableEventViewController:  UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredEventsOld = self.eventsOld.filter({ (event) -> Bool in
            let stringMatch = event.nombre_evento!.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            
            if let fecha_fin = self.parseDate(event.fecha_fin!){
                return fecha_fin.compare(NSDate()) == NSComparisonResult.OrderedAscending && stringMatch != nil
            }else {
                return false
            }
        })
        
        self.filteredEventsNews = self.eventsNews.filter({ (event) -> Bool in
            let stringMatch = event.nombre_evento!.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            
            if let fecha_fin = self.parseDate(event.fecha_fin!){
                return fecha_fin.compare(NSDate()) == NSComparisonResult.OrderedDescending && stringMatch != nil
            }else {
                return false
            }
        })
        self.eventTable.reloadData()
    }
}
