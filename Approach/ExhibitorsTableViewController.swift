//
//  ExhibitorsTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class ExhibitorsTableViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var tableViewConstraintTop: NSLayoutConstraint!
    
    let refresController = UIRefreshControl()
    var resultSearchController  = UISearchController()
    var exhibitor               = Exhibitor()
    var exhibitors              = [Exhibitor]()
    var filteredExhibitor       = [Exhibitor]()
    var sponsors:[Sponsor]      = []
    var itemSelected:Exhibitor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresController.backgroundColor = UIColor.whiteColor()
        buttonNotifications()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.buttonNotifications),name: "reload-notifications", object: nil)

        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.searchBar.placeholder = "Buscar"
        definesPresentationContext = true
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.translucent = true
        //self.resultSearchController.searchBar.barTintColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.resultSearchController.searchBar
        self.resultSearchController.searchBar.setValue("Cancelar", forKey: "_cancelButtonText")
        self.refresController.addTarget(self, action: #selector(self.load), forControlEvents: .ValueChanged)
        self.tableView.addSubview(refresController)
        load()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        buttonNotifications()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        if self.resultSearchController.active == true{
            self.resultSearchController.active = !self.resultSearchController.active
        }
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func load() {
        FactoryExhiborts.exhiborts("\(Commonds.getIdEvent())").responseJSON { (response) in
            self.refresController.endRefreshing()
            if let JSON = response.result.value, data = JSON as? [Dictionary<String, AnyObject>]{
                self.exhibitors = self.exhibitor.addItems(data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                    
                    let cells = self.tableView.visibleCells
                    let tableHeight: CGFloat = self.tableView.bounds.size.height
                    
                    for i in cells {
                        let cell: UITableViewCell = i as UITableViewCell
                        cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
                    }
                    
                    var index = 0
                    
                    for a in cells {
                        let cell: UITableViewCell = a as UITableViewCell
                        UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionNone, animations: {
                            cell.transform = CGAffineTransformMakeTranslation(0, 0);
                            }, completion: nil)
                        
                        index += 1
                    }
                })
                
            }else {
                Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
            }
        }
        
        FactorySponsor.getSponsors({ (sponsors) in
            if sponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
                self.sponsors = sponsors
                self.collectionSponsor.reloadData()
            }else {
                self.collectionConstraintBottom.constant = -75
            }
        }) { (error) in
            Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
        }
    }
    
    func buttonNotifications(){
        let badgeButton : MIBadgeButton = MIBadgeButton(frame: CGRectMake(0, 0, 20, 20))
        badgeButton.setImage(UIImage(named: "noti_24"), forState: .Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        let num = Commonds.getNotification()
        
        if num > 0{
            badgeButton.badgeString = "\(num)";
        }else {
            badgeButton.badgeString = nil
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.actionNotifications))
        badgeButton.userInteractionEnabled = true
        badgeButton.addGestureRecognizer(tapGestureRecognizer3)
    }

    
    func actionNotifications() {
        Commonds.setNotification(Commonds.getNotification(), plus: false)
        self.slideMenuController()?.openRight()
        self.buttonNotifications()
    }
    
    // -------------------------------------------------------------
    // MARK: - Nagivation
    // -------------------------------------------------------------
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let exhibitorVC = segue.destinationViewController as? ExhibitorViewController {
            exhibitorVC.exhibitor = self.itemSelected
        }
    }
    
}

extension ExhibitorsTableViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            itemSelected = self.filteredExhibitor[indexPath.row]
        }else {
            itemSelected = self.exhibitors[indexPath.row]
        }
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("sw_exhibitor", sender: self)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.resultSearchController.active{
            return filteredExhibitor.count
        }else {
            return exhibitors.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ExhibitorTableViewCell
        var entry = exhibitors[indexPath.row]
        
        if self.resultSearchController.active{
            entry = self.filteredExhibitor[indexPath.row]
        }else {
            entry = self.exhibitors[indexPath.row]
        }
        
        cell.nameCell.text = entry.nombre
        
        if let img = entry.foto {
            cell.imgageCell.layer.cornerRadius = cell.imgageCell.frame.size.width / 2;
            cell.imgageCell.clipsToBounds = true;
            cell.imgageCell.loadImageUsingCacheWithUrlString(img)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 58
    }
}

extension ExhibitorsTableViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors[indexPath.row]
        
        if let img = entry.imagen{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors.count
        
        let totalCellWidth = 65 * self.sponsors.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors[indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
    
}

extension ExhibitorsTableViewController: UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredExhibitor = self.exhibitors.filter({ (exhibitor) -> Bool in
            let stringMatch = exhibitor.nombre!.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            return stringMatch != nil
        })
        
        self.tableView.reloadData()
    }
}
