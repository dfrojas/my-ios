//
//  User.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class Person: NSObject, NSCoding{
    
    var id                   :Int?
    var nombres              :String?
    var apellidos            :String?
    var foto                 :String?
    var email                :String?
    var fcm_tok              :String?
    var empresa              :String?
    var clave                :String?
    var logo                 :String?
    var cargo                :String?
    var giro                 :String?
    var telefono             :String?
    var descripcion_servicios:String?
    var idEvento             :Int?
    var idUsuario            :Int?
    var isView               :Bool?
    var like                 :Bool?
    var isFriend             :Bool?
    var linkedin             :String?
    var telefono_2           :String?
    var messages             :[Message] = []
    
    override init(){
        super.init()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        if let cnombres = aDecoder.decodeObjectForKey("nombres") as? String{
            self.nombres = cnombres
        }
        if let cisview = aDecoder.decodeObjectForKey("isView") as? Bool{
            self.isView = cisview
        }
        if let cisfriend = aDecoder.decodeObjectForKey("isFriend") as? Bool{
            self.isFriend = cisfriend
        }
        if let clike = aDecoder.decodeObjectForKey("like") as? Bool{
            self.like = clike
        }
        if let capellidos = aDecoder.decodeObjectForKey("apellidos") as? String {
            self.apellidos = capellidos
        }
        if let cfoto = aDecoder.decodeObjectForKey("foto") as? String {
            self.foto = cfoto
        }
        if let cid = aDecoder.decodeObjectForKey("id") as? Int where cid > 0 {
            self.id = cid
        }
        if let cidusuario = aDecoder.decodeObjectForKey("idUsuario") as? Int where cidusuario > 0 {
            self.idUsuario = cidusuario
        }
        if let cidEvento = aDecoder.decodeObjectForKey("idEvento") as? Int where cidEvento > 0 {
            self.idEvento = cidEvento
        }
        if let cEmail = aDecoder.decodeObjectForKey("email") as? String{
            self.email = cEmail
        }
        if let cfcmTok = aDecoder.decodeObjectForKey("fcm_tok") as? String{
            self.fcm_tok = cfcmTok
        }
        if let cempresa = aDecoder.decodeObjectForKey("empresa") as? String{
            self.empresa = cempresa
        }
        if let cclave = aDecoder.decodeObjectForKey("clave") as? String{
            self.clave = cclave
        }
        if let clogo = aDecoder.decodeObjectForKey("logo") as? String{
            self.logo = clogo
        }
        if let ccargo = aDecoder.decodeObjectForKey("cargo") as? String{
            self.cargo = ccargo
        }
        if let cgiro = aDecoder.decodeObjectForKey("giro") as? String{
            self.giro = cgiro
        }
        if let ctelefono = aDecoder.decodeObjectForKey("telefono") as? String{
            self.telefono = ctelefono
        }
        if let cdescripcion_servicios = aDecoder.decodeObjectForKey("descripcion_servicios") as? String{
            self.descripcion_servicios = cdescripcion_servicios
        }
        if let ctelefono = aDecoder.decodeObjectForKey("telefono_2") as? String{
            self.telefono_2 = ctelefono
        }
        
        if let ctelefono = aDecoder.decodeObjectForKey("linkedin") as? String{
            self.linkedin = ctelefono
        }
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        if let cnombres = self.nombres{
            aCoder.encodeObject(cnombres, forKey: "nombres")
        }
        if let cisview = self.isView{
            aCoder.encodeObject(cisview, forKey: "isView")
        }
        if let clike = self.like{
            aCoder.encodeObject(clike, forKey: "like")
        }
        if let cisfriend = self.isFriend{
            aCoder.encodeObject(cisfriend, forKey: "isFriend")
        }
        if let capellidos = self.apellidos{
            aCoder.encodeObject(capellidos, forKey: "apellidos")
        }
        if let cfoto = self.foto{
            aCoder.encodeObject(cfoto, forKey: "foto")
        }
        if let cid = self.id{
            aCoder.encodeObject(cid, forKey: "id")
        }
        if let cidusuario = self.idUsuario{
            aCoder.encodeObject(cidusuario, forKey: "idUsuario")
        }
        if let cidEvento = self.idEvento{
            aCoder.encodeObject(cidEvento, forKey: "idEvento")
        }
        if let cEmail = self.email{
            aCoder.encodeObject(cEmail, forKey: "email")
        }
        if let cfcmTok = self.fcm_tok{
            aCoder.encodeObject(cfcmTok, forKey: "fcm_tok")
        }
        if let cempresa = self.empresa{
            aCoder.encodeObject(cempresa, forKey: "empresa")
        }
        if let cclave = self.clave{
            aCoder.encodeObject(cclave, forKey: "clave")
        }
        if let clogo = self.logo{
            aCoder.encodeObject(clogo, forKey: "logo")
        }
        if let ccargo = self.cargo{
            aCoder.encodeObject(ccargo, forKey: "cargo")
        }
        if let cgiro = self.giro{
            aCoder.encodeObject(cgiro, forKey: "giro")
        }
        if let ctelefono = self.telefono{
            aCoder.encodeObject(ctelefono, forKey: "telefono")
        }
        if let cdescripcion_servicios = self.descripcion_servicios{
            aCoder.encodeObject(cdescripcion_servicios, forKey: "descripcion_servicios")
        }
        if let ctelefono = self.telefono_2{
            aCoder.encodeObject(ctelefono, forKey: "telefono_2")
        }
        if let ctelefono = self.linkedin{
            aCoder.encodeObject(ctelefono, forKey: "linkedin")
        }
    }
}
