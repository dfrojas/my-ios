//
//  FactoryExhibitors.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactorySpeaker {
    
    class func speakers(idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "conferencistas", idEvent].map{String($0)}.joinWithSeparator("/"))
    }
    
    class func speaker(id:String, idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "conferencista", id, idEvent].map{String($0)}.joinWithSeparator("/"))
    }
    
    class func getSpeakers(successBlock: (speakers: [Speaker]) -> (), andFailure failureBlock: (NSError?) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "conferencistas", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                var nSpeaker = Speaker()
                let pSpeaker = nSpeaker.addItems(resp)
                successBlock(speakers: pSpeaker)
            } else {
                failureBlock(nil)
            }
        }
    }
}