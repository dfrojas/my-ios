//
//  FactoryExhibitors.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryAgenda {
    
    
    class func agends(successBlock: (agendas: [Agenda]) -> (), andFailure failureBlock: (String?) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "agendas", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                let listAgenda = ListAgenda()
                
                successBlock(agendas: listAgenda.saveList(resp))
            } else {
                failureBlock("Ocurrio un error, intenta nuevamente")
            }
        }
    }
    
    class func getAgenda(id:Int, successBlock: (agenda: Agenda) -> (), andFailure failureBlock: (NSError?) -> ()) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "agenda", Commonds.getIdEvent(), id].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                let listAgenda = ListAgenda()
                successBlock(agenda: listAgenda.saveItem(resp[0]))
            } else {
                failureBlock(nil)
            }
        }
    }
    
    
}