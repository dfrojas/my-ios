import UIKit

class Menu: NSObject, NSCoding {
    
    // -------------------------------------------------------------------
    // ATRIBUTOS
    // -------------------------------------------------------------------
    
    var items    : [Int]?
    var idEvent  : Int?
    var id       : Int?
    
    // -------------------------------------------------------------------
    // CONSTRUCTOR
    // -------------------------------------------------------------------
    
    override init(){
        super.init()
    }
    
    // -------------------------------------------------------------------
    // METODS
    // -------------------------------------------------------------------
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        if let cmIdEvent = aDecoder.decodeObjectForKey("idEvent") as? Int{
            self.idEvent = cmIdEvent
        }
        
        if let citems = aDecoder.decodeObjectForKey("items") as? NSArray{
            self.items = citems as? [Int]
        }
        
        if let cId = aDecoder.decodeObjectForKey("id") as? Int{
            self.id = cId
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        if let cmIdEvent = self.idEvent{
            aCoder.encodeObject(cmIdEvent, forKey: "idEvent")
        }
        
        if let citems = self.items{
            aCoder.encodeObject(citems, forKey: "items")
        }
        if let cId = self.id{
            aCoder.encodeObject(cId, forKey: "id")
        }
    }
}
