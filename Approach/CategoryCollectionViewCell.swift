//
//  CategoryCollectionViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 7/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import M13Checkbox

class CategoryCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var check: M13Checkbox!
}
