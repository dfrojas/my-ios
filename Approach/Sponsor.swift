//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Sponsor{
    
    var id:Int?
    var nombre:String?
    var link_facebook:String?
    var link_twitter:String?
    var link_linkedin:String?
    var descripcion:String?
    var tipo_patrocinio:String?
    var imagen:String?
    var evento:Int?
    
    var sponsors:[Sponsor] = []
    var socialNetworks:[SocialNetwork] = []
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Sponsor] {
        
        sponsors = []
        
        for item in items {
            var sponsor               = Sponsor()
            sponsor.id                = item["id"] as? Int
            sponsor.nombre            = item["nombre"] as? String
            sponsor.tipo_patrocinio   = item["tipo_patrocinio"] as? String
            sponsor.imagen            = item["imagen"] as? String
            sponsor.descripcion       = item["descripcion"] as? String
            sponsor.evento            = item["evento"] as? Int
            
            if let array = item["redes_sociales"], pSponsors = array as? [Dictionary<String,AnyObject>]{
                var nSponsor = SocialNetwork()
                sponsor.socialNetworks = nSponsor.addItems(pSponsors)
            }
            self.sponsors.append(sponsor)
        }
        return self.sponsors
    }
    
    func setSponsor(item:Dictionary<String,AnyObject>) -> Sponsor {
        var sponsor               = Sponsor()
        sponsor.id                = item["id"] as? Int
        sponsor.nombre            = item["nombre"] as? String
        sponsor.tipo_patrocinio   = item["tipo_patrocinio"] as? String
        sponsor.imagen            = item["imagen"] as? String
        sponsor.descripcion       = item["descripcion"] as? String
        sponsor.evento            = item["evento"] as? Int
        
        if let array = item["redes_sociales"], pSponsors = array as? [Dictionary<String,AnyObject>]{
            var nSponsor = SocialNetwork()
            sponsor.socialNetworks = nSponsor.addItems(pSponsors)
        }
        
        return sponsor
    }
    
    func getItem(index: Int) -> Sponsor {
        return sponsors[index]
    }
}