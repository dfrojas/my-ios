//
//  AgendaLugarViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 16/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MapKit

class AgendaLugarViewController: UIViewController, MKMapViewDelegate{

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelRoom: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    
    var agenda:Agenda?
    var listAgenda          = ListAgenda()
    var sponsors:[Sponsor]?
    
    
    @IBOutlet weak var buttonFavorite: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupCamera()
        self.indicator.stopAnimating()
        self.load()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.load()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    @IBAction func btnBack(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func actionFavorite(sender: UIBarButtonItem) {
        
        if let agenda = self.agenda{
            
            if let state = agenda.isFavorite {
                agenda.isFavorite = !state
                listAgenda.updateItem(agenda)
                if agenda.isFavorite == true {
                    self.buttonFavorite.image = UIImage(named: "fav_ful_24")
                }else {
                    self.buttonFavorite.image = UIImage(named: "fav_24")
                }
            }
        }
    }
    
    func load(){
        if let pAgenda = agenda {
            self.labelDate.text = pAgenda.fecha
            self.labelTitle.text = pAgenda.titulo
            if let phi = pAgenda.hora_inicio, phf = pAgenda.hora_fin{
                self.labelTime.text = "\(phi) - \(phf)"
            }
            self.labelRoom.text = pAgenda.salon
            
            
            FactoryEvent.getEvent({ (event) in
                
                if let pCoordinates = event.localizacion{
                    self.setupMap(pCoordinates)
                }
                }, andFailure: { (error) in
                    Commonds.showMessage("Ocurrio un error al cargar la ubicación", title: "Error", content: self)
            })
            
            if let nc = self.tabBarController?.viewControllers![0] as? UINavigationController, tab = nc.topViewController as? AgendaInfoViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if let pSponsors = self.sponsors where  pSponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
            }
            
            if let nc = self.tabBarController?.viewControllers![1] as? UINavigationController, tab = nc.topViewController as? AgendaSpeakersViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if pAgenda.isFavorite == true {
                self.buttonFavorite.image = UIImage(named: "fav_ful_24")
            }else {
                self.buttonFavorite.image = UIImage(named: "fav_24")
            }
        }
    }
    
    func setupMap(pCoordinates:String){
        let coordinates = pCoordinates.characters.split{$0 == ","}.map(String.init)
        let location    = CLLocationCoordinate2D(
            latitude    : Double(coordinates[0])!,
            longitude   : Double(coordinates[1])!
        )
        
        let span    = MKCoordinateSpanMake(0.05, 0.05)
        let region  = MKCoordinateRegion(center: location, span: span)
        
        let annotation          = MKPointAnnotation()
        annotation.coordinate   = location
        self.map.setRegion(region, animated: true)
        self.map.addAnnotation(annotation)
        self.map.zoomEnabled = false;
        self.map.scrollEnabled = false;
        self.map.userInteractionEnabled = false;
    }
    
    func setupCamera(){
        map.camera.altitude = 1400
        map.camera.pitch    = 50
        map.camera.heading  = 180
    }
}

extension AgendaLugarViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors![indexPath.row]
        if let img = entry.imagen{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors!.count
        
        let totalCellWidth = 65 * self.sponsors!.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors![indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
}
