//
//  Agenda.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

class Agenda: NSObject, NSCoding{
    
    var id          :Int?
    var fecha       :String?
    var hora_inicio:String?
    var hora_fin   :String?
    var titulo      :String?
    var descripcion :String?
    var salon       :String?
    var evento      :Int?
    var idUsiario   :Int?
    var isFavorite  :Bool?
    var link_encuesta :String?
    var speakers:[Speaker] = []
    
    override init(){
        super.init()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        
        
        if let cid = aDecoder.decodeObjectForKey("id") as? Int where cid > 0 {
            self.id = cid
        }
        
        if let cid = aDecoder.decodeObjectForKey("evento") as? Int where cid > 0 {
            self.evento = cid
        }
        
        if let cid = aDecoder.decodeObjectForKey("isFavorite") as? Bool {
            self.isFavorite = cid
        }
        
        if let cid = aDecoder.decodeObjectForKey("idUsuario") as? Int where cid > 0 {
            self.idUsiario = cid
        }
        
        if let obj = aDecoder.decodeObjectForKey("fecha") as? String{
            self.fecha = obj
        }
        
        if let obj = aDecoder.decodeObjectForKey("hora_fin") as? String{
            self.hora_fin = obj
        }
        
        if let obj = aDecoder.decodeObjectForKey("hora_inicio") as? String{
            self.hora_inicio = obj
        }
        
        if let obj = aDecoder.decodeObjectForKey("titulo") as? String{
            self.titulo = obj
        }
        
        if let obj = aDecoder.decodeObjectForKey("descripcion") as? String{
            self.descripcion = obj
        }
        
        if let obj = aDecoder.decodeObjectForKey("salon") as? String{
            self.salon = obj
        }
        if let obj = aDecoder.decodeObjectForKey("link_encuesta") as? String{
            self.link_encuesta = obj
        }
        
        /*if let obj = aDecoder.decodeObjectForKey("speakers") as? [Speaker]{
         self.speakers = obj
         }*/
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        if let cid = self.id{
            aCoder.encodeObject(cid, forKey: "id")
        }
        
        if let cid = self.isFavorite{
            aCoder.encodeObject(cid, forKey: "isFavorite")
        }
        
        if let cid = self.idUsiario{
            aCoder.encodeObject(cid, forKey: "idUsuario")
        }
        
        if let cid = self.evento{
            aCoder.encodeObject(cid, forKey: "evento")
        }
        
        if let obj = self.fecha{
            aCoder.encodeObject(obj, forKey: "fecha")
        }
        
        if let obj = self.hora_inicio{
            aCoder.encodeObject(obj, forKey: "hora_inicio")
        }
        
        if let obj = self.hora_fin{
            aCoder.encodeObject(obj, forKey: "hora_fin")
        }
        
        if let obj = self.titulo{
            aCoder.encodeObject(obj, forKey: "titulo")
        }
        
        if let obj = self.descripcion{
            aCoder.encodeObject(obj, forKey: "descripcion")
        }
        
        if let obj = self.salon{
            aCoder.encodeObject(obj, forKey: "salon")
        }
        
        if let obj = self.link_encuesta{
            aCoder.encodeObject(obj, forKey: "link_encuesta")
        }
        
        /*if let obj = self.speakers{
         aCoder.encodeObject(obj, forKey: "speakers")
         aCoder.e
         }*/
    }
}