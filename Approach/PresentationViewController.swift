//
//  PresentationViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 12/09/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class PresentationViewController: UIViewController{
    
    
    @IBOutlet weak var btnClose: UIButton!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var images:[String] = ["intro_1", "intro_2", "intro_3"]
    var textos:[String] = ["Bienvenido a tu red de Networking. Conoce a los asistentes de este evento y elige con quien deseas conectarte", "Si a alguien le diste like, te da like...", "Chatea con tus nuevos contactos y genera nuevas oportunidades"]
    
    
    
    var userLogin = Commonds.getUser()
    var idEvento = Commonds.getIdEvent()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.btnClose.layer.cornerRadius = self.btnClose.frame.size.width / 2;
        self.btnClose.clipsToBounds = true;
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        rotated()
        self.navigationController?.navigationBar.hidden = true
        UIApplication.sharedApplication().statusBarHidden = true
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool) {
        UIApplication.sharedApplication().statusBarHidden = false
        if let id = self.userLogin?.id where idEvento > 0{
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(id, forKey: "relation_\(id)_\(idEvento)")
        }
        NSNotificationCenter.defaultCenter().postNotificationName("open_relationship", object: nil)
    }
    
    
    @IBAction func actionClose(sender: UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func rotated(){
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
            UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}



extension PresentationViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! PresentationCollectionViewCell
        
        let entry = self.images[indexPath.row]
        let entry2 = self.textos[indexPath.row]
        
        cell.labTexto.text = entry2
        cell.imageView.image = UIImage(named: entry)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake(view.frame.width, view.frame.height)
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
    }
    
}