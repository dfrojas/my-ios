//
//  LeftMenuTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 17/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelNombre: UILabel!
    @IBOutlet weak var labelCargo: UILabel!
    @IBOutlet weak var imgSetting: UIImageView?

    @IBOutlet weak var imageConfig: UIImageView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var labNombreEvento: UILabel!
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
