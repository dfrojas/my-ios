//
//  RegisterViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class RegisterViewController: UIViewController, UINavigationControllerDelegate {

    var event:Event?
    var window: UIWindow?
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var labTerms: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.changeWindow),name: "change-window", object: nil)
        self.addGesture()
        self.indicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func btnRegister(sender: UIButton) {
        
        if let email = txtEmail.text, name = txtName.text, lastName = txtLastName.text, clave = txtPassword.text where lastName.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" && email.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" && name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" && clave.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != ""{
            
            self.indicator.startAnimating()
            
            var txt = "nombres=\(name)&apellidos=\(lastName)&email=\(email)&id_evento=\(Commonds.getIdEvent())&platform=ios&contrasena=\(clave)&foto=&fcm_tok="
            
            if let fcm = Commonds.getToken() {
                txt = txt + fcm
            }
            
            FactoryUser.register(txt, successBlock: { (user) in
                
                self.indicator.stopAnimating()
                
                self.txtName.resignFirstResponder()
                self.txtLastName.resignFirstResponder()
                self.txtEmail.resignFirstResponder()
                self.txtPassword.resignFirstResponder()
                
                
                if Commonds.getUser() != nil {
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        self.performSegueWithIdentifier("sw_complete_p3", sender: self)
                    })
                }else {
                    Commonds.showMessage("El email no esta disponible", title: "Lo sentimos", content: self)
                }
                
                }, andFailure: { (error) in
                    self.indicator.stopAnimating()
                    Commonds.showMessage("Ocurrio un error intenta nuevamente", title: "Lo sentimos", content: self)
            })
        }else {
            Commonds.showMessage("Por favor ingresa todos los campos", title: "Espera un momento!", content: self)
            
        }
    }
    
    func navigationControllerSupportedInterfaceOrientations(navigationController: UINavigationController) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.Portrait
    }
    
    func actionOpenTerms(){
        self.performSegueWithIdentifier("sw_terms_2", sender: self)
    }
    
    func addGesture(){
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.addTarget(self, action: #selector(self.actionOpenTerms))
        self.labTerms.userInteractionEnabled = true
        self.labTerms.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
    func changeWindow(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
        
        //self.dismissViewControllerAnimated(true, completion: nil)
        
        mainViewController.view.frame = UIScreen.mainScreen().bounds
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
        let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        
        SlideMenuOptions.contentViewScale = 1
        //SlideMenuOptions.hideStatusBar = false;
        appDelegate.window?.rootViewController = slideMenuController
        
    }

    /*
    // ------------------------------------------------------------------------------------
    // MARK: - Navigation
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let homeViewController = segue.destinationViewController as? HomeViewController{
            homeViewController.event = self.event
        }
    }*/

}


