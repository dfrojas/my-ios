//
//  RightMenuTableCollectionViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class RightMenuTableCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout{
    
    var notifications:[Notification] = []
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: #selector(actionNotification), userInfo: nil, repeats: false)
        
        self.navigationController?.navigationBar.hidden = true
        self.load()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func actionNotification(){
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.load),name: "reload-notifications", object: nil)
    }
    
    func load() {
        FactoryNotification.getNotifications({ (notifications) in
            self.notifications = notifications
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.collectionView?.reloadData()
            })
            
            }) { (error) in
                
        }
    }


    // MARK: UICollectionViewDataSource

    override func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        let headerView =
            collectionView.dequeueReusableSupplementaryViewOfKind(kind,withReuseIdentifier: "CellHeader", forIndexPath: indexPath)
        return headerView
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return notifications.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! NotificationCollectionViewCell
        let item = notifications[indexPath.row]
        cell.notification = item
        return cell
    }
    
    /*func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let item = notifications[indexPath.row]
        
        if let messageText = item.texto {
            let size            = CGSizeMake(250, 1000)
            let options         = NSStringDrawingOptions.UsesFontLeading.union(.UsesLineFragmentOrigin)
            let estimatedFrame  = NSString(string: messageText).boundingRectWithSize(size, options: options, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13)], context: nil)
            return CGSizeMake(view.frame.width, estimatedFrame.height + 22)
        }
        return CGSizeMake(view.frame.width, 15)
    }*/
    
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let item = notifications[indexPath.row]
        
        var height: CGFloat = 100
        
        if let text = item.texto {
            height = estimateFrameForText(text).height + 20
        }
        
        let width = view.frame.width
        return CGSize(width: width, height: height)
    }
    
    
    func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.UsesFontLeading.union(.UsesLineFragmentOrigin)
        return NSString(string: text).boundingRectWithSize(size, options: options, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(13)], context: nil)
    }
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(collectionView: UICollectionView, shouldHighlightItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(collectionView: UICollectionView, shouldSelectItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(collectionView: UICollectionView, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, canPerformAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) -> Bool {
        return false
    }

    override func collectionView(collectionView: UICollectionView, performAction action: Selector, forItemAtIndexPath indexPath: NSIndexPath, withSender sender: AnyObject?) {
    
    }
    */

}
