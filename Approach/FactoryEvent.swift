//
//  FactoryEvent.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryEvent {
    
    class func events() -> Request{
        
        return Alamofire.request(.GET, [Commonds.URL, "eventos/"].map{String($0)}.joinWithSeparator("/"))
    }
    
    class func event(id:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "evento", id].map{String($0)}.joinWithSeparator("/"))
    }
    
    
    class func getEvent(successBlock: (event: Event) -> (), andFailure failureBlock: (String) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "evento", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                let pEvent = resp[0]
                var nEvent = Event()
                nEvent     = nEvent.setEvent(pEvent)
                successBlock(event: nEvent)
            } else {
                failureBlock("Ocurrio un error intenta nuevamente")
            }
        }
    }
    
    class func menuActive(listMenu: ListMenu, successBlock: (menu: Menu) -> (), andFailure failureBlock: (String) -> ()){
        Alamofire.request(.GET, [Commonds.URL, "menu", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>]{
                successBlock(menu: listMenu.saveList(resp))
            } else {
                failureBlock("Ocurrio un error intenta nuevamente")
            }
        }
    }
}
