//
//  PersonList.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 18/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

class ListPerson: NSObject {
    
    // ------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------
    
    private var person:Person?
    
    private let fileUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("PersonList.plist")
    
    // ------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------
    
    var items:[Person] = []
    var userLogin = Commonds.getUser()
    
    // ------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------
    
    override init() {
        super.init()
        self.loadItemsOnStart()
    }
    
    // ------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------
    
    // agregr item
    func addItem(pitem:Person){
        var existe = false
        for (index,item) in self.items.enumerate() {
            if item.idEvento == pitem.idEvento && pitem.id == item.id && pitem.idUsuario! == item.idUsuario!{
                self.items[index] = pitem
                persistenceItems()
                existe = true
                break
            }
        }
        
        if existe == false {
            items.append(pitem)
            persistenceItems()
        }
    }
    
    func addItem2(pitem:Person){
        items.append(pitem)
        persistenceItems()
    }
    
    // almacenar items
    func persistenceItems()  {
        
        let itemsArray = items as NSArray
        NSKeyedArchiver.archiveRootObject(itemsArray, toFile: self.fileUrl.path!)
    }
    
    // Cargar items al iniciar
    func loadItemsOnStart() {
        if let itemsArray = NSKeyedUnarchiver.unarchiveObjectWithFile(self.fileUrl.path!) {
            self.items = itemsArray as! [Person]
        }
    }
    
    // Dar item
    func getItem(index:Int) -> Person {
        return items[index]
    }
    
    // guardar lista de items
    func saveList(pItems: [Dictionary<String, AnyObject>]) {
        for item in pItems{
            self.person                        = Person()
            self.person?.id                    = item["id"] as? Int
            self.person?.nombres               = item["nombres"] as? String
            self.person?.apellidos             = item["apellidos"] as? String
            self.person?.foto                  = item["foto"] as? String
            self.person?.email                 = item["email"] as? String
            self.person?.empresa               = item["empresa"] as? String
            self.person?.clave                 = item["clave"] as? String
            self.person?.logo                  = item["logo"] as? String
            self.person?.cargo                 = item["cargo"] as? String
            self.person?.giro                  = item["giro"] as? String
            self.person?.telefono              = item["telefono"] as? String
            self.person?.descripcion_servicios = item["descripcion_servicios"] as? String
            self.person?.fcm_tok               = item["fcm_tok"] as? String
            self.person?.idEvento              = item["mEvent"] as? Int
            self.person?.idUsuario             = userLogin?.id
            self.person?.isView                = item["isFriend"] as? Bool
            self.person?.like                  = item["isFriend"] as? Bool
            self.person?.telefono_2            = item["telefono_2"] as? String
            self.person?.linkedin              = item["linkedin"] as? String
            self.addItem(self.person!)
        }
    }
    
    func saveItem(item: Dictionary<String, AnyObject>) {
        
        self.person                        = Person()
        self.person?.id                    = item["id"] as? Int
        self.person?.nombres               = item["nombres"] as? String
        self.person?.apellidos             = item["apellidos"] as? String
        self.person?.foto                  = item["foto"] as? String
        self.person?.email                 = item["email"] as? String
        self.person?.empresa               = item["empresa"] as? String
        self.person?.clave                 = item["clave"] as? String
        self.person?.logo                  = item["logo"] as? String
        self.person?.cargo                 = item["cargo"] as? String
        self.person?.giro                  = item["giro"] as? String
        self.person?.telefono              = item["telefono"] as? String
        self.person?.descripcion_servicios = item["descripcion_servicios"] as? String
        self.person?.fcm_tok               = item["fcm_tok"] as? String
        self.person?.idEvento              = Int(item["mEvent"] as! String)
        self.person?.idUsuario             = userLogin?.id
        self.person?.isFriend              = item["isFriend"] as? Bool
        self.person?.isView                = item["isView"] as? Bool
        self.person?.like                  = item["isFriend"] as? Bool
        self.person?.telefono_2            = item["telefono_2"] as? String
        self.person?.linkedin              = item["linkedin"] as? String
        
        if self.updateItem(self.person!) != true{
            self.addItem(self.person!)
        }
    }
    
    func updateItem(person:Person) -> Bool{
        if let index = self.items.indexOf({$0.id == person.id && $0.idEvento == person.idEvento && $0.idUsuario == person.idUsuario}){
            self.items[index] = person
            persistenceItems()
            return true
        }
        return false
    }
    
    func getContacts() -> [Person]{
        var list:[Person] = []
        for item in self.items {
            
            if item.idEvento! == Commonds.getIdEvent() && item.idUsuario == userLogin?.id && item.isFriend == true && item.id != userLogin?.id{
                list.append(item)
            }
        }
        return list
    }
    
    func getPersonUser() -> [Person]{
        var list:[Person] = []
        for item in self.items {
            if item.idEvento! == Commonds.getIdEvent() && item.idUsuario == userLogin?.id  && item.id != userLogin?.id{
                list.append(item)
            }
        }
        return list
    }
    
    func getPerson(id:Int) -> Person?{
        for item in self.items {
            if item.id == id{
                return item
            }
        }
        return nil
    }
}