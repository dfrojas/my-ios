//
//  ListAgenda.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 9/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

class ListMenu: NSObject{
    
    // ------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------
    
    private var menu:Menu?
    
    private let fileUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("MenuList.plist")
    
    // ------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------
    
    var items:[Menu] = []
    var userLogin = Commonds.getUser()
    var idEvent = Commonds.getIdEvent()
    
    // ------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------
    
    override init() {
        super.init()
        self.loadItemsOnStart()
    }
    
    // ------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------
    
    
    
    func persistenceItems()  {
        let itemsArray = self.items as NSArray
        NSKeyedArchiver.archiveRootObject(itemsArray, toFile: self.fileUrl.path!)
    }
    
    func getItems() ->[Menu]{
        return self.items
    }
    
    func getMenuEvent() -> Menu?{
        
        if let index = self.items.indexOf({ $0.idEvent == idEvent }){
            return self.items[index]
        }
        
        return nil
    }
    
    // Cargar items al iniciar
    func loadItemsOnStart() {
        if let itemsArray = NSKeyedUnarchiver.unarchiveObjectWithFile(self.fileUrl.path!) {
            self.items = itemsArray as! [Menu]
        }
    }
    
    func addItem(pitem:Menu){
        if let index = self.items.indexOf({$0.id == pitem.id}){
            self.items[index] = pitem
            persistenceItems()
        } else {
            items.append(pitem)
            persistenceItems()
        }
    }
    
    func saveList(itemsAg:[Dictionary<String,AnyObject>]) -> Menu{
        self.menu = Menu()
        
        for item in itemsAg {
            
            menu?.id           = item["id"] as? Int
            menu?.idEvent      = item["evento"] as? Int
            if let items = item["items"] as? [Int]{
                menu?.items = items
                
            }
            self.addItem(menu!)
        }
        return self.menu!
    }
    
    func updateItem(person:Menu) {
        
        if let index = self.items.indexOf({$0.id == person.id}){
            self.items[index] = person
            persistenceItems()
        }
    }
}
