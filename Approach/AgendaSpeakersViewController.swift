//
//  AgendaSpeakersViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 16/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class AgendaSpeakersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    // ------------------------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------------------------
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelRoom: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    
    @IBOutlet weak var buttonFavorite: UIBarButtonItem!
    var listAgenda = ListAgenda()
    
    var agenda:Agenda?
    var speakers:[Speaker] = []
    var sponsors:[Sponsor]?
    
    // ------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.load()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: IBAction
    // ------------------------------------------------------------------------------------
    
    @IBAction func goBack(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func actionFavorite(sender: UIBarButtonItem) {
        if let agenda = self.agenda{
            
            if let state = agenda.isFavorite {
                agenda.isFavorite = !state
                listAgenda.updateItem(agenda)
                if agenda.isFavorite == true {
                    self.buttonFavorite.image = UIImage(named: "fav_ful_24")
                }else {
                    self.buttonFavorite.image = UIImage(named: "fav_24")
                }
            }
        }
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func load(){
        if let pAgenda = agenda {
            
            self.speakers           = pAgenda.speakers
            self.labelDate.text     = pAgenda.fecha
            self.labelTitle.text    = pAgenda.titulo
            
            if let phi = pAgenda.hora_inicio, phf = pAgenda.hora_fin{
                self.labelTime.text = "\(phi) - \(phf)"
            }
            self.labelRoom.text = pAgenda.salon
            
            
            if let nc = self.tabBarController?.viewControllers![0] as? UINavigationController, tab = nc.topViewController as? AgendaInfoViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if let pSponsors = self.sponsors where  pSponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
            }
            
            if let nc = self.tabBarController?.viewControllers![2] as? UINavigationController, tab = nc.topViewController as? AgendaLugarViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if pAgenda.isFavorite == true {
                self.buttonFavorite.image = UIImage(named: "fav_ful_24")
            }else {
                self.buttonFavorite.image = UIImage(named: "fav_24")
            }
            
            indicator.startAnimating()
            
            if let pAgenda = self.agenda{
                self.speakers = pAgenda.speakers
                indicator.stopAnimating()
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                })
            }
        }
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: tableViewDelegate
    // ------------------------------------------------------------------------------------
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.speakers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AgendaSpeakersTableViewCell
        
        let count = speakers.count
        
        if  count > 0 && indexPath.row < count{
            let entry = speakers[indexPath.row]
            cell.labelName.text     = entry.nombre_conferencista
            
            if let img = entry.foto{
                cell.imgSpeaker.layer.cornerRadius = cell.imgSpeaker.frame.size.width / 2;
                cell.imgSpeaker.clipsToBounds = true;
                cell.imgSpeaker.loadImageUsingCacheWithUrlString(img)
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
}

extension AgendaSpeakersViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors![indexPath.row]
        if let img = entry.imagen{
            
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors!.count
        
        let totalCellWidth = 65 * self.sponsors!.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors![indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
}
