//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Exhibitor{
    
    var id:Int?
    var nombre:String?
    var codigo_stand:String?
    var link_facebook:String?
    var link_twitter:String?
    var link_linkedin:String?
    var descripcion:String?
    var foto:String?
    var evento:Int?
    var foto_stand:String?
    
    var exhibitors:[Exhibitor] = []
    var socialNetworks:[SocialNetwork] = []
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Exhibitor] {
        
        exhibitors = []
        
        for item in items {
            var exhibitor           = Exhibitor()
            exhibitor.id            = item["id"] as? Int
            exhibitor.nombre        = item["nombre"] as? String
            exhibitor.codigo_stand  = item["codigo_stand"] as? String
            exhibitor.link_facebook = item["link_facebook"] as? String
            exhibitor.link_twitter  = item["link_twitter"] as? String
            exhibitor.link_linkedin = item["link_linkedin"] as? String
            exhibitor.descripcion   = item["descripcion"] as? String
            exhibitor.foto          = item["foto"] as? String
            exhibitor.foto_stand    = item["foto_stand"] as? String
            exhibitor.evento        = item["evento"] as? Int
            
            if let array = item["redes_sociales"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
                var nSpeaker = SocialNetwork()
                exhibitor.socialNetworks = nSpeaker.addItems(pSpeakers)
            }
            exhibitors.append(exhibitor)
        }
        return exhibitors
    }
    
    func setEvent(item:Dictionary<String,AnyObject>) -> Exhibitor {
        var exhibitor           = Exhibitor()
        exhibitor.id            = item["id"] as? Int
        exhibitor.nombre        = item["nombre"] as? String
        exhibitor.codigo_stand  = item["codigo_stand"] as? String
        exhibitor.link_facebook = item["link_facebook"] as? String
        exhibitor.link_twitter  = item["link_twitter"] as? String
        exhibitor.link_linkedin = item["link_linkedin"] as? String
        exhibitor.descripcion   = item["descripcion"] as? String
        exhibitor.foto          = item["foto"] as? String
        exhibitor.evento        = item["evento"] as? Int
        exhibitor.foto_stand    = item["foto_stand"] as? String
        
        if let array = item["redes_sociales"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
            var nSpeaker = SocialNetwork()
            exhibitor.socialNetworks = nSpeaker.addItems(pSpeakers)
        }
        return exhibitor
    }
    
    func getItem(index: Int) -> Exhibitor {
        return exhibitors[index]
    }
}