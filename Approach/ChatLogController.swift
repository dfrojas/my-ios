//
//  ChatViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChatLogController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextFieldDelegate{
    
    private let cellId              = "cellId"
    private var messageList: ListMessages!
    private let idEvent             = Commonds.getIdEvent()
    private let userLogin           = Commonds.getUser()
    private var messages:[Message]  = []
    private var bottomConstraint: NSLayoutConstraint?
    internal var imageFriend = UIImageView()
    
    let messageInputContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.whiteColor()
        return view
    }()
    
    let inputTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Escribe un mensaje..."
        return textField
    }()
    
    let sendButton: UIButton = {
        let button = UIButton(type: .Custom)
        button.setImage(UIImage(named: "btn_send"), forState: UIControlState.Normal)
        button.adjustsImageWhenDisabled = true
        button.contentHorizontalAlignment = .Center
        button.contentVerticalAlignment = .Center
        button.contentMode = .ScaleToFill
        button.clearsContextBeforeDrawing = true
        button.autoresizesSubviews = true
        return button
    }()
    
    internal var friend: Person? {
        didSet {
            if let nombres = friend?.nombres, apellidos = friend?.apellidos {
                navigationItem.title = "\(nombres) \(apellidos)"
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBarController?.tabBar.hidden = true
        collectionView?.backgroundColor = UIColor.whiteColor()
        collectionView?.registerClass(ChatMessageCell.self, forCellWithReuseIdentifier: cellId)
        view.addSubview(messageInputContainerView)
        
        view.addConstraintsWithFormat("H:|[v0]|", views: messageInputContainerView)
        view.addConstraintsWithFormat("V:[v0(48)]", views: messageInputContainerView)
        
        bottomConstraint = NSLayoutConstraint(item: messageInputContainerView, attribute: .Bottom, relatedBy: .Equal, toItem: view, attribute: .Bottom, multiplier: 1, constant: 0)
        view.addConstraint(bottomConstraint!)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.reloadTable),name: "chat-notification", object: nil)
        
        setupInputComponents()
        self.setupKeyboardObservers()
        
        self.inputTextField.delegate = self
        
        let item : UITextInputAssistantItem = self.inputTextField.inputAssistantItem
        item.leadingBarButtonGroups = []
        item.trailingBarButtonGroups = []
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        IQKeyboardManager.sharedManager().enable = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = false
        messageList = ListMessages()
        if let user = friend {
            
            if Commonds.isConnectedToNetwork() == true{
                
                FactoryRelationship.getMessages((user.id)!, listMessage: self.messageList,idEvent: self.idEvent,successBlock: { (response) in
                        self.loadInfo(response)
                        self.reloadDataView()
                        }, andFailure: { (error) in
                    })
                
            }else {
                let list = messageList.getMessagesOfUsers(user.id!)
                self.loadInfo(list)
                self.reloadDataView()
            }
            
            if let image = user.foto where imageFriend.image == nil{
                self.imageFriend.loadImageUsingCacheWithUrlString(image)
                self.reloadDataView()
            }
        }
        
    }
    
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    
    func setupKeyboardObservers() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(handleKeyboardWillShow), name: UIKeyboardWillShowNotification, object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(handleKeyboardWillHide), name: UIKeyboardWillHideNotification, object: nil)
    }
    
    func handleKeyboardWillHide(notification: NSNotification) {
        let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
        
        bottomConstraint?.constant = 0
        self.reloadDataView()
        UIView.animateWithDuration(keyboardDuration!) {
            self.view.layoutIfNeeded()
        }
    }
    
    func handleKeyboardWillShow(notification: NSNotification) {
        let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey]?.CGRectValue()
        let keyboardDuration = notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey]?.doubleValue
        
        bottomConstraint?.constant = -keyboardFrame!.height

        self.reloadDataView()
        UIView.animateWithDuration(keyboardDuration!) {
            self.view.layoutIfNeeded()
        }
    }
    
    func showNavigationBar(){
        self.navigationController?.navigationBarHidden = false
    }

    func loadInfo(list: [Message]) {
        self.messages = list
        let noRead = list.filter({$0.state != true && $0.mTo == self.userLogin?.id})
        if noRead.count > 0 {
            
            var response:[Int] = []
            
            for item in noRead {
                item.state = true
                response.append(item.id!)
            }
            
            self.messageList.updateItems(noRead)
            
            //BADGE
            Commonds.setNotificationMessages(response.count, plus: false)
            
            let updateMessages: Dictionary<String, AnyObject>= [
                "id_mensaje": response
            ]
            
            FactoryRelationship.updateMessage(updateMessages, successBlock: { (result) in
                
                }, andFailure: { (error) in
            })
        }
    }
    
    
    func actionInfo(sender: UIBarButtonItem) {
        self.performSegueWithIdentifier("sw_profile_info", sender: self)
    }
    
    @IBAction func actionClose() {
        self.dismissViewControllerAnimated(true, completion: {});
    }
    
    private func reloadDataView(){
        let item = self.collectionView(self.collectionView!, numberOfItemsInSection: 0)
        if item > 0{
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.collectionView?.reloadData()
                let lastItemIndex = NSIndexPath(forItem: item - 1, inSection: 0)
                self.collectionView?.scrollToItemAtIndexPath(lastItemIndex, atScrollPosition: UICollectionViewScrollPosition.Top, animated: false)
            })
        }
    }
    
    
    func reloadTable(notification: NSNotification){
        if let nn = notification.userInfo as? Dictionary<String, AnyObject>, mEvent = nn["mEvent"] as? String, mTo = Int(nn["mFrom"] as! String)  where Int(mEvent) == self.idEvent && mTo == friend?.id!{
            
            let message         = Message()
            message.mMessage    = nn["mMessage"] as? String
            message.mFecha      = nn["mFecha"] as? NSDate
            message.mIdEvent    = Int(mEvent)
            message.mFrom       = Int(nn["mFrom"] as! String)
            message.mTo         = Int(nn["mTo"] as! String)
            message.id          = Int(nn["id"] as! String)
            message.state       = true
            
            self.messageList.addItem(message)
            
            if let id = message.id, _ = message.mMessage, _ = message.mTo, _ = message.mFrom {
                
                let updateMessages: Dictionary<String, AnyObject>= [
                    "id_mensaje": [id]
                ]
                FactoryRelationship.updateMessage(updateMessages, successBlock: { (result) in
                    
                    }, andFailure: { (error) in
                })
            }
            self.messages.append(message)
            self.reloadDataView()
        }
    }
    
    func sendMessage(){
        let nMessage = Message()
        
        if let msj = inputTextField.text, person = self.friend where !msj.isEmpty{
            nMessage.mTo        = person.id
            nMessage.mFecha     = NSDate.init()
            nMessage.mFrom      = self.userLogin?.id
            nMessage.mMessage   = msj
            nMessage.mIdEvent   = self.idEvent
            
            let send:Dictionary<String, AnyObject> = ["mMessage":msj, "mTo": nMessage.mTo!]
            
            self.messages.append(nMessage)
            self.inputTextField.text = nil
            reloadDataView()
            FactoryRelationship.sendMessage(send, successBlock: { (result) in
                self.messageList.addItem2(result)
                
                
                }, andFailure: { (error) in
                    self.inputTextField.resignFirstResponder()
                    Commonds.showMessage(error, title: "Lo sentimos", content: self)
            })
        }
    }
    
    private func setupInputComponents() {
        let topBorderView = UIView()
        topBorderView.backgroundColor = UIColor(white: 0.5, alpha: 0.5)
        
        sendButton.addTarget(self, action:#selector(self.sendMessage), forControlEvents: .TouchUpInside)
        messageInputContainerView.addSubview(inputTextField)
        messageInputContainerView.addSubview(sendButton)
        messageInputContainerView.addSubview(topBorderView)
        messageInputContainerView.addConstraintsWithFormat("H:|-8-[v0][v1(40)]-4-|", views: inputTextField, sendButton)
        messageInputContainerView.addConstraintsWithFormat("V:|[v0]|", views: inputTextField)
        messageInputContainerView.addConstraintsWithFormat("V:|-4-[v0(40)]-4-|", views: sendButton)
        messageInputContainerView.addConstraintsWithFormat("H:|[v0]|", views: topBorderView)
        messageInputContainerView.addConstraintsWithFormat("V:|[v0(0.5)]", views: topBorderView)
        
        let containerView = UIView()
        containerView.backgroundColor = UIColor.whiteColor()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(containerView)
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: collection view
    // ------------------------------------------------------------------------------------
    
    override func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        inputTextField.endEditing(true)
    }
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellId, forIndexPath: indexPath) as! ChatMessageCell
        
        let item = messages[indexPath.item]
        cell.textView.text = item.mMessage
        cell.textView.editable = false
        cell.textView.selectable = false
        
        cell.bubbleWidthAnchor?.constant = estimateFrameForText(item.mMessage!).width + 32
        
        if let user = self.userLogin, id = user.id where id == item.mFrom{
            cell.textView.leftAnchor.constraintEqualToAnchor(cell.bubbleView.leftAnchor, constant: 8).active = true
        }else {
            cell.textView.leftAnchor.constraintEqualToAnchor(cell.bubbleView.leftAnchor, constant: 12).active = true
        }
        
        self.setupCell(cell, message: item)
        
        return cell
    }
    
    
    private func setupCell(cell: ChatMessageCell, message: Message) {
        
        cell.profileImageView.image = self.imageFriend.image
        
        if let user = self.userLogin, id = user.id where id != message.mFrom{
            //outgoing blue
            cell.bubbleView.backgroundColor = UIColor.clearColor()
            cell.textView.textColor = UIColor(rgba: "#555555")
            cell.profileImageView.hidden = false
            cell.bubbleViewRightAnchor?.active = false
            cell.bubbleViewLeftAnchor?.active = true
            cell.bubbleImageView.tintColor   = UIColor(rgba: "#E4E4E4")
            cell.bubbleImageView.image = ChatMessageCell.grayBubbleImage
            
        } else {
            //incoming gray
            cell.bubbleView.backgroundColor = UIColor.clearColor()
            cell.textView.textColor = UIColor(rgba: "#555555")
            cell.profileImageView.hidden = true
            cell.bubbleImageView.tintColor   = UIColor(rgba: "#BBEEEB")
            cell.bubbleImageView.image = ChatMessageCell.blueBubbleImage
            cell.bubbleViewRightAnchor?.active = true
            cell.bubbleViewLeftAnchor?.active = false
        }
    }
    
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let item = messages[indexPath.item]
        
        var height: CGFloat = 100
        
        if let text = item.mMessage {
            height = estimateFrameForText(text).height + 20
        }
        
        let width = UIScreen.mainScreen().bounds.width
        return CGSize(width: width, height: height)
    }
    
    private func estimateFrameForText(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.UsesFontLeading.union(.UsesLineFragmentOrigin)
        return NSString(string: text).boundingRectWithSize(size, options: options, attributes: [NSFontAttributeName: UIFont.systemFontOfSize(16)], context: nil)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(8, 0, 50, 0)
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: navigation
    // ------------------------------------------------------------------------------------
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let info = segue.destinationViewController as? PerfilChatViewController{
            if let person = self.friend{
                info.friend = person
            }
        }
        
    }
}

extension UIView {
    func addConstraintsWithFormat(format: String, views: UIView...) {
        var viewsDictionary = [String: UIView]()
        for (index, view) in views.enumerate() {
            let key = "v\(index)"
            viewsDictionary[key] = view
            view.translatesAutoresizingMaskIntoConstraints = false
        }
        
        addConstraints(NSLayoutConstraint.constraintsWithVisualFormat(format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
}