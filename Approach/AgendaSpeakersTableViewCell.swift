//
//  AgendaSpeakersTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 17/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class AgendaSpeakersTableViewCell: UITableViewCell {

    @IBOutlet weak var imgSpeaker: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
