//
//  TerminosViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 30/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class TerminosViewController: UIViewController {

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var textView: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.indicator.startAnimating()
        
        FactoryLegal.getTerm({ (term) in
            self.indicator.stopAnimating()
            self.textView.text = term
            }) { (result) in
                self.indicator.stopAnimating()
                Commonds.showMessage(result, title: "Lo sentimos", content: self)
        }
        
        
        //viewHeightConstraint.constant = viewHeightConstraint.constant + textView.requiredHeight()
        //viewBottomConstraint.constant = viewHeightConstraint.constant + textView.requiredHeight()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func actionMenu(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    @IBAction func actionCloseModal(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
