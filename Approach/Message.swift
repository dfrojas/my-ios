import UIKit

class Message: NSObject, NSCoding {
    
    // -------------------------------------------------------------------
    // ATRIBUTOS
    // -------------------------------------------------------------------
    
    var mType        : String?
    var mFrom        : Int?
    var mMessage     : String?
    var mFecha       : NSDate?
    var mTo          : Int?
    var mIdEvent     : Int?
    var state        : Bool?
    var id           : Int?
    
    // -------------------------------------------------------------------
    // CONSTRUCTOR
    // -------------------------------------------------------------------
    
    override init(){
        super.init()
    }
    
    // -------------------------------------------------------------------
    // METODS
    // -------------------------------------------------------------------
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        if let cmIdEvent = aDecoder.decodeObjectForKey("mIdEvent") as? Int{
            self.mIdEvent = cmIdEvent
        }
        if let cFecha = aDecoder.decodeObjectForKey("fecha") as? NSDate{
            self.mFecha = cFecha
        }
        
        if let cmFrom = aDecoder.decodeObjectForKey("mFrom") as? Int {
            self.mFrom = cmFrom
        }
        if let cmMessage = aDecoder.decodeObjectForKey("mMessage") as? String{
            self.mMessage = cmMessage
        }
        
        if let cmTo = aDecoder.decodeObjectForKey("mTo") as? Int{
            self.mTo = cmTo
        }
        
        if let cid = aDecoder.decodeObjectForKey("read") as? Bool {
            self.state = cid
        }
        
        if let cId = aDecoder.decodeObjectForKey("idMessage") as? Int{
            self.id = cId
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        if let cmIdEvent = self.mIdEvent{
            aCoder.encodeObject(cmIdEvent, forKey: "mIdEvent")
        }
        
        if let cFecha = self.mFecha{
            aCoder.encodeObject(cFecha, forKey: "fecha")
        }
        
        if let cmFrom = self.mFrom{
            aCoder.encodeObject(cmFrom, forKey: "mFrom")
        }
        
        if let cmMessage = self.mMessage{
            aCoder.encodeObject(cmMessage, forKey: "mMessage")
        }
        
        if let cmTo = self.mTo{
            aCoder.encodeObject(cmTo, forKey: "mTo")
        }
        
        if let cid = self.state{
            aCoder.encodeObject(cid, forKey: "read")
        }
        
        if let cId = self.id{
            aCoder.encodeObject(cId, forKey: "idMessage")
        }
    }
}
