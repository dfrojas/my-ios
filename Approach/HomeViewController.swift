//
//  HomeViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MapKit
import SlideMenuControllerSwift
import MessageUI
import MIBadgeButton_Swift


class HomeViewController: UIViewController, MKMapViewDelegate {
    
    // ------------------------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------------------------
    
    let refresController        = UIRefreshControl()

    // -------------------------------------------------------------
    // ATRIBUTOS
    // -------------------------------------------------------------
    
    //@IBOutlet var btnRightBadge: MIBadgeButton!
    
    @IBOutlet weak var btmMenu: UIBarButtonItem!
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var labTitle: UILabel!
    @IBOutlet weak var labDateTime: UILabel!
    @IBOutlet weak var labTelephone: UILabel!
    @IBOutlet weak var labWebPage: UILabel!
    @IBOutlet weak var labDescription: UILabel!
    @IBOutlet weak var map: MKMapView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var collectionSocial: UICollectionView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    @IBOutlet weak var ubication: UILabel!
    
    @IBOutlet weak var labDondeEs: UILabel!
    @IBOutlet weak var imgContacto: UIImageView!
    @IBOutlet weak var labelPatrocinadores: UILabel!
    @IBOutlet weak var imgWeb: UIImageView!
    
    
    @IBOutlet weak var constraintHeigtSocialNetwork: NSLayoutConstraint!
    
    @IBOutlet weak var constraintHeightLabPatrocinadores: NSLayoutConstraint!
    
    @IBOutlet weak var constraintHeightSponsor: NSLayoutConstraint!
    var socialNetworks:[SocialNetwork]  = []
    var sponsors:[Sponsor]              = []
    
    var itemSelectedSocial:SocialNetwork?
    var itemSelectedSponsor:Sponsor?
    
    var event:Event = Event()
    
    var window:UIWindow?
    
    // -------------------------------------------------------------
    // CONSTRUCTOR
    // -------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.map.delegate = self
        self.setupCamera()
        self.indicator.stopAnimating()
        self.buttonNotifications()
        
        refresController.addTarget(self, action: #selector(loadInfo), forControlEvents: .ValueChanged)
        self.scrollView.addSubview(refresController)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.buttonNotifications),name: "reload-notifications", object: nil)
        
        self.indicator.startAnimating()
        self.scrollView.hidden = false
        loadInfo()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        buttonNotifications()
        
        if self.event.nombre_evento == nil{
            self.imgContacto.hidden = true
            self.imgWeb.hidden = true
            self.labDondeEs.hidden = true
            self.labelPatrocinadores.hidden = true
            self.map.hidden = true
            loadInfo()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
        
        guard let flowLayoutSocial = collectionSocial.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSocial.invalidateLayout()
    }
    
    // -------------------------------------------------------------
    // METODOS
    // -------------------------------------------------------------
    
    func loadInfo(){
        FactoryEvent.getEvent({ (event) in
            self.indicator.stopAnimating()
            self.refresController.endRefreshing()
            self.scrollView.hidden = false
            self.event = event
            self.labTitle.text  = self.event.nombre_evento
            
            
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(event.id!, forKey: "id_event")
            defaults.setObject(event.nombre_evento, forKey: "name_event")
            defaults.setObject(event.fecha_vida_chat, forKey: "fecha_vida_chat")
            
            
            if let name = self.event.nombre_evento{
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(name, forKey: "name_event")
                self.labTitle.text  = name
            }
            
            
            if let fi = self.event.fecha_inicio, ff = self.event.fecha_fin {
                self.labDateTime.text = "\(fi) - \(ff)"
            }
            
            if let numContac = self.event.numero_contacto {
                
                if numContac.rangeOfString("@") == nil{
                    self.imgContacto.image = UIImage(named: "menu_tel-24")
                }else {
                    self.imgContacto.image = UIImage(named: "menu_mail-24")
                }
                
                self.imgContacto.hidden = false
                self.labTelephone.text   = numContac
            }else {
                self.imgContacto.hidden = true
            }
            
            if let pagWeb = self.event.link_pagina_web{
                self.imgWeb.hidden = false
                self.labWebPage.text = pagWeb
            }else{
                self.imgWeb.hidden = true
            }
            
            self.labDescription.text = self.event.descripcion
            
            if let lugar = self.event.lugar{
                self.labDondeEs.hidden = false
                self.ubication.text = lugar
            }else {
                self.labDondeEs.hidden = true
            }
            
            
            if let pCoordinates = self.event.localizacion{
                self.map.hidden = false
                self.setupMap(pCoordinates)
            }else {
                self.map.hidden = true
            }
            
            if let image = self.event.banner {
                self.imgBanner.loadImageUsingCacheWithUrlString(image)
            }
            
            if self.event.socialNetworks.count > 0 {
                self.constraintHeightSponsor.constant = 55
                self.socialNetworks = event.socialNetworks
                self.collectionSocial.reloadData()
            }else {
                self.constraintHeigtSocialNetwork.constant = 0
            }
            
            FactorySponsor.getSponsors({ (sponsors) in
                
                if sponsors.count > 0{
                    self.constraintHeightSponsor.constant = 65
                    self.constraintHeightLabPatrocinadores.constant = 21
                    self.labelPatrocinadores.hidden = false
                    self.sponsors = sponsors
                    self.collectionSponsor.reloadData()
                }else {
                    self.constraintHeightSponsor.constant = 0
                    self.constraintHeightLabPatrocinadores.constant = 0
                    self.labelPatrocinadores.hidden = true
                }
            }) { (error) in
                
                Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
            }
            
            self.addGestured()
            self.indicator.stopAnimating()
            self.scrollView.hidden = false
            
        }) { (error) in
            self.indicator.stopAnimating()
            self.refresController.endRefreshing()
            //self.scrollView.hidden = true
            
            self.imgContacto.hidden = true
            self.imgWeb.hidden = true
            self.labDondeEs.hidden = true
            self.labelPatrocinadores.hidden = true
            self.map.hidden = true
            Commonds.showMessage("No dispones de conexion a internet, intenta nuevamente", title: "Error", content: self)
        }
    }
    
    func addGestured(){
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.addTarget(self, action: #selector(self.actionSendEmail))
        self.labTelephone.userInteractionEnabled = true
        self.labTelephone.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizer2 = UITapGestureRecognizer()
        tapGestureRecognizer2.numberOfTapsRequired = 1
        tapGestureRecognizer2.numberOfTouchesRequired = 1
        tapGestureRecognizer2.addTarget(self, action: #selector(self.openMap))
        self.map.userInteractionEnabled = true
        self.map.addGestureRecognizer(tapGestureRecognizer2)
        
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.openWeb))
        self.labWebPage.userInteractionEnabled = true
        self.labWebPage.addGestureRecognizer(tapGestureRecognizer3)
    }
    
    func openWeb(){
        if let txt = event.link_pagina_web, url = NSURL(string: (txt)){
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func openMap(){
        if let location = self.event.localizacion {
            let coordinates = location.characters.split{$0 == ","}.map(String.init)
            let query = "q=\(Double(coordinates[0])!),\(Double(coordinates[1])!)"
            let path = "maps:" + query
            if let url = NSURL(string: path) {
                UIApplication.sharedApplication().openURL(url)
            } else {
                Commonds.showMessage("La ubicación del evento no esta bien referenciada", title: "Lo sentimos", content: self)
            }
        } else {
            Commonds.showMessage("El evento no tiene posición", title: "Lo sentimos", content: self)
        }
    }
    
    func actionSendEmail(){
        let numContact = self.event.numero_contacto
        
        if numContact != nil && numContact != ""{
            if numContact!.rangeOfString("@") == nil{
                UIApplication.sharedApplication().openURL(NSURL(string: "tel://\(numContact!)")!)
            }else if MFMailComposeViewController.canSendMail(){
                let mail = MFMailComposeViewController()
                mail.mailComposeDelegate = self
                mail.setToRecipients([self.event.numero_contacto!])
                mail.setSubject("")
                mail.setMessageBody("", isHTML: true)
                presentViewController(mail, animated: true, completion: nil)
            }
            
            
        }
    }
    
    func setupMap(pCoordinates:String){
        let coordinates = pCoordinates.characters.split{$0 == ","}.map(String.init)
        let location    = CLLocationCoordinate2D(
            latitude    : Double(coordinates[0])!,
            longitude   : Double(coordinates[1])!
        )
        
        let span    = MKCoordinateSpanMake(0.05, 0.05)
        let region  = MKCoordinateRegion(center: location, span: span)
        let annotation          = MKPointAnnotation()
        annotation.coordinate   = location
        self.map.addAnnotation(annotation)
        self.map.setRegion(region, animated: true)
        self.map.zoomEnabled = false;
        self.map.scrollEnabled = false;
        self.map.userInteractionEnabled = false;
    }
    
    func setupCamera(){
        map.camera.altitude = 1400
        map.camera.pitch    = 50
        map.camera.heading  = 180
    }
    
    func buttonNotifications(){
        let badgeButton : MIBadgeButton = MIBadgeButton(frame: CGRectMake(0, 0, 20, 20))
        badgeButton.setImage(UIImage(named: "noti_24"), forState: .Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        let num = Commonds.getNotification()
        
        if num > 0{
            badgeButton.badgeString = "\(num)";
        }else {
            badgeButton.badgeString = nil
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.actionNotifications))
        badgeButton.userInteractionEnabled = true
        badgeButton.addGestureRecognizer(tapGestureRecognizer3)
    }
    
    func actionNotifications() {
        Commonds.setNotification(Commonds.getNotification(), plus: false)
        self.slideMenuController()?.openRight()
        self.buttonNotifications()
    }
    
    // -------------------------------------------------------------
    // MARK: - IBAction
    // -------------------------------------------------------------
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
}

extension HomeViewController :MFMailComposeViewControllerDelegate{
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        switch result.rawValue {
        case MFMailComposeResultCancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResultSaved.rawValue:
            print("Saved")
        case MFMailComposeResultSent.rawValue:
            print("Sent")
        case MFMailComposeResultFailed.rawValue:
            print("Error: \(error?.localizedDescription)")
        default:
            break
        }
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionSocial {
            return self.socialNetworks.count
        }
        return self.sponsors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        if collectionView == self.collectionSocial {
            let entry = self.socialNetworks[indexPath.row]
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            
            if let image = entry.icono{
                cell.imageView.loadImageUsingCacheWithUrlString(image)
            }
            return cell
        } else {
            let entry = self.sponsors[indexPath.row]
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            
            if let img = entry.imagen{
                
                cell.imageView.loadImageUsingCacheWithUrlString(img)
            }
            
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        if collectionView == self.collectionSocial {
            self.itemSelectedSocial = self.socialNetworks[indexPath.row]
            if let txt = itemSelectedSocial?.url, url = NSURL(string: (txt)){
                UIApplication.sharedApplication().openURL(url)
            }
        }else if collectionView == self.collectionSponsor{
            if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: self.view.frame, transitionCompletion: nil)
                    viewSponsor.sponsor = self.sponsors[indexPath.row]
                })
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if collectionView == self.collectionSponsor{
            let count = self.sponsors.count
            
            let totalCellWidth = 55 * self.sponsors.count
            let totalSpacingWidth = 30 * (count - 1)
            
            let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            let rightInset = leftInset
            
            if collectionSponsor.frame.width > CGFloat((totalSpacingWidth + totalCellWidth)){
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            }else {
                return UIEdgeInsetsMake(0, 10, 0, 10)
            }
        }else {
            
            let count = self.socialNetworks.count
            
            let totalCellWidth = 49 * count
            let totalSpacingWidth = 10 * (count - 1)
            
            let leftInset = (self.collectionSocial.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            let rightInset = leftInset
            
            if collectionSocial.frame.width > CGFloat((totalSpacingWidth + totalCellWidth)){
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            }else {
                return UIEdgeInsetsMake(0, 10, 0, 10)
            }
        }
    }
}

extension HomeViewController : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}