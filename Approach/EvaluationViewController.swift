//
//  EvaluationViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 20/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class EvaluationViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var userLogin = Commonds.getUser()
    var eventName = Commonds.getNameEvent()
    var p1:String?
    var p2:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.startAnimating()
        
        FactoryEvent.getEvent({ (event) in
            self.indicator.stopAnimating()
            if let link = event.link_encuesta{
                
                let linkArray = link.componentsSeparatedByString("id_usuario")
                
                if linkArray.count > 1{
                    self.p1 = "\(linkArray[0])\(self.userLogin!.email!)"
                    
                    let linkArray2 = linkArray[1].componentsSeparatedByString("id_evento")
                    
                    if let name = self.eventName where linkArray2.count > 0{
                        self.p2 = "\(linkArray2[0])\(name)"
                    }
                }
                
                if let link1 = self.p1, link2 = self.p2{
                    
                    let cadena = "\(link1)\(link2)"
                    
                    if let urlwithPercentEscapes = cadena.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                        
                        if let url = NSURL(string: urlwithPercentEscapes) {
                            let request = NSURLRequest(URL: url)
                            self.webView.loadRequest(request)
                        }else {
                            Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
                        }
                    }else {
                        Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
                    }
                }else {
                    Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
                }
            } else {
                Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
            }
            
            }) { (error) in
                Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
