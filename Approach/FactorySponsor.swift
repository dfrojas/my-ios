//
//  FactoryExhibitors.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactorySponsor {
    
    class func sponsors(idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "patrocinadores", idEvent].map{String($0)}.joinWithSeparator("/"))
    }
    
    class func getSponsors(successBlock: (sponsors: [Sponsor]) -> (), andFailure failureBlock: (NSError?) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "patrocinadores", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                var nSpeaker = Sponsor()
                let pSpeaker = nSpeaker.addItems(resp)
                successBlock(sponsors: pSpeaker)
            } else {
                failureBlock(nil)
            }
        }
    }
    
    class func sponsor(id:String, idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "patrocinador", id, idEvent].map{String($0)}.joinWithSeparator("/"))
    }
}