//
//  ContactTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class ContactTableViewController: UITableViewController{
    

    

    private var contacts:[Person] = []
    var resultSearchController    = UISearchController()
    var persons                   = ListPerson()
    var itemSelected:Person?
    var filteredPerson:[Person] = []
    var listMessage: ListMessages?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.hidesNavigationBarDuringPresentation = false
        self.resultSearchController.searchBar.sizeToFit()
        self.resultSearchController.searchBar.placeholder = "Buscar"
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.backgroundColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.translucent = true
        
        self.tableView.tableHeaderView = self.resultSearchController.searchBar
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.reloadData),name: "contact-notification", object: nil)
        self.resultSearchController.searchBar.searchBarStyle = UISearchBarStyle.Prominent
        
        definesPresentationContext = true
        self.extendedLayoutIncludesOpaqueBars = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        listMessage?.loadItemsOnStart()
        reloadData()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().postNotificationName("update-false", object: nil)
    }
    // ------------------------------------------------------------------------------------
    // MARK: - Table view data source
    // ------------------------------------------------------------------------------------
    

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            itemSelected = self.filteredPerson[indexPath.row]
            self.resultSearchController.active = false
        }else {
            itemSelected = self.contacts[indexPath.row]
        }
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("sw_contact", sender: self)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.resultSearchController.active{
            return filteredPerson.count
        }else {
            return contacts.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ContactTableViewCell
        var entry = contacts[indexPath.row]
        
        if self.resultSearchController.active{
            entry = self.filteredPerson[indexPath.row]
        }
        
        cell.person = entry
        cell.listMessages = self.listMessage
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    
    
    // ------------------------------------------------------------------------------------
    // MARK: METODOS
    // ------------------------------------------------------------------------------------
    
    
    
    func reloadData(){
        self.persons.loadItemsOnStart()
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.contacts = self.persons.getContacts()
            self.tableView.reloadData()
        })
    }

    // ------------------------------------------------------------------------------------
    // MARK: Navigation
    // ------------------------------------------------------------------------------------
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destination = segue.destinationViewController as! UINavigationController
        if let chat = destination.topViewController as? ChatLogController {
            if let person = self.itemSelected{
                chat.friend = person
            }
        }
    }
}

extension ContactTableViewController: UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredPerson = self.contacts.filter({ (exhibitor) -> Bool in
            let name = "\(exhibitor.nombres!) \(exhibitor.apellidos!)"
            let stringMatch = name.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            return stringMatch != nil
        })
        
        
        self.tableView.reloadData()
    }
}
