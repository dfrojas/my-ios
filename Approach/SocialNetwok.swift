//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct SocialNetwork{
    
    var red_social  :String?
    var icono       :String?
    var url         :String?
    var socialNetowoks:[SocialNetwork] = []
    
    
    func setEvent(item:Dictionary<String,AnyObject>) -> SocialNetwork {
        var nn = SocialNetwork()
        nn.red_social  = item["red_social"] as? String
        nn.icono       = item["icono"] as? String
        nn.url         = item["url"] as? String
        return nn
    }
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [SocialNetwork] {
        
        self.socialNetowoks = []
        
        for item in items {
            var nn = SocialNetwork()
            nn.red_social  = item["red_social"] as? String
            nn.icono       = item["icono"] as? String
            nn.url         = item["url"] as? String
            socialNetowoks.append(nn)
        }
        return socialNetowoks
    }
    
}

