//
//  MyTinderCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 8/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//


import UIKit
import Koloda

private let overlayRightImageName = "yesOverlayImage"
private let overlayLeftImageName = "noOverlayImage"

class MyTinderCell: OverlayView {
    
    @IBOutlet weak var overlayImageView: UIImageView!
    
    @IBOutlet var imgUser: UIImageView!
    @IBOutlet var imgCompany: UIImageView!
    
    @IBOutlet var img1: UIImageView!
    @IBOutlet var img3: UIImageView!
    
    
    @IBOutlet var labName: UILabel!
    @IBOutlet var labCargo: UILabel!
    
    @IBOutlet var lab1: UILabel!
    @IBOutlet var lab2: UILabel!
    @IBOutlet var lab3: UILabel!
    
    var user:Person?{
        didSet{
            
            if let pUser = user{
                
                if let text = pUser.empresa{
                    self.lab1.text = text
                }
                if let text = pUser.descripcion_servicios{
                    self.lab3.text = text
                }
                if let text = pUser.nombres, text2 = pUser.apellidos{
                    self.labName.text = "\(text) \(text2)"
                }
                if let text = pUser.cargo{
                    self.labCargo.text = text
                }
                
                if let photo = pUser.logo {
                    let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                    if !trimmedString.isEmpty{
                        
                        self.imgUser.loadImageUsingCacheWithUrlString(photo)
                    }
                }
                
                self.imgCompany.layer.cornerRadius = self.imgCompany.frame.size.width / 2;
                self.imgCompany.clipsToBounds      = true;
                
                if let photo = pUser.foto {
                    let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                    if !trimmedString.isEmpty{
                        imgCompany.loadImageUsingCacheWithUrlString(photo)
                    }
                }
            }
        }
    }
    
    override var overlayState: SwipeResultDirection? {
        didSet {
            switch overlayState {
            case .Left? :
                overlayImageView.image = UIImage(named: overlayLeftImageName)
            case .Right? :
                overlayImageView.image = UIImage(named: overlayRightImageName)
            default:
                overlayImageView.image = nil
            }
        }
    }
}