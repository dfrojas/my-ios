//
//  FactoryExhibitors.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryExhiborts {
    
    class func exhiborts(idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "expositores", idEvent].map{String($0)}.joinWithSeparator("/"))
    }
    
    class func event(id:String, idEvent:String) -> Request{
        return Alamofire.request(.GET, [Commonds.URL, "expositor", id, idEvent].map{String($0)}.joinWithSeparator("/"))
    }
}