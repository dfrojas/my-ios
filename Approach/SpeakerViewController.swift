//
//  ExhibitorViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
class SpeakerViewController: UIViewController{
    
    @IBOutlet weak var imgExhibitor     : UIImageView!
    @IBOutlet weak var labStand         : UILabel!
    @IBOutlet weak var labName          : UILabel!
    @IBOutlet weak var labDescription   : UILabel!
    @IBOutlet weak var collectionSocial : UICollectionView!
    //@IBOutlet weak var btnScore         : UIButton!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    @IBOutlet weak var collectionSocialConstraintHeigt: NSLayoutConstraint!
    
    let transition = PopAnimator()
    var speaker:Speaker?
    var socialNetworks:[SocialNetwork]  = []
    var sponsors:[Sponsor]     = []
    var itemSelectedSocial:SocialNetwork?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = " "
        
        self.collectionSocialConstraintHeigt.constant = 0
        
        if let nSpeaker = self.speaker {
            labStand.text       = nSpeaker.empresa_conferencista
            labName.text        = nSpeaker.nombre_conferencista
            labDescription.text = nSpeaker.descripcion
            
            if let img = nSpeaker.foto {
                self.imgExhibitor.layer.cornerRadius = self.imgExhibitor.frame.size.width / 2;
                self.imgExhibitor.clipsToBounds = true;
                self.imgExhibitor.loadImageUsingCacheWithUrlString(img)
            }
            
            let num = nSpeaker.socialNetworks.count
            
            if num > 0 {
                self.collectionSocialConstraintHeigt.constant = 55
                self.socialNetworks = nSpeaker.socialNetworks
            }else {
                self.collectionSocialConstraintHeigt.constant = 0
            }
            
            
            collectionSocial.reloadData()
        }
        
        FactorySponsor.getSponsors({ (sponsors) in
            if sponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
                self.sponsors = sponsors
                self.collectionSponsor.reloadData()
            }
        }) { (error) in
            Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
        }
        
        let tapGestureRecognizerPrev = UITapGestureRecognizer()
        tapGestureRecognizerPrev.numberOfTapsRequired       = 1
        tapGestureRecognizerPrev.numberOfTouchesRequired    = 1
        tapGestureRecognizerPrev.addTarget(self, action: #selector(self.openPreview))
        self.imgExhibitor.userInteractionEnabled = true
        self.imgExhibitor.addGestureRecognizer(tapGestureRecognizerPrev)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
        
    }
    
    func openPreview(){
        
        if let img = self.imgExhibitor.image {
            let imgPrev = storyboard!.instantiateViewControllerWithIdentifier("ImagePreviewViewController") as! ImagePreviewViewController
            imgPrev.image = img
            imgPrev.transitioningDelegate = self
            presentViewController(imgPrev, animated: true, completion: nil)
        }
        
    }
}

extension SpeakerViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.collectionSocial {
            return self.socialNetworks.count
        }else {
            return self.sponsors.count
        }
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionSocial {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
            
            let entry = self.socialNetworks[indexPath.row]
            
            if let img = entry.icono {
                cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
                cell.imageView.clipsToBounds = true;
                cell.imageView.loadImageUsingCacheWithUrlString(img)
            }
            
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
            
            let entry = self.sponsors[indexPath.row]
            if let img = entry.imagen {
                cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
                cell.imageView.clipsToBounds = true;
                cell.imageView.loadImageUsingCacheWithUrlString(img)
            }
            return cell
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if collectionView == self.collectionSocial {
            self.itemSelectedSocial = self.socialNetworks[indexPath.row]
            if let txt = itemSelectedSocial?.url, url = NSURL(string: (txt)){
                UIApplication.sharedApplication().openURL(url)
            }
        }else {
            if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
                viewSponsor.sponsor = self.sponsors[indexPath.row]
                self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
            }
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        if collectionView == self.collectionSponsor{
            let count = self.sponsors.count
            
            let totalCellWidth = 65 * self.sponsors.count
            let totalSpacingWidth = 10 * (count - 1)
            
            let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            let rightInset = leftInset
            
            if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            }else {
                return UIEdgeInsetsMake(0, 10, 0, 10)
            }
        }else {
            
            let count = self.socialNetworks.count
            
            let totalCellWidth = 55 * count
            let totalSpacingWidth = 10 * (count - 1)
            
            let leftInset = (self.collectionSocial.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
            let rightInset = leftInset
            
            if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
                return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
            }else {
                return UIEdgeInsetsMake(0, 10, 0, 10)
            }
        }
    }
}


extension SpeakerViewController: UIViewControllerTransitioningDelegate{
    func animationControllerForPresentedController(
        presented: UIViewController,
        presentingController presenting: UIViewController,
                             sourceController source: UIViewController) ->
        UIViewControllerAnimatedTransitioning? {
            
            transition.originFrame = imgExhibitor!.superview!.convertRect(imgExhibitor!.frame, toView: nil)
            transition.presenting = true
            
            return transition
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}

