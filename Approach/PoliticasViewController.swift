//
//  PoliticasViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 30/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class PoliticasViewController: UIViewController {
    
    @IBOutlet weak var textView: UILabel!

    @IBOutlet weak var indicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.indicator.startAnimating()
        FactoryLegal.getPrivacity({ (term) in
            self.indicator.stopAnimating()
            self.textView.text = term
        }) { (result) in
            self.indicator.stopAnimating()
            Commonds.showMessage(result, title: "Lo sentimos", content: self)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionMenu(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
