//
//  AppDelegate.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 9/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import SlideMenuControllerSwift
import LNRSimpleNotifications
import AudioToolbox

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var counter = 0
    var timer : NSTimer?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        IQKeyboardManager.sharedManager().enable = true
        SlideMenuOptions.contentViewScale = 1
        
        if Commonds.getIdEvent() > 0 && Commonds.getUser() != nil {
            
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
                
                mainViewController.view.frame = UIScreen.mainScreen().bounds
                
                let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
                let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
                let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
                
                UIView.transitionWithView(self.window!, duration: 0.5, options: .TransitionCrossDissolve, animations: {
                    self.window?.rootViewController = slideMenuController
                }, completion: nil)
        }
        
        
        
        // Status bar
        UIApplication.sharedApplication().statusBarStyle = UIStatusBarStyle.LightContent
        //Reguster push notifications
        let notificationTypes : UIUserNotificationType = [.Alert, .Badge, .Sound]
        let notificationSettings : UIUserNotificationSettings = UIUserNotificationSettings(forTypes: notificationTypes, categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        
        // FIREBASE
        FIRApp.configure()
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        
        // Add observer for InstanceID token refresh callback.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.tokenRefreshNotification),name: kFIRInstanceIDTokenRefreshNotification, object: nil)
        return true
    }
    
    
    // [START receive_message]
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        self.manageNotifications(application, userInfo: userInfo, completionHandler: completionHandler)
        
        
    }
    
    func vibrate() {
        counter = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: #selector(self.vibratePhone), userInfo: nil, repeats: true)
    }
    
    func vibrate2() {
        counter = 0
        timer = NSTimer.scheduledTimerWithTimeInterval(0.6, target: self, selector: #selector(self.vibratePhone2), userInfo: nil, repeats: true)
    }
    
    func vibratePhone2() {
        counter = counter + 1
        switch counter {
        case 1, 2:
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        default:
            timer?.invalidate()
        }
    }
    
    func vibratePhone() {
        counter = counter + 1
        switch counter {
        case 1, 2:
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
        default:
            timer?.invalidate()
        }
    }
    
    func convertStringToDictionary(text: String) -> Dictionary<String,AnyObject>? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
                return json
            } catch {
                
            }
        }
        return nil
    }
    
    func shouldNotification(application: UIApplication, userInfo: [NSObject : AnyObject], mMessage: String?, action: Bool = false, title: String = ""){
        let notificationChat = UILocalNotification()
        notificationChat.userInfo       = userInfo
        notificationChat.alertTitle     = title
        notificationChat.alertBody      = mMessage
        notificationChat.alertAction    = "open"
        notificationChat.fireDate       = NSDate.init()
        notificationChat.soundName      = UILocalNotificationDefaultSoundName
        application.scheduleLocalNotification(notificationChat)
        //vibrate()
        vibrate2()
        if action == true {
            self.actionNotification(userInfo, application: application)
        }
    }
    
    func shouldNotification2(application: UIApplication, userInfo: [NSObject : AnyObject], mMessage: String, title: String, blockOpen blockSucces: () -> Void ){
        
        let notificationManager = LNRNotificationManager()
        notificationManager.notificationsPosition = LNRNotificationPosition.Top
        notificationManager.notificationsBackgroundColor = UIColor.blackColor()
        notificationManager.notificationsTitleTextColor = UIColor.whiteColor()
        notificationManager.notificationsBodyTextColor = UIColor.whiteColor()
        notificationManager.notificationsSeperatorColor = UIColor(rgba: "#E42164")
        self.vibrate()
        notificationManager.showNotification(title, body: mMessage, onTap: { () -> Void in
            
            notificationManager.dismissActiveNotification({ () -> Void in
                blockSucces()
            })
        })
    }
    
    func actionNotification(userInfo: [NSObject : AnyObject], application: UIApplication){
        if(application.applicationState == UIApplicationState.Inactive || application.applicationState == UIApplicationState.Background && Commonds.getIdEvent() > 0){
                let pp = ListPerson()
                
                if let id = userInfo["mFrom"] as? String, person = pp.getPerson(Int(id)!){
                    
                    let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("chatLog") as! UINavigationController
                    rootVC.view.frame = UIScreen.mainScreen().bounds
                    
                    let targetController = rootVC.topViewController as! ChatLogController
                    person.messages = []
                    targetController.friend = person
                    self.window?.rootViewController?.presentViewController(rootVC, animated: false, completion: nil)
                }
        }
    }
    
    func manageNotifications(application: UIApplication, userInfo: [NSObject : AnyObject], completionHandler: (UIBackgroundFetchResult) -> Void){
        
        if let type = userInfo["mType"] as? String where type == "chat"{
            let persons = ListPerson()
            if let mEvent = userInfo["mEvent"] as? String, person = persons.getPerson(Int(userInfo["mFrom"] as! String)!), names = person.nombres, lasName = person.apellidos where NSInteger(mEvent) == Commonds.getIdEvent(){
                if let pmessage = userInfo["mMessage"] as? String{
                    let message:Dictionary<String, AnyObject> = ["mMessage" : pmessage, "mEvent" : mEvent, "mFrom" : userInfo["mFrom"]!, "mTo" : userInfo["mTo"]!, "mType": type, "mFecha": NSDate.init(), "id": userInfo["id"]!]
                    
                    let listMessage = ListMessages()
                    listMessage.saveItem(message)
                    
                    if application.applicationState == .Active{
                        
                        NSNotificationCenter.defaultCenter().postNotificationName("chat-notification", object: nil, userInfo: message)
                        
                        if let smc = self.window?.rootViewController as? SlideMenuController {
                            if let vc = smc.mainViewController as? UINavigationController{
                                if let _ = vc.presentedViewController{
                                    //Observer for notification chat message
                                } else if let _ = vc.topViewController as? UITabBarController{
                                    //Observer for notification chat message
                                    Commonds.setNotificationMessages(1, plus: true)
                                    
                                }else {
                                    
                                    let pp = ListPerson()
                                    
                                    
                                    if let id = userInfo["mFrom"] as? String, person = pp.getPerson(Int(id)!){
                                        
                                        Commonds.setNotificationMessages(1, plus: true)
                                        
                                        self.shouldNotification2(application, userInfo: userInfo, mMessage: pmessage, title: "\(names) \(lasName)", blockOpen: {
                                            
                                            smc.closeLeft()
                                            smc.closeRight()
                                            
                                            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("chatLog") as! UINavigationController
                                            rootVC.view.frame = UIScreen.mainScreen().bounds
                                            
                                            
                                            let targetController = rootVC.topViewController as! ChatLogController
                                            person.messages = []
                                            targetController.friend = person
                                            self.window?.rootViewController?.presentViewController(rootVC, animated: false, completion: nil)
                                            
                                        })
                                    }
                                }
                            }
                        }
                    }else {
                        Commonds.setNotificationMessages(1, plus: true)
                        
                        if let aps = userInfo["aps"] as? Dictionary<String, AnyObject>, alert = aps["alert"] as? Dictionary<String, AnyObject> where application.applicationState == .Inactive{
                            
                            self.shouldNotification(application, userInfo: userInfo, mMessage: "\(alert["body"]!)", action: true, title: "\(alert["title"]!)")
                        }
                        
                    }
                    completionHandler(.NewData)
                }
            }
        }else if let types = userInfo["mType"] as? String where types == "liked"{
            if let pp = userInfo["mUser"] as? NSString, person = self.convertStringToDictionary(pp as String){
                
                Commonds.setNotification(1, plus: true)
                
                
                let lperson = ListPerson()
                var aux = person
                aux["isFriend"] = true
                aux["isView"] = true
                lperson.saveItem(aux)
                let messageNot = "Tienes un nuevo interesado"
                
                if let mEvent = person["mEvent"] as? String where Int(mEvent) == Commonds.getIdEvent() {
                    if application.applicationState == .Active{
                        
                        
                        
                        
                        if let smc = self.window?.rootViewController as? SlideMenuController {
                            if let vc = smc.mainViewController as? UINavigationController{
                                if let _ = vc.presentedViewController{
                                    //Observer for notification chat message
                                } else if let _ = vc.topViewController as? UITabBarController{
                                    //Observer for notification chat message
                                    Commonds.setNotification(1, plus: true)
                                    
                                }else {
                                    
                                    let pp = ListPerson()
                                    
                                    
                                    if let id = userInfo["mFrom"] as? String, person = pp.getPerson(Int(id)!){
                                        
                                        Commonds.setNotification(1, plus: true)
                                        
                                        self.shouldNotification2(application, userInfo: userInfo, mMessage: "", title: messageNot, blockOpen: {
                                            
                                            smc.closeLeft()
                                            smc.closeRight()
                                            
                                            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("chatLog") as! UINavigationController
                                            rootVC.view.frame = UIScreen.mainScreen().bounds
                                            
                                            Commonds.setNotification(1, plus: false)
                                            let targetController = rootVC.topViewController as! ChatLogController
                                            person.messages = []
                                            targetController.friend = person
                                            self.window?.rootViewController?.presentViewController(rootVC, animated: false, completion: nil)
                                            
                                        })
                                    }
                                }
                            }
                        }
                    }else {
                        self.shouldNotification(application, userInfo: userInfo, mMessage: messageNot, action: true)
                    }
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName("contact-notification", object: nil)
                completionHandler(.NewData)
            }else if let  person = userInfo["mUser"] as? Dictionary<String, AnyObject>{
                
                Commonds.setNotification(1, plus: true)
                
                let lperson = ListPerson()
                var aux = person
                aux["isFriend"] = true
                aux["isView"] = true
                lperson.saveItem(aux)
                let messageNot = "Tienes un nuevo interesado"
                
                if let mEvent = person["mEvent"] as? String where Int(mEvent) == Commonds.getIdEvent() {
                    if application.applicationState == .Active{
                        self.shouldNotification2(application, userInfo: userInfo, mMessage: "", title: messageNot, blockOpen: {
                        })
                    }else {
                        self.shouldNotification(application, userInfo: userInfo, mMessage: messageNot)
                    }
                }
                
                NSNotificationCenter.defaultCenter().postNotificationName("contact-notification", object: nil)
                completionHandler(.NewData)
            }
        }else if let types = userInfo["mType"] as? String where types == "Notificacion"{
            Commonds.setNotification(1, plus: true)
            
            if let notification = userInfo["notification"] as? Dictionary<String, AnyObject>, body = notification["body"] as? String{
                
                NSNotificationCenter.defaultCenter().postNotificationName("reload-notifications", object: nil, userInfo: nil)
                
                self.shouldNotification2(application, userInfo: userInfo, mMessage: "", title: "\(body)", blockOpen: {
                    
                })
                completionHandler(.NewData)
            } else {
                
                NSNotificationCenter.defaultCenter().postNotificationName("reload-notifications", object: nil, userInfo: nil)
                
                if application.applicationState == .Active{
                    self.shouldNotification2(application, userInfo: userInfo, mMessage: " ", title: "\(userInfo["body"]!)", blockOpen: {
                        
                    })
                }else {
                    self.shouldNotification(application, userInfo: userInfo, mMessage: "\(userInfo["body"]!)")
                }
                
                completionHandler(.NewData)
            }
            
            
        }
    }
    // [END receive_message]
    
    // [START refresh_token]
    func tokenRefreshNotification(notification: NSNotification) {
        if let refreshedToken = FIRInstanceID.instanceID().token(){
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setObject(refreshedToken, forKey: "fcm_token")
            print("InstanceID token: \(refreshedToken)")
        }
        connectToFcm()
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        //FIRMessaging.messaging().disconnect()
        print("Disconnected from FCM.")
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        
        connectToFcm()
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings)
    {
        UIApplication.sharedApplication().registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        
        print("el tokeeeeeen es \(deviceToken)")
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error.localizedDescription)
    }
    
    func application(application: UIApplication, supportedInterfaceOrientationsForWindow window: UIWindow?) -> UIInterfaceOrientationMask {
        
        if let _ = window?.currentViewController() as? PresentationViewController {
            
            if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
                UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
            }
            
            return UIInterfaceOrientationMask.Portrait
        }
        
        if let _ = window?.currentViewController() as? ImagePreviewViewController {
            
            if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
                UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
            }
            
            return UIInterfaceOrientationMask.Portrait
        }
        
        if let _ = window?.rootViewController as? TableEventViewController {
            
            if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
                UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
            }
            
            return UIInterfaceOrientationMask.Portrait
        }
        
        if let nc = window?.rootViewController as? UINavigationController, _ = nc.topViewController as? TableEventViewController{
            
            if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
                UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
            }
            
            return UIInterfaceOrientationMask.Portrait
        }
        
        if let smc = self.window?.rootViewController as? SlideMenuController, vc = smc.mainViewController as? UINavigationController, _ = vc.topViewController as? RelationshipViewController{
            
            if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
                UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
            }
            
            return UIInterfaceOrientationMask.Portrait
            
        } else {
            return UIInterfaceOrientationMask.All
        }
        
        
    }
    
    // FCM CLOUD MESSENG
    
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
}