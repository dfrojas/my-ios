//
//  ScoreExhibitorViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 17/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class ScoreExhibitorViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    var userLogin = Commonds.getUser()
    var eventName = Commonds.getNameEvent()
    var agenda:Agenda?
    var p1:String?
    var p2:String?
    var p3:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        indicator.stopAnimating()
        
        
        if let link = agenda?.link_encuesta where !link.isEmpty{
            
            let linkArray = link.componentsSeparatedByString("id_usuario")
            if linkArray.count > 1 {
                self.p1 = "\(linkArray[0])\(self.userLogin!.email!)"
                let linkArray2 = linkArray[1].componentsSeparatedByString("id_evento")
                
                if let name = self.eventName where linkArray2.count > 1{
                    self.p2 = "\(linkArray2[0])\(name)"
                    
                    let linkArray3 = linkArray2[1].componentsSeparatedByString("id_conferencia")
                    
                    if let name = self.agenda?.titulo where linkArray3.count > 0{
                        self.p3 = "\(linkArray3[0])\(name)"
                    }
                }
            }
            
            if let link1 = self.p1, link2 = self.p2, link3 = self.p3{
                
                let cadena = "\(link1)\(link2)\(link3)"
                
                
                
                
                if let urlwithPercentEscapes = cadena.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet()) {
                    if let url = NSURL(string: urlwithPercentEscapes) {
                        let request = NSURLRequest(URL: url)
                        self.webView.loadRequest(request)
                    }else {
                        Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
                    }
                }else {
                    Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
                }

                
                
                
                
            }else {
                Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
            }
            
            
            
        } else {
            Commonds.showMessage("Por el momento no esta disponible la encuesta de evaluación", title: "Ocurrio un error", content: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
    }
    
    

}
