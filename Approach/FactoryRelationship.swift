//
//  FactoryRelationship.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryRelationship {
    class func getUsers(successBlock: (users: [Person]) -> (), andFailure failureBlock: (String?) -> ()) {
        
        if let id = Commonds.getUser()?.id{
            Alamofire.request(.GET, [Commonds.URL, "relacionamiento", Commonds.getIdEvent(), id].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
                if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                    var send:[Dictionary<String, AnyObject>] = []
                    for item in resp{
                        if let puser = item["usuario"] as? [Dictionary<String, AnyObject>] {
                            send.append(puser[0])
                        }
                    }
                    var nSpeaker = UserList()
                    let pSpeaker = nSpeaker.addItems(send)
                    successBlock(users: pSpeaker)
                } else {
                    failureBlock("Ocurrio un error, intenta nuevamente")
                }
            }
        }
    }
    
    class func swipe(id:Int, success:(resp:Bool)-> (), callError error:(error: String) ->()){
        if let idUser = Commonds.getUser()?.id{
            Alamofire.request(.GET, [Commonds.URL, "swipe", Commonds.getIdEvent(), idUser, id].map{String($0)}.joinWithSeparator("/")).responseJSON(completionHandler: { response in
                
                if let value = response.result.value, json = value as? Dictionary<String, AnyObject>{
                    if let status = json["status"] as? String where status == "match"{
                        success(resp: false)
                    }else if let status = json["status"] as? String where status == "Liked"{
                        let lperson = ListPerson()
                        if let person = json["usuario"] as? Dictionary<String, AnyObject> {
                            var aux = person
                            aux["isFriend"] = true
                            aux["isView"] = true
                            lperson.saveItem(aux)
                            success(resp: true)
                        }else {
                            error(error: "Ocurrio un error en el servidor, intenta nuevamente")
                        }
                    }else {
                        error(error: "Ocurrio un error en el servidor, intenta nuevamente")
                    }
                }else {
                    error(error: "Ocurrio un error, verifica tu conexión a internet")
                }
            })
        }
    }
    
    class func sendMessage(parameters: Dictionary<String, AnyObject>, successBlock: (result: Message) -> (), andFailure failureBlock: (error:String) -> ()){
        
        let URL = NSURL(string: [Commonds.URL, "enviar-mensaje/"].map{String($0)}.joinWithSeparator("/"))!
        var send = parameters
        send["mFrom"]   = Commonds.getUser()?.id!
        send["mEvento"] = Commonds.getIdEvent()
        
        Alamofire.request(.POST, URL, parameters: send, encoding: .JSON)
            .responseJSON { response in
                if let value = response.result.value, json = value as? Dictionary<String, AnyObject>, data = json["data"] as? Dictionary<String, AnyObject>{
                    let nMessage = Message()
                    var item = data
                    if let event = item["mEvent"] as? Int, mFrom = item["mFrom"] as? Int, mTo = item["mTo"] as? Int, message = item["mMessage"] as? String, id = item["id"] as? Int{
                        
                        nMessage.mFrom        = NSInteger(mFrom)
                        nMessage.mTo          = NSInteger(mTo)
                        nMessage.mMessage     = message
                        nMessage.mIdEvent     = NSInteger(event)
                        nMessage.mType        = "chat"
                        nMessage.mFecha       = NSDate.init()
                        nMessage.id           = NSInteger(id)
                        if let hh = item["read"] as? Bool {
                            nMessage.state = hh
                        }else {
                            nMessage.state = false
                        }
                        successBlock(result: nMessage)
                        
                    } else {
                        failureBlock(error: "No fue posible enviar su mensaje, intente nuevamente")
                    }
                }else {
                    failureBlock(error: "No Dispone de conexión a internet, por favor intente intente nuevamente")
                }
        }
    }
    
    class func getMaches(idEvento: Int, idUsuario: Int,successBlock: (result: [Int]) -> (), andFailure failureBlock: (error:String) -> ()){
        
        let URL = NSURL(string: [Commonds.URL, "matchedios/"].map{String($0)}.joinWithSeparator("/"))!
        let send: Dictionary<String, AnyObject> = ["id_evento": idEvento, "id_usuario": idUsuario]
        
        Alamofire.request(.POST, URL, parameters: send, encoding: .JSON)
            .responseJSON { response in
                if let value = response.result.value, send = value as? [Int]{
                    successBlock(result: send)
                }else {
                    failureBlock(error: "No Dispone de conexión a internet, por favor intente intente nuevamente")
                }
        }
    }
    
    class func updateMessage(parameters: Dictionary<String, AnyObject>, successBlock: (result: Bool) -> (), andFailure failureBlock: (error:String) -> ()){
        
        let URL = NSURL(string: [Commonds.URL, "readed-chat/"].map{String($0)}.joinWithSeparator("/"))!
        Alamofire.request(.POST, URL, parameters: parameters, encoding: .JSON)
            .responseJSON { response in
                if response.result.value != nil{
                    successBlock(result: true)
                }else {
                    successBlock(result: false)
                }
        }
    }
    
    class func getMessages(idPerson: Int, listMessage: ListMessages, idEvent: Int,successBlock: (response: [Message])-> (), andFailure failureBlock: (String)-> ()){
        var send:Dictionary<String, AnyObject> = [:]
        
        send["idUser"]          = Commonds.getUser()!.id!
        send["idUserMessage"]   = idPerson
        send["idEvent"]         = idEvent
        
        let URL = NSURL(string: [Commonds.URL, "saved-chat/"].map{String($0)}.joinWithSeparator("/"))!
        
        Alamofire.request(.POST, URL, parameters: send, encoding: .JSON)
            .responseJSON { response in
                if let value = response.result.value, json = value as? [Dictionary<String, AnyObject>]{
                    successBlock(response: listMessage.saveList2(json))
                }else {
                    failureBlock("Ocurrio un error")
                }
        }
    }
}
