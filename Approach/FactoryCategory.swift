//
//  FactoryCategory.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 7/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryCategory {
    class func getCategories(successBlock: (categories: [Category]) -> (), andFailure failureBlock: (NSError?) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "categorias", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                var nSpeaker = Category()
                let pSpeaker = nSpeaker.addItems(resp)
                successBlock(categories: pSpeaker)
            } else {
                failureBlock(nil)
            }
        }
    }
}
