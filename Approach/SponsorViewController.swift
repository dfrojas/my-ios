//
//  ExhibitorViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class SponsorViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var imgExhibitor     : UIImageView!
    @IBOutlet weak var labStand         : UILabel!
    @IBOutlet weak var labName          : UILabel!
    @IBOutlet weak var labDescription   : UILabel!
    @IBOutlet weak var collectionSocial : UICollectionView!
    
    @IBOutlet weak var constraintHeigtSocial: NSLayoutConstraint!
    
    var sponsor:Sponsor?
    var socialNetworks:[SocialNetwork]  = []
    var itemSelectedSocial:SocialNetwork?
    let transition = PopAnimator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = " "
        
        
        if let nSponsor = self.sponsor {
            
            if let pp = nSponsor.tipo_patrocinio {
                labStand.text = pp
            }
            
            labName.text = nSponsor.nombre
            labDescription.text = nSponsor.descripcion
            
            if let img = nSponsor.imagen{
                self.imgExhibitor.layer.cornerRadius = self.imgExhibitor.frame.size.width / 2;
                self.imgExhibitor.clipsToBounds = true;
                self.imgExhibitor.loadImageUsingCacheWithUrlString(img)
            }
            
            if !nSponsor.socialNetworks.isEmpty{
                constraintHeigtSocial.constant = 55
                self.socialNetworks = nSponsor.socialNetworks
                collectionSocial.reloadData()
            }else{
                constraintHeigtSocial.constant = 0
            }
        }
        
        let tapGestureRecognizerPrev = UITapGestureRecognizer()
        tapGestureRecognizerPrev.numberOfTapsRequired       = 1
        tapGestureRecognizerPrev.numberOfTouchesRequired    = 1
        tapGestureRecognizerPrev.addTarget(self, action: #selector(self.openPreview))
        self.imgExhibitor.userInteractionEnabled = true
        self.imgExhibitor.addGestureRecognizer(tapGestureRecognizerPrev)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func openPreview(){
        
        if let img = self.imgExhibitor.image {
            let imgPrev = storyboard!.instantiateViewControllerWithIdentifier("ImagePreviewViewController") as! ImagePreviewViewController
            imgPrev.image = img
            imgPrev.transitioningDelegate = self
            presentViewController(imgPrev, animated: true, completion: nil)
        }
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    
    // -------------------------------------------------------------
    // MARK: - Collection
    // -------------------------------------------------------------
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.socialNetworks.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.socialNetworks[indexPath.row]
        
        if let img = entry.icono{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        self.itemSelectedSocial = self.socialNetworks[indexPath.row]
        if let txt = itemSelectedSocial?.url, url = NSURL(string: (txt)){
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        let count = self.socialNetworks.count
        
        let totalCellWidth = 55 * count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSocial.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSocial.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
}

extension SponsorViewController: UIViewControllerTransitioningDelegate{
    func animationControllerForPresentedController(
        presented: UIViewController,
        presentingController presenting: UIViewController,
                             sourceController source: UIViewController) ->
        UIViewControllerAnimatedTransitioning? {
            
            transition.originFrame = imgExhibitor!.superview!.convertRect(imgExhibitor!.frame, toView: nil)
            transition.presenting = true
            
            return transition
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}

