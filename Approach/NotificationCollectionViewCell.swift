//
//  NotificationCollectionViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import TimeAgoInWords
import UIColor_Hex_Swift

class NotificationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var text: UILabel!
    //@IBOutlet weak var text:UITextView!
    @IBOutlet weak var labDate:UILabel!
    
    var notification:Notification? {
        didSet{
            
            if let nn = notification{
                if let pTxt = nn.texto{
                    self.text.text = pTxt
                    self.text.textColor = UIColor(rgba: "#AAAAAA")
                }
                if let pTxt = nn.fecha{
                    self.timeAgo(pTxt)
                }
            }
        }
    }
    
    func timeAgo(fecha: String){
        let railsStrings = [
            "LessThan": "",
            "About": "",
            "Over": "",
            "Almost": "Casi ",
            "Seconds": "S ",
            "Minute": " m",
            "Minutes": "ms",
            "Hour": " h",
            "Hours": "hs ",
            "Day": "d",
            "Days": " ds",
            "Months": " M",
            "Years": " A",
            ]
        TimeAgoInWordsStrings.updateStrings(railsStrings)
        if let ff = self.parseDate(fecha){
            let tt = NSDate(timeIntervalSinceNow: ff.timeIntervalSinceNow).timeAgoInWords()
            self.labDate.text = tt
        }
    }
    
    func parseDate(string: String) -> NSDate?{
        let parse = NSDateFormatter()
        parse.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        parse.dateFormat = "d MMM, yyyy HH:mm"
        return parse.dateFromString(string)
    }
    
}
