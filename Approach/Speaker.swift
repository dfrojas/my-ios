//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Speaker{
    
    var id                   :Int?
    var nombre_conferencista :String?
    var foto                 :String?
    var empresa_conferencista:String?
    var cargo_conferencista  :String?
    var salon                :String?
    var link_facebook        :String?
    var link_twitter         :String?
    var link_linkedin        :String?
    var link_gplus           :String?
    var link_email           :String?
    var descripcion          :String?
    var link_encuesta    :String?
    var evento               :Int?
    
    var speakers:[Speaker]              = []
    var socialNetworks:[SocialNetwork]   = []
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Speaker] {
        
        speakers = []
        
        for item in items {
            var speaker                   = Speaker()
            speaker.id                    = item["id"] as? Int
            speaker.nombre_conferencista  = item["nombre_conferencista"] as? String
            speaker.foto                  = item["foto"] as? String
            speaker.empresa_conferencista = item["empresa_conferencista"] as? String
            speaker.cargo_conferencista   = item["cargo_conferencista"] as? String
            speaker.salon                 = item["salon"] as? String
            speaker.link_facebook         = item["link_facebook"] as? String
            speaker.link_twitter          = item["link_twitter"] as? String
            speaker.link_linkedin         = item["link_linkedin"] as? String
            speaker.link_gplus            = item["link_gplus"] as? String
            speaker.link_email            = item["link_email"] as? String
            speaker.descripcion           = item["descripcion"] as? String
            speaker.link_encuesta         = item["link_encuesta"] as? String
            speaker.evento                = item["evento"] as? Int
            if let array = item["redes_sociales"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
                var nSpeaker = SocialNetwork()
                speaker.socialNetworks = nSpeaker.addItems(pSpeakers)
            }
            speakers.append(speaker)
        }
        return speakers
    }
    
    func setSpeaker(item:Dictionary<String,AnyObject>) -> Speaker {
        var speaker                   = Speaker()
        speaker.id                    = item["id"] as? Int
        speaker.nombre_conferencista  = item["nombre_conferencista"] as? String
        speaker.foto                  = item["foto"] as? String
        speaker.empresa_conferencista = item["empresa_conferencista"] as? String
        speaker.cargo_conferencista   = item["cargo_conferencista"] as? String
        speaker.salon                 = item["salon"] as? String
        speaker.link_facebook         = item["link_facebook"] as? String
        speaker.link_twitter          = item["link_twitter"] as? String
        speaker.link_linkedin         = item["link_linkedin"] as? String
        speaker.link_gplus            = item["link_gplus"] as? String
        speaker.link_email            = item["link_email"] as? String
        speaker.descripcion           = item["descripcion"] as? String
        speaker.link_encuesta         = item["link_encuesta"] as? String
        speaker.evento                = item["evento"] as? Int
        
        if let array = item["redes_sociales"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
            var nSpeaker = SocialNetwork()
            speaker.socialNetworks = nSpeaker.addItems(pSpeakers)
        }
        return speaker
    }
    
    func getItem(index: Int) -> Speaker {
        return speakers[index]
    }
}