//
//  FactoryUser.swift
//  El diario de Morat
//
//  Created by Andres Felipe Lozano on 10/05/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import Alamofire


class FactoryUser{
    
    class func register(params: String, successBlock: (user: User) -> (), andFailure failureBlock: (String?) -> ()){
        let URL                 = NSURL(string: [Commonds.URL, "registro/"].map{String($0)}.joinWithSeparator("/"))!
        let mutableURLRequest   = NSMutableURLRequest(URL: URL)
        
        mutableURLRequest.setValue(params, forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        mutableURLRequest.HTTPMethod    = "POST"
        mutableURLRequest.HTTPBody      = params.dataUsingEncoding(NSUTF8StringEncoding)
        
        let manager = Alamofire.Manager.sharedInstance
        manager.request(mutableURLRequest).responseJSON { (response) in
            if let JSON = response.result.value, resp = JSON["status"] as? String where resp != "success"{
                
                if let JSON = response.result.value, user = JSON["usuario"] as? [Dictionary<String, AnyObject>] {
                    
                    
                    
                    var pUser = user[0]
                    pUser["evento"] = Commonds.getIdEvent()
                    
                    var nUser = User()
                    nUser     = nUser.getUser(pUser)
                    successBlock(user: nUser)
                }else {
                    failureBlock("Ocurrio un error en el servidor, intenta nuevamente")
                }
            }
            else if let JSON = response.result.value, resp = JSON["usuario"] as? [Dictionary<String, AnyObject>] {
                var nUser = User()
                nUser     = nUser.getUser(resp[0])
                successBlock(user: nUser)
            } else {
                failureBlock("Ocurrio un error en el servidor, intenta nuevamente")
            }
        }
    }
    
    class func saveProfile(parameters: Dictionary<String, AnyObject>, successBlock: (user: User) -> (), andFailure failureBlock: (error:String) -> ()){
        
        let URL = NSURL(string: [Commonds.URL, "completa-perfil/"].map{String($0)}.joinWithSeparator("/"))!
        
        Alamofire.request(.POST, URL, parameters: parameters, encoding: .JSON)
            .responseJSON { response in
                if let JSON = response.result.value, resp = JSON as? Dictionary<String, AnyObject> {
                    if let nn = resp["usuario"] as? Dictionary<String, AnyObject>{
                        var nUser = User()
                        nUser     = nUser.getUser(nn)
                        successBlock(user: nUser)
                    }else {
                        if let msj = JSON["status"] as? String{
                            failureBlock(error: "\(msj)")
                        }
                    }
                }else {
                    failureBlock(error: "Ocurrio un error en el servidor, intenta nuevamente")
                }
        }
    }
    
    class func login(params: String, successBlock: (user: User) -> (), andFailure failureBlock: (error:String) -> ()){
        let URL                 = NSURL(string: [Commonds.URL, "login/"].map{String($0)}.joinWithSeparator("/"))!
        let mutableURLRequest   = NSMutableURLRequest(URL: URL)
        
        mutableURLRequest.setValue(params, forHTTPHeaderField: "Authorization")
        mutableURLRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        mutableURLRequest.setValue("application/json", forHTTPHeaderField: "Accept")
        
        mutableURLRequest.HTTPMethod    = "POST"
        mutableURLRequest.HTTPBody      = params.dataUsingEncoding(NSUTF8StringEncoding)
        
        let manager = Alamofire.Manager.sharedInstance
        manager.request(mutableURLRequest).responseJSON { (response) in
            if let JSON = response.result.value, resp = JSON as? Dictionary<String, AnyObject> {
                if let nn = resp["usuario"] as? Dictionary<String, AnyObject>{
                    var nUser = User()
                    nUser     = nUser.getUser(nn)
                    successBlock(user: nUser)
                }else {
                    failureBlock(error: "Email y/o contraseña incorrectos")
                }
                
            } else {
                failureBlock(error: "Ocurrio un error intenta nuevamente")
            }
        }
    }
    
    class func isRegister(successBlock1: (state: Bool) -> (), andFailure failureBlock1: (String) -> ()){
        
        if let user = Commonds.getUser() where Commonds.getIdEvent() > 0{
            let parameters:Dictionary<String, AnyObject> = [
                "id_usuario": user.id!,
                "id_evento": Commonds.getIdEvent()
            ]
            
            let URL = NSURL(string: [Commonds.URL, "usuario-evento/"].map{String($0)}.joinWithSeparator("/"))!
            
            Alamofire.request(.POST, URL, parameters: parameters, encoding: .JSON)
                .responseJSON { response in
                    
                    if let JSON = response.result.value, resp = JSON as? Dictionary<String, AnyObject> {
                        if let nn = resp["status"] as? String{
                            if nn == "success" {
                                successBlock1(state: true)
                            }else {
                                successBlock1(state: false)
                            }
                        }else {
                            successBlock1(state: false)
                        }
                    }else {
                        failureBlock1("Verifica tu conexión a internet e intenta nuevamente")
                    }
            }
        }else {
            successBlock1(state: false)
        }
        
        
        
        
    }
}
