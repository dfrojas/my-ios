//
//  MessageList.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 18/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class ListMessages: NSObject {
    
    // ------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------
    
    private var message:Message?
    private let userLogin = Commonds.getUser()
    private let idEvent = Commonds.getIdEvent()
    private let fileUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("MessageList.plist")
    
    
    // ------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------
    
    var items:[Message] = []
    
    // ------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------
    
    override init() {
        super.init()
        self.loadItemsOnStart()
    }
    
    // ------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------
    
    // agregr item
    func addItem(item:Message){
        
        
        if let _ = self.items.indexOf({$0.id == item.id}){
            
        }else {
            self.items.append(item)
            persistenceItems()
        }
        
        /*if let id = item.id{
            var existe = false
            for m in self.items{
                if m.id! == id {
                 existe = true
                 break
                 }
            }
            if !existe{
                self.items.append(item)
                persistenceItems()
            }
        }*/
    }
    
    func addItem2(item:Message){
        self.items.append(item)
        persistenceItems()
        
    }
    
    // almacenar items
    func persistenceItems()  {
        let itemsArray = self.items as NSArray
        NSKeyedArchiver.archiveRootObject(itemsArray, toFile: self.fileUrl.path!)
    }
    
    // Cargar items al iniciar
    func loadItemsOnStart() {
        if let itemsArray = NSKeyedUnarchiver.unarchiveObjectWithFile(self.fileUrl.path!) {
            self.items = itemsArray as! [Message]
        }
    }
    
    func updateItem(message:Message) {
        if let index = self.items.indexOf({$0.id == message.id}){
            self.items[index] = message
            persistenceItems()
        }
    }
    
    func updateItems(messages:[Message]) {
        
        for uMessage in messages {
            if let index = self.items.indexOf({$0.id == uMessage.id}){
                self.items[index] = uMessage
                
            }
        }
        
        persistenceItems()
    }
    
    // Dar item
    func getItem(index:Int) -> Message {
        return items[index]
    }
    
    // guardar lista de items
    func saveList(pItems: [Dictionary<String, AnyObject>]) {
        for item in pItems{
            self.message               = Message()
            self.message?.mFecha       = item["mFecha"] as? NSDate
            
            self.message?.id           = item["id"] as? Int
            self.message?.mType        = item["mType"] as? String
            self.message?.mMessage     = item["mMessage"] as? String
            self.message?.state        = item["read"] as? Bool
            self.message?.mFrom        = item["mFrom"] as? Int
            self.message?.mTo          = item["mTo"] as? Int
            self.message?.mIdEvent     = item["mEvent"] as? Int
            
            
            if let mm = self.message{
                self.addItem(mm)
            }
            
        }
    }
    
    
    func saveList2(pItems: [Dictionary<String, AnyObject>]) -> [Message]{
        
        var list :[Message] = []
        
        for item in pItems{
            self.message               = Message()
            self.message?.mFecha       = item["mFecha"] as? NSDate
            
            self.message?.id           = item["id"] as? Int
            self.message?.mType        = item["mType"] as? String
            self.message?.mMessage     = item["mMessage"] as? String
            self.message?.state        = item["read"] as? Bool
            self.message?.mFrom        = item["mFrom"] as? Int
            self.message?.mTo          = item["mTo"] as? Int
            self.message?.mIdEvent     = item["mEvent"] as? Int
            
            
            if let mm = self.message{
                
                list.append(self.message!)
                self.addItem(mm)
            }
            
        }
        
        return list
    }
    
    
    func saveItem(item: Dictionary<String, AnyObject>) {
        if let event = item["mEvent"] as? String, mFrom = item["mFrom"] as? String, mTo = item["mTo"] as? String, message = item["mMessage"] as? String, type = item["mType"] as? String, fecha = item["mFecha"] as? NSDate, id = item["id"] as? String{
            
            self.message               = Message()
            self.message?.mFrom        = NSInteger(mFrom)
            self.message?.mTo          = NSInteger(mTo)
            self.message?.mMessage     = message
            self.message?.mIdEvent     = NSInteger(event)
            self.message?.mType        = type
            self.message?.mFecha       = fecha
            
            if let hh = item["read"] as? Bool {
                self.message?.state = hh
            }else {
                self.message?.state = false
            }
            
            self.message?.id           = NSInteger(id)
            self.items.append(self.message!)
            persistenceItems()
        }
    }
    
    // dar formato a la fecha
    func parseDate(string: String) -> NSDate?{
        let parse = NSDateFormatter()
        parse.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return parse.dateFromString(string)
    }
    
    
    func getMessagesOfUsers(idPerson: Int) -> [Message]{
        var list:[Message] = []
        
        for item in self.items {
            
            if  (item.mIdEvent! == self.idEvent) && ((self.userLogin?.id == item.mFrom && item.mTo! == idPerson) || (idPerson == item.mFrom! && item.mTo == self.userLogin?.id)){
                list.append(item)
            }
        }
        return list
    }
    
    func deleteMessagesRom() -> Void{
        for (index, item) in self.items.enumerate() {
            
            if item.mIdEvent == self.idEvent{
                items.removeAtIndex(index)
            }
        }
    }
}

