//
//  ContactTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class ContactTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgageCell: UIImageView!
    @IBOutlet weak var nameCell: UILabel!
    
    var person:Person?{
        didSet{
            if let ff = person, photo = ff.foto {
                let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                if !trimmedString.isEmpty{
                    self.imgageCell.loadImageUsingCacheWithUrlString(photo)
                }else {
                    self.imgageCell.image = UIImage(named: "profile_default")
                }
            }else {
                self.imgageCell.image = UIImage(named: "profile_default")
            }
            if let name = person?.nombres, lname = person?.apellidos {
                self.nameCell.text = "\(name) \(lname)"
            }
        }
    }
    
    var listMessages:ListMessages?{
        didSet{
            if let messages = listMessages, pPerson = self.person{
                if messages.getMessagesOfUsers(pPerson.id!).isEmpty{
                    self.nameCell.font = UIFont.boldSystemFontOfSize(15.0)
                }else {
                    self.nameCell.font = UIFont.systemFontOfSize(15, weight: UIFontWeightRegular)
                }
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.imgageCell.layer.cornerRadius = self.imgageCell.frame.size.width / 2;
        self.imgageCell.clipsToBounds = true;
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated) 
    }

}
