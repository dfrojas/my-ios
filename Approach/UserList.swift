//
//  UserList.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct UserList{
var id                   :Int?
var nombres              :String?
var apellidos            :String?
var foto                 :String?
var email                :String?
var fcm_tok              :String?
var empresa              :String?
var clave                :String?
var logo                 :String?
var cargo                :String?
var giro                 :String?
var telefono             :String?
var descripcion_servicios:String?
var isFriend:Bool?
var isView:Bool?
    
var users:[Person] = []
    
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Person] {
        for item in items {
            let user                   = Person()
            user.id                    = item["id"] as? Int
            user.nombres               = item["nombres"] as? String
            user.apellidos             = item["apellidos"] as? String
            user.foto                  = item["foto"] as? String
            user.email                 = item["email"] as? String
            user.empresa               = item["empresa"] as? String
            user.clave                 = item["clave"] as? String
            user.logo                  = item["logo"] as? String
            user.cargo                 = item["cargo"] as? String
            user.giro                  = item["giro"] as? String
            user.telefono              = item["telefono"] as? String
            user.descripcion_servicios = item["descripcion_servicios"] as? String
            user.fcm_tok               = item["fcm_tok"] as? String
            user.isFriend              = item["isFriend"] as? Bool
            user.isView                = false
            user.idUsuario             = Commonds.getUser()?.id
            user.idEvento              = Commonds.getIdEvent()
            
            users.append(user)
        }
        return users
    }
    
    func setUser(item:Dictionary<String,AnyObject>) -> Person {
        let user                   = Person()
        user.id                    = item["id"] as? Int
        user.nombres               = item["nombres"] as? String
        user.apellidos             = item["apellidos"] as? String
        user.foto                  = item["foto"] as? String
        user.email                 = item["email"] as? String
        user.empresa               = item["empresa"] as? String
        user.clave                 = item["clave"] as? String
        user.logo                  = item["logo"] as? String
        user.cargo                 = item["cargo"] as? String
        user.giro                  = item["giro"] as? String
        user.telefono              = item["telefono"] as? String
        user.descripcion_servicios = item["descripcion_servicios"] as? String
        user.fcm_tok               = item["fcm_tok"] as? String
        user.isFriend              = item["isFriend"] as? Bool
        user.isView                = false
        user.idUsuario             = Commonds.getUser()?.id
        user.idEvento              = Commonds.getIdEvent()
        return user
    }
    
    func getItem(index: Int) -> Person {
        return users[index]
    }
    

}