//
//  FactoryLegal.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 31/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryLegal {
    class func getTerm(successBlock: (term: String) -> (), andFailure failureBlock: (String) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "terminos"].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? Dictionary<String, AnyObject> {
                
                if let term = resp["terminos"] as? String {
                    successBlock(term: term)
                }else {
                    failureBlock("Ocurrio un error en el servidor, intenta nuevamente")
                }
            } else {
                failureBlock("Verifica tu conexion a internet e intenta nuevamente")
            }
        }
    }
    
    class func getPrivacity(successBlock: (term: String) -> (), andFailure failureBlock: (String) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "privacidad"].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            if let JSON = response.result.value, resp = JSON as? Dictionary<String, AnyObject> {
                
                if let term = resp["privacidad"] as? String {
                    successBlock(term: term)
                }else {
                    failureBlock("Ocurrio un error en el servidor, intenta nuevamente")
                }
            } else {
                failureBlock("Verifica tu conexion a internet e intenta nuevamente")
            }
        }
    }
}