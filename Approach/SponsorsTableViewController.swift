//
//  sponsorsTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class SponsorsTableViewController: UITableViewController, UISearchResultsUpdating {
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    
    let refresController = UIRefreshControl()
    var resultSearchController  = UISearchController()
    var sponsor               = Sponsor()
    var sponsors              = [Sponsor]()
    var filteredSponsor       = [Sponsor]()
    var itemSelected:Sponsor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonNotifications()
        self.refresController.backgroundColor = UIColor.whiteColor()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.buttonNotifications),name: "reload-notifications", object: nil)
        
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.searchBar.placeholder = "Buscar"
        definesPresentationContext = true
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.translucent = true
        //self.resultSearchController.searchBar.barTintColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.sizeToFit()
        self.tableView.tableHeaderView = self.resultSearchController.searchBar
        self.resultSearchController.searchBar.setValue("Cancelar", forKey: "_cancelButtonText")
        self.refresController.addTarget(self, action: #selector(self.load), forControlEvents: .ValueChanged)
        self.tableView.addSubview(refresController)
        load()
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        buttonNotifications()
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            itemSelected = self.filteredSponsor[indexPath.row]
        }else {
            itemSelected = self.sponsors[indexPath.row]
        }
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("sw_sponsor", sender: self)
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        if self.resultSearchController.active == true{
            self.resultSearchController.active = !self.resultSearchController.active
        }
    }
    
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    
    
    func setupSearchBarSize(){
        self.resultSearchController.searchBar.frame.size.width = self.view.frame.size.width
    }
    
    func load(){
        FactorySponsor.sponsors("\(Commonds.getIdEvent())").responseJSON { (response) in
            
            self.refresController.endRefreshing()
            
            if let JSON = response.result.value, data = JSON as? [Dictionary<String, AnyObject>]{
                self.sponsors = self.sponsor.addItems(data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                    
                    let cells = self.tableView.visibleCells
                    let tableHeight: CGFloat = self.tableView.bounds.size.height
                    
                    for i in cells {
                        let cell: UITableViewCell = i as UITableViewCell
                        cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
                    }
                    
                    var index = 0
                    
                    for a in cells {
                        let cell: UITableViewCell = a as UITableViewCell
                        UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionNone, animations: {
                            cell.transform = CGAffineTransformMakeTranslation(0, 0);
                            }, completion: nil)
                        
                        index += 1
                    }
                })
                
            }else {
                Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
            }
        }
    }
    
    func buttonNotifications(){
        let badgeButton : MIBadgeButton = MIBadgeButton(frame: CGRectMake(0, 0, 20, 20))
        badgeButton.setImage(UIImage(named: "noti_24"), forState: .Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        let num = Commonds.getNotification()
        
        if num > 0{
            badgeButton.badgeString = "\(num)";
        }else {
            badgeButton.badgeString = nil
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.actionNotifications))
        badgeButton.userInteractionEnabled = true
        badgeButton.addGestureRecognizer(tapGestureRecognizer3)
    }

    
    func actionNotifications() {
        Commonds.setNotification(Commonds.getNotification(), plus: false)
        self.slideMenuController()?.openRight()
        self.buttonNotifications()
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: Table view data source
    // ------------------------------------------------------------------------------------
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.resultSearchController.active{
            return filteredSponsor.count
        }else {
            return sponsors.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SponsorTableViewCell
        var entry = sponsors[indexPath.row]
        
        if self.resultSearchController.active{
            entry = self.filteredSponsor[indexPath.row]
        }else {
            entry = self.sponsors[indexPath.row]
        }
        
        cell.nameCell.text = entry.nombre
        
        
        if let img = entry.imagen{
            cell.imgageCell.layer.cornerRadius = cell.imgageCell.frame.size.width / 2;
            cell.imgageCell.clipsToBounds = true;
            cell.imgageCell.loadImageUsingCacheWithUrlString(img)
        }
        
        
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 58
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let pSponsor = segue.destinationViewController as? SponsorViewController {
            pSponsor.sponsor = self.itemSelected
        }
    }
    
    
    // ------------------------------------------------------------------------------------
    // MARK: searchController
    // ------------------------------------------------------------------------------------
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredSponsor = self.sponsors.filter({ (sponsor) -> Bool in
            let stringMatch = sponsor.nombre!.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            return stringMatch != nil
        })
        self.tableView.reloadData()
    }
}
