//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Event{
    
    var id                  :Int?
    var nombre_evento       :String?
    var avatar              :String?
    var fecha_inicio        :String?
    var fecha_fin           :String?
    var localizacion        :String?
    var pin                 :String?
    var banner              :String?
    var numero_contacto     :String?
    var link_pagina_web     :String?
    var link_encuesta       :String?
    var descripcion         :String?
    var mapa                :String?
    var lugar               :String?
    var fecha_vida_chat     :String?
    
    var events:[Event] = []
    var socialNetworks:[SocialNetwork] = []
    var sponsors:[Sponsor] = []
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Event] {
        
        self.events = []
        
        for item in items {
            
            var event = Event()
            event.id              = item["id"] as? Int
            event.nombre_evento   = item["nombre_evento"] as? String
            event.avatar          = item["avatar"] as? String
            event.fecha_inicio    = item["fecha_inicio"] as? String
            event.fecha_fin       = item["fecha_fin"] as? String
            event.localizacion    = item["localizacion"] as? String
            event.pin             = item["pin_md5"] as? String
            event.banner          = item["banner"] as? String
            event.numero_contacto = item["numero_contacto"] as? String
            event.link_pagina_web = item["pagina_web"] as? String
            event.link_encuesta   = item["link_encuesta"] as? String
            event.descripcion     = item["descripcion"] as? String
            event.lugar           = item["lugar"] as? String
            event.mapa            = item["mapa"] as? String
            event.fecha_vida_chat = item["fecha_vida_chat"] as? String
            if let array = item["redes_sociales"], pEvent = array as? [Dictionary<String,AnyObject>]{
                var nSocialN = SocialNetwork()
                event.socialNetworks = nSocialN.addItems(pEvent)
            }
            
            if let arrays = item["patrocinadores"], pSponsor = arrays as? [Dictionary<String,AnyObject>]{
                var nSponsorN = Sponsor()
                event.sponsors = nSponsorN.addItems(pSponsor)
            }
            
            events.append(event)
        }
        return events
    }
    
    mutating func setEvent(item:Dictionary<String,AnyObject>) -> Event {
        var event = Event()
        event.id              = item["id"] as? Int
        event.nombre_evento   = item["nombre_evento"] as? String
        event.avatar          = item["avatar"] as? String
        event.fecha_inicio    = item["fecha_inicio"] as? String
        event.fecha_fin       = item["fecha_fin"] as? String
        event.localizacion    = item["localizacion"] as? String
        event.pin             = item["pin_md5"] as? String
        event.banner          = item["banner"] as? String
        event.numero_contacto = item["numero_contacto"] as? String
        event.link_pagina_web = item["pagina_web"] as? String
        event.link_encuesta   = item["link_encuesta"] as? String
        event.descripcion     = item["descripcion"] as? String
        event.lugar           = item["lugar"] as? String
        event.mapa            = item["mapa"] as? String
        event.fecha_vida_chat = item["fecha_vida_chat"] as? String
        
        if let array = item["redes_sociales"], pEvent = array as? [Dictionary<String,AnyObject>]{
            var nSocialN = SocialNetwork()
            event.socialNetworks = nSocialN.addItems(pEvent)
        }
        
        if let arrays = item["patrocinadores"], pSponsor = arrays as? [Dictionary<String,AnyObject>]{
            var nSponsorN = Sponsor()
            event.sponsors = nSponsorN.addItems(pSponsor)
        }
        
        return event
    }
    
    func getItem(index: Int) -> Event {
        return events[index]
    }
}