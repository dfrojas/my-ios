//
//  ModePayInViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class ModePayInViewController: UIViewController {

    @IBOutlet weak var welcom       : UILabel!
    @IBOutlet weak var startSession : UILabel!
    @IBOutlet weak var imageBackground: UIImageView!
    
    var alert       :UIAlertController?
    var event       :Event?
    var statePerfil :Bool?
    var linkedin    :String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.chageHome),name: "change-window", object: nil)
        self.navigationController?.navigationBarHidden = true
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.addTarget(self, action: #selector(self.showSession))
        self.startSession.userInteractionEnabled = true
        self.startSession.addGestureRecognizer(tapGestureRecognizer)
        // Do any additional setup after loading the view.
        
        if let pEvent = self.event, text =  pEvent.nombre_evento{
            welcom.text = "Bienvenido a \(text)"
            self.backgroundView(pEvent)
        }else {
            FactoryEvent.getEvent({ (event) in
                self.welcom.text = "Bienvenido a \(event.nombre_evento!)"
                self.backgroundView(event)
                }, andFailure: { (error) in
                    Commonds.showMessage(error, title: "Lo sentimos", content: self)
            })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBarHidden = false
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBarHidden = true
        
        if let user = Commonds.getUser() where user.evento == Commonds.getIdEvent(){
            self.chageHome()
        } else {
            checkForExistingAccessToken()
        }
    }
    
    
    func chageHome()  {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
            
            mainViewController.view.frame = UIScreen.mainScreen().bounds
            let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
            let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
            
            SlideMenuOptions.contentViewScale = 1
            //SlideMenuOptions.hideStatusBar = false;
            appDelegate.window?.rootViewController = slideMenuController
        })
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: - IBAction
    // ------------------------------------------------------------------------------------
    
    @IBAction func showLinkedin(sender: UIButton) {
        self.performSegueWithIdentifier("sw_linkedin", sender: self)
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func backgroundView(pEvent: Event){
        
        if let img = pEvent.banner {
            self.imageBackground.loadImageUsingCacheWithUrlString(img)
            self.imageBackground.makeBlurImage()
            self.imageBackground.contentMode = .ScaleAspectFill
            self.imageBackground.alpha = 0.55
        }
    }
    
    func showSession(){
        self.performSegueWithIdentifier("sw_session", sender: self)
    }
    
    func checkForExistingAccessToken() {
        if NSUserDefaults.standardUserDefaults().objectForKey("LIAccessToken") != nil {
            self.getUser()
        }
    }
    
    func getUser(){
        if let accessToken = NSUserDefaults.standardUserDefaults().objectForKey("LIAccessToken") {
            // Specify the URL string that we'll get the profile info from.
            let targetURLString = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,maiden-name,email-address,picture-url,siteStandardProfileRequest)?format=json"
            
            
            // Initialize a mutable URL request object.
            let request = NSMutableURLRequest(URL: NSURL(string: targetURLString)!)
            
            // Indicate that this is a GET request.
            request.HTTPMethod = "GET"
            
            // Add the access token as an HTTP header field.
            request.addValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
            
            
            // Initialize a NSURLSession object.
            let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
            
            // Make the request.
            let task: NSURLSessionDataTask = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
                // Get the HTTP status code of the request.
                let statusCode = (response as! NSHTTPURLResponse).statusCode
                
                if statusCode == 200 {
                    
                    // Convert the received JSON data into a dictionary.
                    do {
                        let dataDictionary = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers)
                        
                        if let email = dataDictionary["emailAddress"] as? String, name = dataDictionary["firstName"] as? String, lastName = dataDictionary["lastName"] as? String{
                            
                            var txt = "nombres=\(name)&apellidos=\(lastName)&email=\(email)&id_evento=\(Commonds.getIdEvent())&platform=ios&contrasena=&fcm_tok="
                            
                            if let fcm = Commonds.getToken() {
                                txt = txt + fcm
                            }
                            
                            if let pphoto = dataDictionary["pictureUrl"] as? String{
                                txt = txt + "&foto=\(pphoto)"
                            }
                            
                            if let urlProfile = dataDictionary["siteStandardProfileRequest"] as? Dictionary<String, AnyObject>, url = urlProfile["url"] as? String{
                                
                                self.linkedin = url
                            }
                            
                            FactoryUser.isRegister({ (state) in
                                FactoryUser.register(txt, successBlock: { (user) in
                                    if state == true{
                                        self.chageHome()
                                    }else {
                                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                            self.performSegueWithIdentifier("sw_complete_p2", sender: self)
                                        })
                                    }
                                    
                                    }, andFailure: { (error) in
                                        Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
                                })
                                
                                }, andFailure: { (error) in
                                    Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
                            })
                        }
                    }
                    catch {
                        Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
                    }
                }
            }
            task.resume()
        }
    }

    // ------------------------------------------------------------------------------------
    // MARK: - Navigation
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let modeViewController = segue.destinationViewController as? LoginLinkedinViewController{
            modeViewController.event = self.event
        }
        else if let modeViewController = segue.destinationViewController as? LoginViewController{
            modeViewController.statePerfil = self.statePerfil
        }
        else if let destination = segue.destinationViewController as? UINavigationController{
            if let perfilVC = destination.topViewController as? PerfilViewController, link = linkedin {
                perfilVC.urlLinkedin = link
            }
        }
        
        
    }
}
