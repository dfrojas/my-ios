//
//  CellCollectionViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 26/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class CellCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
