//
//  AgendaInfoViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 16/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class AgendaInfoViewController: UIViewController {
    
    // ------------------------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------------------------

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var labelDescription2: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var labelRoom: UILabel!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    
    @IBOutlet weak var buttonFavorite: UIBarButtonItem!
    
    var agenda:Agenda?
    var listAgenda = ListAgenda()
    var sponsors:[Sponsor]?
    
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    
    // ------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.indicator.stopAnimating()
        self.load()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.load()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: IBAction
    // ------------------------------------------------------------------------------------
    
    @IBAction func btnBack(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func actionFavorite(sender: UIBarButtonItem) {
        if let pAgenda = self.agenda{
            
            if let state = pAgenda.isFavorite {
                pAgenda.isFavorite = !state
                listAgenda.updateItem(pAgenda)
                if pAgenda.isFavorite == true {
                    self.buttonFavorite.image = UIImage(named: "fav_ful_24")
                }else {
                    self.buttonFavorite.image = UIImage(named: "fav_24")
                }
            }
        }
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func load() {
        if let pAgenda = agenda {
            
            self.labelDate.text = pAgenda.fecha
            self.labelDescription.text = pAgenda.titulo
            self.labelDescription2.text = pAgenda.descripcion
            if let phi = pAgenda.hora_inicio, phf = pAgenda.hora_fin{
                self.labelTime.text = "\(phi) - \(phf)"
            }
            self.labelRoom.text = pAgenda.salon
            
            if let pSponsors = self.sponsors where  pSponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
            }
            
            if let nc = self.tabBarController?.viewControllers![1] as? UINavigationController, tab = nc.topViewController as? AgendaSpeakersViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if let nc = self.tabBarController?.viewControllers![2] as? UINavigationController, tab = nc.topViewController as? AgendaLugarViewController{
                tab.agenda = pAgenda
                tab.sponsors = self.sponsors
            }
            
            if pAgenda.isFavorite == true {
                self.buttonFavorite.image = UIImage(named: "fav_ful_24")
            }else {
                self.buttonFavorite.image = UIImage(named: "fav_24")
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let exhibitorVC = segue.destinationViewController as? ScoreExhibitorViewController {
            exhibitorVC.agenda = self.agenda
        }
    }
}

extension AgendaInfoViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors!.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors![indexPath.row]
        if let img = entry.imagen{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors!.count
        
        let totalCellWidth = 65 * self.sponsors!.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors![indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
}
