//
//  CategoryCompanyViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 7/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import M13Checkbox
import FirebaseStorage
import SlideMenuControllerSwift


class CategoryCompanyViewController: UIViewController{
    
    
    @IBOutlet weak var txtEmpresa: UITextField!
    
    @IBOutlet weak var logo                 :UIImageView!
    @IBOutlet weak var txtGiro              :UITextField!
    @IBOutlet weak var txtCargo             :UITextField!
    @IBOutlet weak var txtDescripcion       :UITextView!
    @IBOutlet weak var categoryCollection   :UICollectionView!
    
    var changeImageProfile  :Bool?
    var categoriesUser      :[Category]?
    var itemSelected        :Category?
    var previewImg          :UIImageView!
    var localPath           :NSURL?
    var imgUser             :UIImage?
    var alert               :UIAlertController?
    var user                :User?
    var idEvent  = Commonds.getIdEvent()
    
    
    var categories  :[Category]     = []
    var imgLogo     :UIImageView    = UIImageView()
    
    var changeLogo = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizerSF = UITapGestureRecognizer()
        tapGestureRecognizerSF.numberOfTapsRequired       = 1
        tapGestureRecognizerSF.numberOfTouchesRequired    = 1
        tapGestureRecognizerSF.addTarget(self, action: #selector(self.openSelct))
        self.logo.userInteractionEnabled = true
        self.logo.addGestureRecognizer(tapGestureRecognizerSF)
        self.loadInfo()
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.uploadProfile),name: "save-image-profile", object: nil)
        
        FactoryCategory.getCategories({ (categories) in
            if categories.count > 0{
                
                self.categories = categories
                let div             = Double(self.categories.count) / 2
                let result          = ceil(div)
                
                let cns = NSLayoutConstraint(item: self.categoryCollection, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: CGFloat(result * 58))
                self.view.addConstraint(cns)
                self.categoryCollection.reloadData()
            }
            
        }) { (error) in
            
        }
    }
    
    // Extraer imagen
    func getImage(type:Int) {
        
        switch type {
        case 1:
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            //imagePickerController.allowsEditing = true
            imagePickerController.delegate = self
            
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            break
        case 2:
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
            //imagePickerController.allowsEditing = true
            imagePickerController.delegate = self
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    func openSelct() {
        
        let alertController = UIAlertController(title: "Cargar imagen", message: "", preferredStyle: .ActionSheet)
        let camera = UIAlertAction(title: "Tomar una fotografía", style: .Default, handler: { (action) -> Void in
            self.getImage(2)
        })
        let galery = UIAlertAction(title: "Escoger desde la galería", style: .Default, handler: { (action) -> Void in
            self.getImage(1)
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .Cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(camera)
        alertController.addAction(cancel)
        alertController.addAction(galery)
        
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = self.view;
        }
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func getInfo(){
        user?.empresa = txtEmpresa.text
        user?.giro = txtGiro.text
        user?.cargo = txtCargo.text
        user?.descripcion_servicios = txtDescripcion.text
    }
    
    func loadInfo(){
        txtEmpresa.text = user?.empresa
        txtGiro.text = user?.giro
        txtCargo.text = user?.cargo
        txtDescripcion.text = user?.descripcion_servicios
    }
    
    func uploadImge(image: UIImage, successBlock: (resp: NSURL?) -> (), andFailure failureBlock: (NSError?) -> ()){
        
        
        if let imageSend = image.resizeWithWidth(450), data = UIImageJPEGRepresentation(imageSend, 0.9){
            
            let storageRef = FIRStorage.storage().reference().child("users/" + NSUUID().UUIDString+".jpeg")
            
            let metaData = FIRStorageMetadata()
            
            
            storageRef.putData(data, metadata: metaData) { metadata, error in
                if (error != nil) {
                    failureBlock(error)
                } else {
                    
                    metadata?.downloadURL()
                    
                    successBlock(resp: metadata?.downloadURL())
                }
            }
        }else {
            failureBlock(nil)
        }
    }
    
    func uploadProfile( notification: NSNotification){
        if let nn = notification.userInfo as? Dictionary<String, AnyObject>{
            
            var send = nn
            if let img = self.imgUser where self.changeImageProfile == true{
                
                self.uploadImge(img, successBlock: { (resp2) in
                    
                    if let plink = resp2?.absoluteString {
                        
                        send["foto"] = plink
                        self.updateUser(send)
                    }
                    
                    }, andFailure: { (error) in
                        Commonds.closeAlert(self.alert!)
                        self.changeImageProfile = false
                        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.showErrorMessageImage), userInfo: nil, repeats: false)
                        
                })
            }else {
                if let puser = user, log = puser.foto {
                    send["foto"] = log
                }else {
                    send["foto"] = ""
                }
                self.updateUser(send)
            }
        }
    }
    
    
    @IBAction func actionSave(sender: UIBarButtonItem) {
        if let _ = self.categories.indexOf({$0.check == true}){
            self.alert = Commonds.loading()
            presentViewController(self.alert!, animated: true, completion: nil)
            
            self.getInfo()
            var list:[Int] = []
            var list2:[Int] = []
            
            for item in self.categories {
                if item.check == true {
                    list.append((item.id!))
                }
            }
            if let listn = self.categoriesUser{
                for item in listn {
                    list2.append(item.id!)
                }
            }
            
            var send:Dictionary<String, AnyObject> = [
                "email": (user?.email)!,
                "nombres": (user?.nombres)!,
                "apellidos": (user?.apellidos)!,
                "id_usuario": (user?.id)!,
                "telefono": (user?.telefono)!,
                "empresa": (user?.empresa)!,
                "giro": (user?.giro)!,
                "cargo": (user?.cargo)!,
                "descripcion_servicios": (user?.descripcion_servicios)!,
                "sobre_mi": list2,
                "intereses": list,
                "evento": Commonds.getIdEvent(),
                "telefono_2": (user?.telefono_2)!,
                "linkedin": (user?.linkedin)!,
                "lastLoginEventId": self.idEvent
            ]
            
            if let fcm = Commonds.getToken() {
                send["fcm_tok"] = fcm
            }
            
            
            
            if let image = self.imgLogo.image where changeLogo == true {
                self.uploadImge(image,successBlock: { (resp) in
                    
                    
                    
                    if let link = resp?.absoluteString {
                        
                        send["logo"] = link
                        NSNotificationCenter.defaultCenter().postNotificationName("save-image-profile", object: nil, userInfo: send)
                    }
                }) { (error) in
                    Commonds.closeAlert(self.alert!)
                    
                    self.changeLogo = false
                    NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.showErrorMessage), userInfo: nil, repeats: false)
                    
                }
                
            } else {
                if let puser = user, log = puser.logo {
                    send["logo"] = log
                }else {
                    send["logo"] = ""
                }
                NSNotificationCenter.defaultCenter().postNotificationName("save-image-profile", object: nil, userInfo: send)
            }
            
        }else {
            Commonds.showMessage("Debe seleccionar al menos una categoria para poder continuar", title: "Espera un momento!", content: self)
        }
    }
    
    
    func showErrorMessage(){
        Commonds.showMessage("Ocurrio un error, intentando subir el logo, nuevamente", title: "Lo sentimos", content: self)
    }
    
    func showErrorMessageImage(){
        Commonds.showMessage("Ocurrio un error, intentando subir tu imagen de perfil, nuevamente", title: "Lo sentimos", content: self)
    }
    
    
    func updateUser(send:Dictionary<String, AnyObject>){
        
        FactoryUser.saveProfile(send, successBlock: { (user) in
            Commonds.closeAlert(self.alert!)
            
            
            NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: #selector(self.closeModal), userInfo: nil, repeats: false)
            
            
        }) { (error) in
            Commonds.closeAlert(self.alert!)
        }
    }
    
    func closeModal(){
        //self.navigationController?.popViewControllerAnimated(false)
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let smc = appDelegate.window?.rootViewController as? SlideMenuController{
            
            if let nc = smc.mainViewController as? UINavigationController, _ = nc.topViewController as? RelationshipViewController {
                
                print("ajamm")
                
                NSNotificationCenter.defaultCenter().postNotificationName("update-true", object: nil)
            }
            
        }else {
            NSNotificationCenter.defaultCenter().postNotificationName("change-window", object: nil, userInfo: nil)
        }
        
        self.dismissViewControllerAnimated(false, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    
    
    // -------------------------------------------------------------
    // MARK: - Collection
    // -------------------------------------------------------------
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.categories.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CategoryCollectionViewCell
        let category = self.categories[indexPath.row]
        cell.name.text = category.nombre
        cell.check.boxType = M13Checkbox.BoxType.Square
        cell.check.stateChangeAnimation = M13Checkbox.Animation.Bounce(M13Checkbox.AnimationStyle.Fill)
        
        if category.check == true{
            self.categories[indexPath.row].check = true
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        } else {
            self.categories[indexPath.row].check = false
            cell.check.setCheckState(M13Checkbox.CheckState.Unchecked, animated: true)
        }
        
        if let user = self.user, sobreMi = user.intereses{
            for item in sobreMi {
                if category.id == item{
                    self.categories[indexPath.row].check = true
                    cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
                }
            }
            
        }else if let _ = self.user{
            self.categories[indexPath.row].check = true
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        }
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell
        
        if cell.check.checkState != M13Checkbox.CheckState.Checked{
            self.categories[indexPath.row].check = true
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        } else {
            self.categories[indexPath.row].check = false
            cell.check.setCheckState(M13Checkbox.CheckState.Unchecked, animated: true)
        }
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((view.frame.width / 2) - 20, 50)
    }
}

extension CategoryCompanyViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if  let imagem = info[UIImagePickerControllerOriginalImage] as? UIImage{
            changeLogo = true
            self.imgLogo.image = imagem
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
