//
//  MessageTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class MessageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgageCell: UIImageView!
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var labBadge: UILabel!
    var nMessages:ListMessages?
    var idEvent:Int?
    
    var person:Person?{
        didSet{
            if let ff = person, photo = ff.foto {
                let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                if !trimmedString.isEmpty{
                    self.imgageCell.loadImageUsingCacheWithUrlString(photo)
                }else {
                    self.imgageCell.image = UIImage(named: "profile_default")
                }
            }else {
                self.imgageCell.image = UIImage(named: "profile_default")
            }
            
            if let name = person?.nombres, lname = person?.apellidos {
                self.nameCell.text = "\(name) \(lname)"
            }
            
            self.load()
        }
    }
    
    
    func load(){
        if let id = person?.id{
            self.updateBadge(nMessages!.items)
            
            if let list = self.nMessages, idEv = self.idEvent where Commonds.isConnectedToNetwork() == true && (self.message.text == nil || self.message.text == ""){
                FactoryRelationship.getMessages(id, listMessage: list,idEvent: idEv,successBlock: { (response) in
                    let message =  response
                    if let mm = message.last?.mMessage{
                        self.message.text = "\(mm)"
                        self.updateBadge(message)
                    }
                }) { (error) in
                    
                }
            }else {
                let message =  nMessages?.getMessagesOfUsers(id)
                if let listMess = message, mm = listMess.last?.mMessage{
                    self.message.text = "\(mm)"
                    self.updateBadge(listMess)
                }
            }
        }
    }
    
    func updateBadge(list: [Message]){
        if let mm = list.last?.mMessage, user = self.person{
            self.message.text = "\(mm)"
            let listMessages = list.filter({$0.state != true && $0.mFrom == user.id})
            let num = listMessages.count
            if num > 0 {
                self.labBadge.hidden = false
                self.labBadge.text = "\(num)"
            }else{
                self.labBadge.hidden = true
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.imgageCell.layer.cornerRadius = self.imgageCell.frame.size.width / 2;
        self.imgageCell.clipsToBounds = true;
        
        self.labBadge.layer.cornerRadius = self.labBadge.frame.size.width / 2;
        self.labBadge.clipsToBounds = true;
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.chageText),name: "chat-notification", object: nil)
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func chageText(notification: NSNotification){
        nMessages?.loadItemsOnStart()
        self.load()
    }

}
