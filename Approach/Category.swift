//
//  Event.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Category{
    
    var id     :Int?
    var nombre :String?
    var check  :Bool!
    
    var categories:[Category] = []
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Category] {
        for item in items {
            var speaker     = Category()
            speaker.id      = item["id"] as? Int
            speaker.nombre  = item["categoria"] as? String
            
            if let check = item["check"] as? Bool{
                speaker.check = check
            }else {
                speaker.check = false
            }
            
            categories.append(speaker)
        }
        return categories
    }
    
    func setCategory(item:Dictionary<String,AnyObject>) -> Category {
        var speaker     = Category()
        speaker.id      = item["id"] as? Int
        speaker.nombre  = item["categoria"] as? String
        
        if let check = item["check"] as? Bool{
            speaker.check = check
        }else {
            speaker.check = false
        }
        
        return speaker
    }
    
    func getItem(index: Int) -> Category {
        return categories[index]
    }
}