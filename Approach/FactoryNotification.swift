//
//  FactoryNotification.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import Alamofire

class FactoryNotification {
    class func getNotifications(successBlock: (notifications: [Notification]) -> (), andFailure failureBlock: (String?) -> ()) {
        Alamofire.request(.GET, [Commonds.URL, "notificaciones", Commonds.getIdEvent()].map{String($0)}.joinWithSeparator("/")).responseJSON { response in
            
            if let JSON = response.result.value, resp = JSON as? [Dictionary<String, AnyObject>] {
                var notification = Notification()
                successBlock(notifications: notification.addItems(resp))
            } else {
                failureBlock("Ocurrio un error, intenta nuevamente")
            }
        }
    }
}