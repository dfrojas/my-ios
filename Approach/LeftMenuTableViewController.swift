//
//  LeftMenuTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 17/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import FirebaseInstanceID
import Firebase

enum LeftMenu: Int {
    case Perfil = 0
    case Relacionamiento
    case Home
    case Agenda
    case Conferencistas
    case Expositores
    case Patrocinadores
    case Separador1
    case Calificar
    case Cambiar
    case Separador2
    case Terminos
    case Politicas
    case Separador3
    case Sesion
}

protocol LeftMenuProtocol : class {
    func changeViewController(menu: LeftMenu)
}

class LeftMenuTableViewController: UITableViewController, LeftMenuProtocol{
    
    var menu:[String] = []
    var perfilViewController: UIViewController!
    var relacionamientoViewController: UIViewController!
    var homeViewController: UIViewController!
    var agendaViewController: UIViewController!
    var conferencistasViewController: UIViewController!
    var expositoresViewController: UIViewController!
    var patrocinadoresViewController: UIViewController!
    var menuActive: [Int] = []
    var listMenu = ListMenu()
    
    var calificarViewController: UIViewController!
    var terminosViewController: UIViewController!
    var politicasViewController: UIViewController!
    var window: UIWindow?
    var userLogin = Commonds.getUser()
    var idEvento = Commonds.getIdEvent()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        menu = ["Perfil","Relacionamiento","Home","Agenda","Conferencistas","Expositores","Patrocinadores", "Separador1", "Calificar", "Cambiar", "Separador2", "Terminos", "Politicas", "Separador3","Sesion"]
        self.tableView.reloadData()
        
        // Uncomment the following line to preserve selection between presentations
        self.clearsSelectionOnViewWillAppear = false

        
        self.navigationController?.navigationBarHidden = true
        self.tableView.layoutMargins = UIEdgeInsetsZero
        self.tableView.separatorInset = UIEdgeInsetsZero
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        let perfilViewController 	= storyboard.instantiateViewControllerWithIdentifier("Perfil") as! UINavigationController
        
        self.perfilViewController 	= perfilViewController
        
        let relacionamientoViewController 	= storyboard.instantiateViewControllerWithIdentifier("Relacionamiento") as! UINavigationController
        
        self.relacionamientoViewController 	= relacionamientoViewController
        
        let homeViewController 	= storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
        
        self.homeViewController 	= homeViewController
        
        let agendaViewController 	= storyboard.instantiateViewControllerWithIdentifier("Agenda") as! UINavigationController
        
        self.agendaViewController 	= agendaViewController
        
        let conferencistasViewController 	= storyboard.instantiateViewControllerWithIdentifier("Conferencistas") as! UINavigationController
        
        self.conferencistasViewController 	= conferencistasViewController
        
        let expositoresViewController 	= storyboard.instantiateViewControllerWithIdentifier("Expositores") as! UINavigationController
        
        self.expositoresViewController 	= expositoresViewController
        
        let patrocinadoresViewController 	= storyboard.instantiateViewControllerWithIdentifier("Patrocinadores") as! UINavigationController
        
        self.patrocinadoresViewController 	= patrocinadoresViewController
        
        let calificarViewController 	= storyboard.instantiateViewControllerWithIdentifier("CalificarNavegation") as! UINavigationController
        
        self.calificarViewController 	= calificarViewController
        
        let terminosViewController 	= storyboard.instantiateViewControllerWithIdentifier("Terminos") as! UINavigationController
        
        self.terminosViewController 	= terminosViewController
        
        let politicasViewController 	= storyboard.instantiateViewControllerWithIdentifier("Politicas") as! UINavigationController
        
        self.politicasViewController 	= politicasViewController
        
        if let items = listMenu.getMenuEvent()?.items{
            self.menuActive = items
        }
        
        FactoryEvent.menuActive(listMenu, successBlock: { (menu) in
            
            
            if let items = menu.items{
                self.menuActive = items
                self.tableView.reloadData()
            }
            }) { (error) in
                
        }
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.reloadTable),name: "reload-menu", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.openRelationship),name: "open_relationship", object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.imageHeaderView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 160)
        self.view.layoutIfNeeded()
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        if indexPath.row == 0 {
            return 210;
        } else if indexPath.row == 7 || indexPath.row == 10 || indexPath.row == 13{
            return 30
        }
        
        
        
        var count = menuActive.count
        if count > 0{
            
            count = menuActive.count - 1
            
            for index in 0...count{
                let item = menuActive[index]
                
                if item == 1 && indexPath.row == 1{
                    return 55;
                }else if item == 2 && indexPath.row == 3{
                    return 55;
                }else if item == 3 && indexPath.row == 4{
                    return 55;
                }else if item == 4 && indexPath.row == 5{
                    return 55;
                }else if item == 5 && indexPath.row == 8{
                    return 55;
                }else if item == 6 && indexPath.row == 6{
                    return 55;
                }
            }
        }
        
        if indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 4 || indexPath.row == 5 || indexPath.row == 8 || indexPath.row == 6 {
            return 0
        }else {
            return 55
        }
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return menu.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if indexPath.row == 7 || indexPath.row == 10 || indexPath.row == 13{
            let cellSeparator = tableView.dequeueReusableCellWithIdentifier(menu[indexPath.row], forIndexPath: indexPath)
            cellSeparator.layoutMargins = UIEdgeInsetsZero
            return cellSeparator
        }
        
        let cell = tableView.dequeueReusableCellWithIdentifier(menu[indexPath.row], forIndexPath: indexPath) as! LeftMenuTableViewCell
        
        cell.layoutMargins = UIEdgeInsetsZero
        
        if indexPath.row == 0 {
            
            
            if let names = userLogin?.nombres {
                cell.labelNombre.text = names
                cell.labelCargo!.text = ""
            }
            
            if let lastName = userLogin?.apellidos, names = userLogin?.nombres{
                cell.labelNombre.text = "\(names) \(lastName)"
            }
            
            if let name = Commonds.getNameEvent(){
                cell.labNombreEvento.text = name
            }
            
            if let ff = userLogin, photo = ff.foto {
                let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                
                cell.imgSetting!.layer.cornerRadius = cell.imgSetting!.frame.size.width / 2;
                cell.imgSetting!.clipsToBounds      = true;
                cell.imgSetting!.layer.borderWidth  = 3;
                cell.imgSetting!.layer.borderColor  = UIColor(rgba: "#7ac1bf").CGColor
                cell.imgSetting!.contentMode = .ScaleAspectFill
                
                if !trimmedString.isEmpty{
                    cell.imgSetting!.loadImageUsingCacheWithUrlString(trimmedString)
                }
            }
            
            let tapGestureRecognizer = UITapGestureRecognizer(target:self, action:#selector(LeftMenuTableViewController.openProfile))
            
            cell.imageConfig!.userInteractionEnabled = true
            cell.imageConfig!.addGestureRecognizer(tapGestureRecognizer)
            if let work = userLogin?.cargo {
                cell.labelCargo?.text = work
            }
        }
        return cell
    }
    
    func openRelationship(){
        NSNotificationCenter.defaultCenter().postNotificationName("update-true", object: nil)
        self.slideMenuController()?.changeMainViewController(self.relacionamientoViewController, close: true)
    }
    
    func reloadTable(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.tableView.reloadData()
        })
    }
    
    func openProfile(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            
            self.closeLeft()
            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Perfil") as! UINavigationController
            rootVC.view.frame = UIScreen.mainScreen().bounds
            self.presentViewController(rootVC, animated: true, completion: nil)
        })
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if let menu = LeftMenu(rawValue: indexPath.item) where indexPath.row != 0{
            self.changeViewController(menu)
        }
        
        if indexPath.row == 14 {
            self.logout()
        }
        
        if indexPath.row == 9 {
            self.changeEvent()
        }
    }
    
    func logout()  {
        do {
            
            let fileManager = NSFileManager.defaultManager()
            if NSUserDefaults.standardUserDefaults().objectForKey("LIAccessToken") != nil {
                NSUserDefaults.standardUserDefaults().removeObjectForKey("LIAccessToken")
            }
            
            //Fire
            if let refreshedToken = FIRInstanceID.instanceID().token(){
                let defaults = NSUserDefaults.standardUserDefaults()
                defaults.setObject(refreshedToken, forKey: "fcm_token")
                print("InstanceID token: \(refreshedToken)")
                
                FIRMessaging.messaging().connectWithCompletion { (error) in
                    if (error != nil) {
                        print("Unable to connect with FCM. \(error)")
                    } else {
                        print("Connected to FCM.")
                    }
                }
                
            }
            
            
            try fileManager.removeItemAtURL(User.fileUrl)
            
            Commonds.setNotification(Commonds.getNotification(), plus: false)
            Commonds.setNotificationMessages(Commonds.getNotificationMessages(), plus: false)
            
            UIApplication.sharedApplication().applicationIconBadgeNumber = 0
            
            self.closeLeft()
            self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("EventNavigationController") as! UINavigationController
                rootVC.view.frame = UIScreen.mainScreen().bounds
                appDelegate.window?.rootViewController = rootVC
            })
            
        }
        catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    func parseDate(string: String) -> NSDate?{
        let parse = NSDateFormatter()
        parse.dateFormat = "yyyy-MM-dd"
        return parse.dateFromString(string)
    }
    
    func changeEvent(){
        self.closeLeft()
        self.closeRight()
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(0, forKey: "id_event")
        self.slideMenuController()?.closeLeft()
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyboard.instantiateViewControllerWithIdentifier("EventNavigationController") as! UINavigationController
            mainViewController.view.frame = UIScreen.mainScreen().bounds
            appDelegate.window?.rootViewController = mainViewController
        })
    }
    
    func changeViewController(menu: LeftMenu) {
        switch menu {
        case .Perfil:
            self.slideMenuController()?.changeMainViewController(self.perfilViewController, close: true)
        case .Relacionamiento:
            
            
            if let fecha_vida_chat = Commonds.getFechaVidaChat(), fecha_fin = self.parseDate(fecha_vida_chat){
                if fecha_fin.compare(NSDate()) == NSComparisonResult.OrderedDescending{
                    
                    let defaults = NSUserDefaults.standardUserDefaults()
                    
                    if let id = self.userLogin?.id, _ = defaults.objectForKey("relation_\(id)_\(idEvento)"){
                        NSNotificationCenter.defaultCenter().postNotificationName("update-true", object: nil)
                        self.slideMenuController()?.changeMainViewController(self.relacionamientoViewController, close: true)
                    }else {
                        self.closeLeft()
                        self.closeRight()
                        self.performSegueWithIdentifier("sw_presentation", sender: self)
                    }
                }else {
                    self.slideMenuController()?.closeLeft()
                    
                    let listMessages = ListMessages()
                    listMessages.deleteMessagesRom()
                    
                    Commonds.showMessage("No es posible entrar al chat de este evento, debido a que ya paso", title: "Lo sentimos", content: self)
                }
            }
            
            
        case .Home:
            self.slideMenuController()?.changeMainViewController(self.homeViewController, close: true)
        case .Agenda:
            self.slideMenuController()?.changeMainViewController(self.agendaViewController, close: true)
        case .Conferencistas:
            self.slideMenuController()?.changeMainViewController(self.conferencistasViewController, close: true)
        case .Expositores:
            self.slideMenuController()?.changeMainViewController(self.expositoresViewController, close: true)
        case .Patrocinadores:
            self.slideMenuController()?.changeMainViewController(self.patrocinadoresViewController, close: true)
        case .Calificar:
            self.slideMenuController()?.changeMainViewController(self.calificarViewController, close: true)
        case .Terminos:
            self.slideMenuController()?.changeMainViewController(self.terminosViewController, close: true)
        case .Politicas:
            self.slideMenuController()?.changeMainViewController(self.politicasViewController, close: true)
        default:
            break
            
        }
    }
}

