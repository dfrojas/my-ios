//
//  PopAnimator.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 4/09/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class PopAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let duration    = 1.0
    var presenting  = true
    var originFrame = CGRect.zero
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?)-> NSTimeInterval {
        return duration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView()!
        let toView = transitionContext.viewForKey(UITransitionContextToViewKey)!
        let herbView = presenting ? toView : transitionContext.viewForKey(UITransitionContextFromViewKey)!
        
        let initialFrame = presenting ? originFrame : herbView.frame
        let finalFrame = presenting ? herbView.frame : originFrame
        
        let xScaleFactor = presenting ?
            initialFrame.width / finalFrame.width :
            finalFrame.width / initialFrame.width
        
        let yScaleFactor = presenting ?
            initialFrame.height / finalFrame.height :
            finalFrame.height / initialFrame.height
        
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        if presenting {
            herbView.alpha = 1
            herbView.hidden = false
            
            herbView.transform = scaleTransform
            herbView.center = CGPoint(
                x: CGRectGetMidX(initialFrame),
                y: CGRectGetMidY(initialFrame))
            herbView.clipsToBounds = true
        }
        
        containerView.addSubview(toView)
        containerView.bringSubviewToFront(herbView)
        
        var duration2: CGFloat = 0.4
        
        if !presenting{
            duration2 = 1
        }else{
            duration2 = 0.4
        }
        
        
        let round = CABasicAnimation(keyPath: "setCornerRadius")
        round.fromValue = presenting ? 1000.0: 0.0
        round.toValue = presenting ? 0.0 : 1000.0
        round.duration = duration / 2
        herbView.layer.addAnimation(round, forKey: nil)
        herbView.layer.cornerRadius = presenting ? 0.0 : 1000.0
        
        UIView.animateWithDuration(duration, delay:0.0,usingSpringWithDamping: duration2, initialSpringVelocity: 0.0, options: [], animations: {
            herbView.transform = self.presenting ?
                CGAffineTransformIdentity : scaleTransform
            herbView.center = CGPoint(x: CGRectGetMidX(finalFrame),
                y: CGRectGetMidY(finalFrame))
            
            }, completion:{_ in
                
                if !self.presenting{
                    herbView.alpha = 0
                    herbView.hidden = true
                }
                
                transitionContext.completeTransition(true)
        })
        
    }
    
}
