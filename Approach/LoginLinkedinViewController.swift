//
//  LoginLinkedinViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class LoginLinkedinViewController: UIViewController {
    
    
    @IBOutlet weak var forgetPassword: UILabel!
    @IBOutlet weak var termOfService: UILabel!
    @IBOutlet weak var privacityPolicy: UILabel!
    var event: Event?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.changeWindow),name: "change-window", object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func payInAction(sender: UIButton) {
        self.performSegueWithIdentifier("sw_register_linkedin_reveal", sender: self)
    }
    
    @IBAction func cancelAction(sender: UIButton) {
        
    }
    
    func changeWindow(){
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
        
        //self.dismissViewControllerAnimated(true, completion: nil)
        
        mainViewController.view.frame = UIScreen.mainScreen().bounds
        let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
        let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
        let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
        
        SlideMenuOptions.contentViewScale = 1
        //SlideMenuOptions.hideStatusBar = false;
        appDelegate.window?.rootViewController = slideMenuController

    }
    
    /*
    // ------------------------------------------------------------------------------------
    // MARK: - Navigation
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let homeViewController = segue.destinationViewController as? SWRevealViewController{
            if let hh = homeViewController. as? HomeViewController{
                hh.event = self.event
            }
        }
    }
 */

}
