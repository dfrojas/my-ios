//
//  Notification.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

struct Notification{
    
    var id            :String?
    var fecha         :String?
    var texto         :String?
    var evento        :Int?
    var notifications :[Notification] = []
    
    
    func setEvent(item:Dictionary<String,AnyObject>) -> Notification {
        var nn = Notification()
        nn.id       = item["id"] as? String
        nn.fecha    = item["fecha"] as? String
        nn.texto    = item["texto"] as? String
        nn.evento   = item["evento"] as? Int
        return nn
    }
    
    mutating func addItems(items:[Dictionary<String,AnyObject>]) -> [Notification] {
        self.notifications = []
        
        for item in items {
            var nn = Notification()
            nn.id       = item["id"] as? String
            nn.fecha    = item["fecha"] as? String
            nn.texto    = item["texto"] as? String
            nn.evento   = item["evento"] as? Int
            notifications.append(nn)
            
        }
        return notifications
    }
}