//
//  speakersTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class SpeakersTableViewController: UIViewController{
    
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!
    
    
    var resultSearchController = UISearchController()
    var speaker                = Speaker()
    var speakers               = [Speaker]()
    var filteredSpeaker        = [Speaker]()
    var sponsors:[Sponsor]     = []
    
    var itemSelectedSponsor:Sponsor?
    var itemSelected :Speaker?
    let refresController = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true
        
        self.refresController.backgroundColor = UIColor.whiteColor()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.buttonNotifications),name: "reload-notifications", object: nil)
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.searchBar.placeholder = "Buscar"
        
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.backgroundColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.sizeToFit()
        self.resultSearchController.searchBar.setValue("Cancelar", forKey: "_cancelButtonText")
        self.tableView.tableHeaderView = self.resultSearchController.searchBar
        
        self.refresController.addTarget(self, action: #selector(self.load), forControlEvents: .ValueChanged)
        self.tableView.addSubview(refresController)
        load()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        buttonNotifications()
    }
    
    override func viewWillDisappear(animated: Bool){
        super.viewWillDisappear(animated)
        if self.resultSearchController.active == true{
            self.resultSearchController.active = !self.resultSearchController.active
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    func load(){
        FactorySpeaker.speakers("\(Commonds.getIdEvent())").responseJSON { (response) in
            
            self.refresController.endRefreshing()
            
            if let JSON = response.result.value, data = JSON as? [Dictionary<String, AnyObject>]{
                self.speakers = self.speaker.addItems(data)
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.tableView.reloadData()
                    
                    let cells = self.tableView.visibleCells
                    let tableHeight: CGFloat = self.tableView.bounds.size.height
                    
                    for i in cells {
                        let cell: UITableViewCell = i as UITableViewCell
                        cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
                    }
                    
                    var index = 0
                    
                    for a in cells {
                        let cell: UITableViewCell = a as UITableViewCell
                        UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.TransitionNone, animations: {
                            cell.transform = CGAffineTransformMakeTranslation(0, 0);
                            }, completion: nil)
                        
                        index += 1
                    }
                })
                
            }else {
                Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
            }
        }
        
        FactorySponsor.getSponsors({ (sponsors) in
            if sponsors.count > 0{
                if sponsors.count > 0{
                    self.collectionConstraintBottom.constant = 0
                    self.sponsors = sponsors
                    self.collectionSponsor.reloadData()
                }else {
                    self.collectionConstraintBottom.constant = -75
                }
                self.collectionSponsor.reloadData()
            }
        }) { (error) in
            Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
        }
    }
    
    func buttonNotifications(){
        let badgeButton : MIBadgeButton = MIBadgeButton(frame: CGRectMake(0, 0, 20, 20))
        badgeButton.setImage(UIImage(named: "noti_24"), forState: .Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        let num = Commonds.getNotification()
        
        if num > 0{
            badgeButton.badgeString = "\(num)"
        }else {
            badgeButton.badgeString = nil
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.actionNotifications))
        badgeButton.userInteractionEnabled = true
        badgeButton.addGestureRecognizer(tapGestureRecognizer3)
    }

    
    func actionNotifications() {
        Commonds.setNotification(Commonds.getNotification(), plus: false)
        self.slideMenuController()?.openRight()
        self.buttonNotifications()
    }
    
    
    // ------------------------------------------------------------------------------------
    // MARK: searchController
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let pSponsor = segue.destinationViewController as? SpeakerViewController {
            pSponsor.speaker = self.itemSelected
        }
    }
}

extension SpeakersTableViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors[indexPath.row]
        if let img = entry.imagen{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors.count
        
        let totalCellWidth = 65 * self.sponsors.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors[indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
    
}

extension SpeakersTableViewController: UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredSpeaker = self.speakers.filter({ (speaker) -> Bool in
            let stringMatch = speaker.nombre_conferencista!.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            return stringMatch != nil
        })
        
        self.tableView.reloadData()
    }
}

extension SpeakersTableViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            itemSelected = self.filteredSpeaker[indexPath.row]
            //self.resultSearchController.active = false
        }else {
            itemSelected = self.speakers[indexPath.row]
        }
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        self.performSegueWithIdentifier("sw_speaker", sender: self)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.resultSearchController.active{
            return filteredSpeaker.count
        }else {
            return speakers.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! SpeakerTableViewCell
        var entry = speakers[indexPath.row]
        
        if self.resultSearchController.active{
            entry = self.filteredSpeaker[indexPath.row]
        }else {
            entry = self.speakers[indexPath.row]
        }
        
        cell.nameCell.text = entry.nombre_conferencista
        cell.company.text = entry.empresa_conferencista
        cell.position.text = entry.cargo_conferencista
        
        if let img = entry.foto{
            cell.imgageCell.layer.cornerRadius = cell.imgageCell.frame.size.width / 2;
            cell.imgageCell.clipsToBounds = true;
            cell.imgageCell.loadImageUsingCacheWithUrlString(img)
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 58
    }
}


