//
//  SearchBar.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 8/09/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class SearchBar: UISearchBar {
    
    
    var preferredFont: UIFont!
    
    var preferredTextColor: UIColor!

    
    init(frame: CGRect, font: UIFont, textColor: UIColor) {
        super.init(frame: frame)
        
        self.frame = frame
        preferredFont = font
        preferredTextColor = textColor
        
        searchBarStyle = UISearchBarStyle.Prominent
        translucent = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
