//
//  ExhibitorTableViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 13/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class SpeakerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imgageCell: UIImageView!
    @IBOutlet weak var nameCell: UILabel!
    @IBOutlet weak var position: UILabel!
    @IBOutlet weak var company: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
