//
//  AgendaViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 15/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import MIBadgeButton_Swift

class AgendaViewController: UIViewController{
    
    // ------------------------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------------------------
    
    let listAgenda = ListAgenda()
    
    // ------------------------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------------------------

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var tableAgenda: UITableView!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var header: UILabel!
    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var collectionSponsor: UICollectionView!
    
    @IBOutlet weak var constraintHeightFilter: NSLayoutConstraint!
    
    @IBOutlet weak var collectionConstraintBottom: NSLayoutConstraint!

    
    var agenda          = Agenda()
    var agends          : [Agenda] = []
    var filteredAgendas : [Agenda] = []
    var auxAgendas      : [Agenda] = []
    var sponsors        :[Sponsor] = []
    var userLogin       = Commonds.getUser()
    var isFilter        = false
    var isFavorites     = false
    
    var itemSelected:Agenda?
    let refresController = UIRefreshControl()
    
    // ------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.header.text = "Todos"
        
        self.picker.hidden = true
        
        self.picker.dataSource = self;
        self.picker.delegate = self;
        
        self.refresController.backgroundColor = UIColor.whiteColor()
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired = 1
        tapGestureRecognizer.numberOfTouchesRequired = 1
        tapGestureRecognizer.addTarget(self, action: #selector(self.toggleDatePicker))
        self.header.userInteractionEnabled = true
        self.header.addGestureRecognizer(tapGestureRecognizer)
        self.tableAgenda.addSubview(self.refresController)
        self.viewContainer.layer.cornerRadius = 5
        self.segmentController.layer.borderColor = UIColor.whiteColor().CGColor
        self.segmentController.layer.borderWidth = 1
        self.segmentController.layer.masksToBounds = true
        self.segmentController.layer.cornerRadius = 5
        
        buttonNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.buttonNotifications),name: "reload-notifications", object: nil)
        
        self.refresController.addTarget(self, action: #selector(self.load), forControlEvents: .ValueChanged)
        self.tableAgenda.hidden = true
        self.indicator.startAnimating()
        self.load()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        buttonNotifications()
        if isFavorites{
            
            self.listAgenda.loadItemsOnStart()
            self.agends = listAgenda.getFavorites()
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableAgenda.reloadData()
                self.picker.reloadAllComponents()
            })
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        guard let flowLayoutSponsor = collectionSponsor.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        flowLayoutSponsor.invalidateLayout()
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: IBAction
    // ------------------------------------------------------------------------------------
    
    @IBAction func actionFilter(sender: UIButton) {
        self.toggleDatePicker()
    }
    
    @IBAction func actionSegmentController(sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0 {
            self.constraintHeightFilter.constant = 38
            isFavorites = false
            print("click agenda")
            
            load()
        }else {
            
            self.constraintHeightFilter.constant = 0
            self.picker.hidden = true
            isFavorites = true
            listAgenda.loadItemsOnStart()
            self.agends = listAgenda.getFavorites()
            self.filteredAgendas = self.agends
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableAgenda.reloadData()
                self.picker.reloadAllComponents()
            })
        }
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    
    func buttonNotifications(){
        let badgeButton : MIBadgeButton = MIBadgeButton(frame: CGRectMake(0, 0, 20, 20))
        badgeButton.setImage(UIImage(named: "noti_24"), forState: .Normal)
        badgeButton.setTitleColor(UIColor.blackColor(), forState: UIControlState.Normal)
        
        let num = Commonds.getNotification()
        
        if num > 0{
            badgeButton.badgeString = "\(num)";
        }else {
            badgeButton.badgeString = nil
        }
        
        let barButton : UIBarButtonItem = UIBarButtonItem(customView: badgeButton)
        self.navigationItem.rightBarButtonItem = barButton
        let tapGestureRecognizer3 = UITapGestureRecognizer()
        tapGestureRecognizer3.numberOfTapsRequired = 1
        tapGestureRecognizer3.numberOfTouchesRequired = 1
        tapGestureRecognizer3.addTarget(self, action: #selector(self.actionNotifications))
        badgeButton.userInteractionEnabled = true
        badgeButton.addGestureRecognizer(tapGestureRecognizer3)
    }

    
    func actionNotifications() {
        Commonds.setNotification(Commonds.getNotification(), plus: false)
        self.slideMenuController()?.openRight()
        self.buttonNotifications()
    }
    
    func load()  {
        
        if segmentController.selectedSegmentIndex == 0 {
        
        FactorySponsor.getSponsors({ (sponsors) in
            if sponsors.count > 0{
                self.collectionConstraintBottom.constant = 0
                self.sponsors = sponsors
                self.collectionSponsor.reloadData()
            }else {
                self.collectionConstraintBottom.constant = -75
            }
        }) { (error) in
            Commonds.showMessage("Ocurrio un error, intenta nuevamente", title: "Lo sentimos", content: self)
        }
        
        FactoryAgenda.agends({ (agendas) in
            self.agends = agendas
            self.getItemsPicker()
            self.tableAgenda.hidden = false
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.refresController.endRefreshing()
                self.indicator.stopAnimating()
                self.picker.reloadAllComponents()
                if let text = self.header.text where text != "Todos" && !text.isEmpty{
                    self.filteredAgendas = self.agends.filter({ (event) -> Bool in
                        let stringMatch = event.fecha!.lowercaseString.rangeOfString(text.lowercaseString)
                        return stringMatch != nil
                    })
                    self.isFilter = true
                }else {
                    self.isFilter = false
                }
                self.tableAgenda.reloadData()
                
            })
        }) { (error) in
            self.listAgenda.loadItemsOnStart()
            self.agends = self.listAgenda.getAgendaEvent()
            self.tableAgenda.hidden = false
            self.getItemsPicker()
            Commonds.showMessage(error!, title: "Lo sentimos", content: self)
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.refresController.endRefreshing()
                self.indicator.stopAnimating()
                self.picker.reloadAllComponents()
                if let text = self.header.text where text != "Todos" && !text.isEmpty{
                    self.filteredAgendas = self.agends.filter({ (event) -> Bool in
                        let stringMatch = event.fecha!.lowercaseString.rangeOfString(text.lowercaseString)
                        return stringMatch != nil
                    })
                    self.isFilter = true
                }else {
                    self.isFilter = false
                }
                self.tableAgenda.reloadData()
            })
        }
        }else {
            self.refresController.endRefreshing()
            self.indicator.stopAnimating()
            self.tableAgenda.hidden = false
            self.agends = listAgenda.getFavorites()
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.tableAgenda.reloadData()
                self.picker.reloadAllComponents()
            })
            
        }
    }
    
    func updateSearchResultsForSearchController(text: String) {
        self.filteredAgendas = self.agends.filter({ (event) -> Bool in
            let stringMatch = event.fecha!.lowercaseString.rangeOfString(text.lowercaseString)
            return stringMatch != nil
        })
        
        self.isFilter = true
        self.tableAgenda.reloadData()
        self.toggleDatePicker()
    }
    
    func toggleDatePicker()  {
        if self.picker.hidden {
            fadeInDatePicker()
        } else {
            fadeOutDatePicker()
        }
    }
    
    func existItem(text:String?) -> Bool{
        if self.auxAgendas.isEmpty {
            return false
        }
        for item in self.auxAgendas {
            if item.fecha == text {
                return true
            }
        }
        return false
    }
    
    func getItemsPicker(){
        let aux = self.agends
        for item in aux{
            if !self.existItem(item.fecha){
                self.auxAgendas.append(item)
            }
        }
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: - Animation
    // ------------------------------------------------------------------------------------
    
    func fadeInDatePicker() {
        self.picker.alpha = 0
        self.picker.hidden = false
        
        UIView.animateWithDuration(1){ () -> Void in
            self.picker.alpha = 1
            self.tableAgenda.alpha = 0
        }
    }
    
    func fadeOutDatePicker() {
        self.picker.alpha = 1
        self.picker.hidden = false
        
        UIView.animateWithDuration(1, animations: {
            self.picker.alpha = 0
            self.tableAgenda.alpha = 1
        }) { (completed) in
            if completed {
                self.picker.hidden = true
            }
        }
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: Navigation
    // ------------------------------------------------------------------------------------
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        let tabVC = segue.destinationViewController as! UITabBarController
        let navVc = tabVC.viewControllers!.first as! UINavigationController
        
        if let vc = navVc.viewControllers.first as? AgendaInfoViewController {
            if let agenda = self.itemSelected{
                vc.agenda = agenda
                vc.sponsors = self.sponsors
            }
        }
        else if let vc = navVc.viewControllers.first as? AgendaLugarViewController {
            if let agenda = self.itemSelected{
                vc.agenda = agenda
                vc.sponsors = self.sponsors
            }
        }
        else if let vc = navVc.viewControllers.first as? AgendaSpeakersViewController {
            if let agenda = self.itemSelected{
                vc.agenda = agenda
                vc.sponsors = self.sponsors
            }
        }
    }

}

extension AgendaViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if self.isFilter{
            itemSelected = self.filteredAgendas[indexPath.row]
        }else {
            itemSelected = self.agends[indexPath.row]
        }
        
        self.tableAgenda.deselectRowAtIndexPath(indexPath, animated: true)
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(self.itemSelected?.id!, forKey: "id_agenda")
        self.performSegueWithIdentifier("sw_detail_agenda", sender: self)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.isFilter == true && self.isFavorites != true {
            return filteredAgendas.count
        }else {
            return agends.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AgendaTableViewCell
        var entry:Agenda?
        
        
        if self.isFilter == true && self.isFavorites != true{
            entry = self.filteredAgendas[indexPath.row]
        }else if indexPath.row <= agends.count && agends.count > 0{
            entry = self.agends[indexPath.row]
        }
        
        if let fi = entry?.hora_inicio,ff = entry?.hora_fin {
            cell.labelTime.text  = "\(fi) - \(ff)"
        }
        
        if let titulo = entry?.titulo{
            cell.labelDescription.text = titulo
        }
        
        var names = ""
        
        //let speakers = entry?.speakers
        
        if let id = entry?.id where isFavorites == true {
            
            FactoryAgenda.getAgenda(id, successBlock: { (agenda) in
                
                if let index = self.agends.indexOf({$0.id == id && $0.idUsiario == self.userLogin?.id}){
                    self.agends[index] = agenda
                }
                
                if agenda.speakers.count > 0{
                    for item in agenda.speakers{
                        names =  item.nombre_conferencista! + ", " + names
                        let auxString = String(names.characters.dropLast())
                        cell.labelSpeakers.text = String(auxString.characters.dropLast())
                    }
                }else {
                    cell.labelSpeakers.text = "Sin conferencistas"
                }
                
                }, andFailure: { (error) in
                    
            })
        }else {
            
            if let speakers = entry?.speakers where speakers.count > 0{
                for item in speakers{
                    names =  item.nombre_conferencista! + ", " + names
                }
                
                let auxString = String(names.characters.dropLast())
                cell.labelSpeakers.text = String(auxString.characters.dropLast())
                
            }else {
                cell.labelSpeakers.text = "Sin conferencistas"
            }
            
        }
        
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animateWithDuration(0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
                UIView.animateWithDuration(0.1, animations: {
                    cell.layer.transform = CATransform3DMakeScale(1,1,1)
                })
        })
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 73
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
}

extension AgendaViewController: UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.auxAgendas.count + 1;
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        self.header.text = "Todos"
        
        if row == 0{
            return "Todos"
        }else {
            self.header.text = self.auxAgendas[row - 1].fecha
            return self.auxAgendas[row - 1].fecha
        }
        
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        if row > 0 && self.auxAgendas.count >= row && isFavorites != true{
            self.updateSearchResultsForSearchController(self.header.text!)
        }else {
            self.isFilter = false
            self.tableAgenda.reloadData()
            self.toggleDatePicker()
        }
    }
}
extension AgendaViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sponsors.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CellCollectionViewCell
        
        let entry = self.sponsors[indexPath.row]
        if let img = entry.imagen{
            cell.imageView.layer.cornerRadius = cell.imageView.frame.size.width / 2;
            cell.imageView.clipsToBounds = true;
            cell.imageView.loadImageUsingCacheWithUrlString(img)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        
        let count = self.sponsors.count
        
        let totalCellWidth = 65 * self.sponsors.count
        let totalSpacingWidth = 10 * (count - 1)
        
        let leftInset = (self.collectionSponsor.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2;
        let rightInset = leftInset
        
        if collectionSponsor.frame.width >= CGFloat((totalSpacingWidth + totalCellWidth)){
            return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
        }else {
            return UIEdgeInsetsMake(0, 10, 0, 10)
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let viewSponsor = self.storyboard?.instantiateViewControllerWithIdentifier("CircularTransition") as? TransitionSponsorViewController {
            viewSponsor.sponsor = self.sponsors[indexPath.row]
            self.navigationController?.radialPushViewController(viewSponsor, duration: 0.2, startFrame: collectionView.frame, transitionCompletion: nil)
        }
    }
    
}
