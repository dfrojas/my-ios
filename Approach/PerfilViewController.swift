//
//  PerfilViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 3/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import M13Checkbox
import SlideMenuControllerSwift

class PerfilViewController: UIViewController{

    
    
    @IBOutlet weak var txtName: UITextField!
    
    @IBOutlet weak var txtLastName: UITextField!
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var txtTelephone: UITextField!
    
    @IBOutlet weak var company: UIBarButtonItem!
    
    @IBOutlet weak var categoryCollection: UICollectionView!
    
    @IBOutlet weak var imageAction: UIImageView!
    
    @IBOutlet weak var txtTelephone2: UITextField!
    
    @IBOutlet weak var txtLinkedin: UITextField!
    
    @IBOutlet weak var btnClose: UIBarButtonItem!
    
    
    var user:User? = Commonds.getUser()
    var chageImage = false
    var categories:[Category] = []
    var itemSelected:Category?
    var urlLinkedin: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGestureRecognizerSF = UITapGestureRecognizer()
        tapGestureRecognizerSF.numberOfTapsRequired       = 1
        tapGestureRecognizerSF.numberOfTouchesRequired    = 1
        tapGestureRecognizerSF.addTarget(self, action: #selector(self.openSelct))
        self.imageAction.userInteractionEnabled = true
        self.imageAction.addGestureRecognizer(tapGestureRecognizerSF)
        
        self.img.layer.cornerRadius = self.img.frame.size.width / 2;
        self.img.clipsToBounds = true;
        self.img.layer.borderWidth = 3;
        self.img.layer.borderColor = UIColor(rgba: "#7ac1bf").CGColor
        
        self.imageAction.layer.cornerRadius = self.imageAction.frame.size.width / 2;
        self.imageAction.clipsToBounds = true;
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if let _ = appDelegate.window?.rootViewController as? SlideMenuController{
            self.btnClose.enabled = true
        } else {
            self.btnClose.enabled = false
        }
        
        
        
        if let ff = user, photo = ff.foto {
            let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            
            if !trimmedString.isEmpty{
                self.img.loadImageUsingCacheWithUrlString(trimmedString)
            }
        }
        
        if let names = user?.nombres {
            self.txtName.text = names
        }
        
        if let lastName = user?.apellidos{
            self.txtLastName.text = lastName
        }
        
        if let email = user?.email{
            self.txtEmail.text = email
        }
        
        if let telephone = user?.telefono{
            self.txtTelephone.text = telephone
        }
        
        if let telephone2 = user?.telefono_2{
            self.txtTelephone2.text = telephone2
        }
        
        if let link = urlLinkedin{
            self.txtLinkedin.text = link
        }else if let linke = user?.linkedin{
            self.txtLinkedin.text = linke
        }
        
        FactoryCategory.getCategories({ (categories) in
            
            if categories.count > 0{
                
                self.categories = categories
                
                let div             = Double(self.categories.count) / 2
                let result          = ceil(div)
                
                let cns = NSLayoutConstraint(item: self.categoryCollection, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: CGFloat(result * 58))
                self.view.addConstraint(cns)
                self.categoryCollection.reloadData()
            }
            
            }) { (error) in
            
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.user = Commonds.getUser()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSizeMake((view.frame.width / 2) - 20, 50)
    }
    
    
    @IBAction func actionClose(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func actionCompany(sender: UIBarButtonItem) {
        
        if let _ = self.categories.indexOf({$0.check == true}){
            
            self.performSegueWithIdentifier("sw_company", sender: self)
        }else {
            Commonds.showMessage("Debe seleccionar al menos una categoría para poder continuar", title: "Espera un momento!", content: self)
        }
        
        
    }
    
    func openSelct() {
        
        let alertController = UIAlertController(title: "Cargar imagen", message: "", preferredStyle: .ActionSheet)
        let camera = UIAlertAction(title: "Tomar una fotografía", style: .Default, handler: { (action) -> Void in
            self.getImage(2)
        })
        let galery = UIAlertAction(title: "Escoger desde la galería", style: .Default, handler: { (action) -> Void in
            self.getImage(1)
        })
        
        let cancel = UIAlertAction(title: "Cancelar", style: .Cancel, handler: { (action) -> Void in
            
        })
        
        alertController.addAction(camera)
        alertController.addAction(cancel)
        alertController.addAction(galery)
        
        if let presenter = alertController.popoverPresentationController {
            presenter.sourceView = self.view;
        }
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // Extraer imagen
    func getImage(type:Int) {
        
        switch type {
        case 1:
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            //imagePickerController.allowsEditing = true
            imagePickerController.delegate = self
            
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            break
        case 2:
            let imagePickerController = UIImagePickerController()
            imagePickerController.sourceType = UIImagePickerControllerSourceType.Camera
            //imagePickerController.allowsEditing = true
            imagePickerController.delegate = self
            self.presentViewController(imagePickerController, animated: true, completion: nil)
            break
        default:
            break
        }
    }
    
    func getInfo(){
        user?.nombres = txtName.text
        user?.apellidos = txtLastName.text
        user?.email = txtEmail.text
        user?.telefono = txtTelephone.text
        user?.telefono_2 = txtTelephone2.text
        user?.linkedin = txtLinkedin.text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransitionToSize(size, withTransitionCoordinator: coordinator)
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
    
    // -------------------------------------------------------------
    // MARK: - Navigation
    // -------------------------------------------------------------
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        var list:[Category] = []
        
        for item in self.categories {
            if item.check == true{
                list.append(item)
            }
        }
        
        self.getInfo()
        
        if let detailViewController = segue.destinationViewController as? CategoryCompanyViewController{
            detailViewController.user = self.user
            detailViewController.categoriesUser = list
            detailViewController.changeImageProfile = self.chageImage
            detailViewController.imgUser = self.img.image
        }
    }
}

extension PerfilViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if  let imagem = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.img.image = imagem
            self.chageImage = true
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}

extension PerfilViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.categories.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CategoryCollectionViewCell
        let category = self.categories[indexPath.row]
        cell.name.text = category.nombre
        cell.check.boxType = M13Checkbox.BoxType.Square
        cell.check.stateChangeAnimation = M13Checkbox.Animation.Bounce(M13Checkbox.AnimationStyle.Fill)
        
        if category.check != false{
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        } else {
            cell.check.setCheckState(M13Checkbox.CheckState.Unchecked, animated: true)
        }
        
        if let user = self.user, sobreMi = user.sobre_mi{
            for item in sobreMi {
                if category.id == item{
                    self.categories[indexPath.row].check = true
                    cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
                }
            }
            
        }else if let _ = self.user{
            self.categories[indexPath.row].check = true
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let cell = collectionView.cellForItemAtIndexPath(indexPath) as! CategoryCollectionViewCell
        
        if cell.check.checkState == M13Checkbox.CheckState.Checked{
            self.categories[indexPath.row].check = false
            cell.check.setCheckState(M13Checkbox.CheckState.Unchecked, animated: true)
        } else {
            self.categories[indexPath.row].check = true
            cell.check.setCheckState(M13Checkbox.CheckState.Checked, animated: true)
        }
    }
}
