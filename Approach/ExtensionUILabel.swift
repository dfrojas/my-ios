//
//  ExtensionUILabel.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 30/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    
    func requiredHeight() -> CGFloat{
        let label = UILabel(frame: CGRectMake(0, 0, self.frame.width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = .ByWordWrapping
        label.font = self.font
        label.text = self.text
        label.sizeToFit()
        return label.frame.height
    }

}