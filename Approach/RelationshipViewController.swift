//
//  RelationshipViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 18/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SwiftGifOrigin
import Koloda

class RelationshipViewController: UIViewController{
    
    // -------------------------------------------------------
    // VARIABLES
    // -------------------------------------------------------
    
    @IBOutlet weak var imgUnlike    : UIImageView!
    @IBOutlet weak var imgReload    : UIImageView!
    @IBOutlet weak var imgLike      : UIImageView!
    @IBOutlet weak var kolodaView   : KolodaView!
    @IBOutlet weak var viewModal    : UIView!
    @IBOutlet weak var imgGift      : UIImageView!
    @IBOutlet weak var labModal     : UILabel!
    @IBOutlet weak var viewButtons  : UIView!
    @IBOutlet weak var indicator    : UIActivityIndicatorView!
    @IBOutlet weak var viewConfirm  : UIView!
    @IBOutlet weak var btnAcept: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var labelMessage: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelEmpy: UILabel!
    
    var users:[Person] = []
    var itemSelected:Person?
    var auxItemSelected:Person?
    var itemAux:Person?
    var idEvent = Commonds.getIdEvent()
    var userLogin = Commonds.getUser()
    var persons:ListPerson!
    var stateLike = false
    var stateUnlike = false
    var actionUpdate = true
    
    override func viewDidLoad() {
        kolodaView.dataSource = self
        kolodaView.delegate = self
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.updateFalse),name: "update-false", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.updateTrue),name: "update-true", object: nil)
        
        
        viewConfirm.layer.shadowColor = UIColor.blackColor().CGColor
        viewConfirm.layer.shadowOffset = CGSizeZero
        viewConfirm.layer.shadowOpacity = 0.5
        viewConfirm.layer.shadowRadius = 5
        viewConfirm.layer.cornerRadius = 10.0
        self.addGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        rotated()
        
        stateLike = false
        stateUnlike = false
        
        self.viewConfirm.hidden = true
        self.labelTitle.text = "Se terminó"
        self.labelMessage.text = "Para conocer mas contactos, amplia tus categorias ¿Deseas hacerlo?"
        
        /*if let item = self.itemSelected {
            if let index = users.indexOf({$0.id == item.id}) where index + 1 == users.count{
                self.viewButtons.hidden = true
                self.persons = ListPerson()
                self.reloadUsers()
                
            }
        }else if users.count == 0 {
            self.viewButtons.hidden = true
            self.persons = ListPerson()
            self.reloadUsers()
        }*/
        
        if actionUpdate == true {
            self.viewButtons.hidden = true
            self.persons = ListPerson()
            
            print("el estado es: \(actionUpdate)")
            
            if !self.users.isEmpty {
                self.users = []
                self.kolodaView.reloadData()
            }
            
            self.reloadUsers()
            
            //NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector: #selector(self.reloadUsers), userInfo: nil, repeats: false)
            
            
        }
        
    }
    
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        let orientation: UIInterfaceOrientationMask =
            [UIInterfaceOrientationMask.Portrait, UIInterfaceOrientationMask.PortraitUpsideDown]
        return orientation
    }
    
    // -------------------------------------------------------
    // METODOS
    // -------------------------------------------------------
    
    func updateFalse(){
        print("entro false")
        self.actionUpdate = false
    }
    
    func updateTrue(){
        print("entro true")
        self.actionUpdate = true
    }
    
    func rotated(){
        if(UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation)){
            UIDevice.currentDevice().setValue(UIInterfaceOrientation.Portrait.rawValue, forKey: "orientation")
        }
    }
    
    
    func reloadUsers(){
        self.labelEmpy.hidden = true
        self.indicator.startAnimating()
        FactoryRelationship.getUsers({ (targets) in
            
            let users = targets
            
            FactoryRelationship.getMaches(self.idEvent, idUsuario: (self.userLogin?.id!)!, successBlock: { (result) in
                
                var getPersons = self.persons.getPersonUser()
                
                if getPersons.isEmpty {
                    for (pUser) in users{
                        pUser.idEvento = self.idEvent
                        pUser.idUsuario = self.userLogin?.id
                        
                        if let _ = result.indexOf({pUser.id!}()){
                            pUser.isFriend = true
                            pUser.isView = true
                            pUser.like = true
                        }else {
                            pUser.isFriend = false
                            pUser.isView = false
                            pUser.like = false
                        }
                        
                        self.persons.addItem2(pUser)
                    }
                }else {
                    
                    for (person) in users{
                        
                        var existe = false
                        for pPerson in getPersons{
                            if pPerson.id == person.id && self.userLogin?.id != person.id{
                                
                                if let _ = result.indexOf({person.id!}()){
                                    person.isFriend = true
                                    person.isView = true
                                    person.like = true
                                }else {
                                    person.isView       = true
                                    person.isFriend     = pPerson.isFriend
                                    person.like         = pPerson.like
                                }
                                
                                if self.persons.updateItem(person) != true {
                                    self.persons.addItem2(person)
                                }
                                existe = true
                                break
                            }
                        }
                        
                        if existe == false {
                            
                            person.idEvento = self.idEvent
                            person.idUsuario = self.userLogin?.id
                            person.isFriend = false
                            person.isView = false
                            person.like = false
                            self.persons.addItem2(person)
                        }
                    }
                }
                
                var nPersons:[Person] = []
                
                getPersons = self.persons.getPersonUser()
                
                for item in getPersons{
                    if item.like != true && item.isFriend != true{
                        nPersons.append(item)
                    }
                }
                
                self.users = nPersons
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    if self.users.isEmpty{
                        self.viewButtons.hidden = true
                        self.labelTitle.text = "Lo sentimos"
                        self.labelMessage.text = "No encontramos personas compatibles por el momento, te invitamos a ampliar tus categorias ¿Deseas hacerlo?"
                        self.showConfirmModal()
                    }else {
                        self.viewButtons.hidden = false
                        self.kolodaView.reloadData()
                    }
                })
                
                }, andFailure: { (error) in
                    
                    var getPersons = self.persons.getPersonUser()
                    
                    if getPersons.isEmpty {

                        for (pUser) in users{
                            pUser.idEvento = self.idEvent
                            pUser.idUsuario = self.userLogin?.id
                            pUser.isFriend = false
                            pUser.isView = false
                            pUser.like = false
                            self.persons.addItem2(pUser)
                        }
                    }else {
                        
                        for (person) in users{
                            var existe = false
                            for pPerson in getPersons{
                                if pPerson.id == person.id && self.userLogin?.id != person.id{
                                    person.isView       = true
                                    person.isFriend     = pPerson.isFriend
                                    person.like         = pPerson.like
                                    self.persons.updateItem(person)
                                    existe = true
                                    
                                    break
                                }
                            }
                            
                            if existe == false {
                                person.idEvento = self.idEvent
                                person.idUsuario = self.userLogin?.id
                                person.isFriend = false
                                person.isView = false
                                person.like = false
                                self.persons.addItem2(person)
                            }
                        }
                    }
                    
                    var nPersons:[Person] = []
                    
                    getPersons = self.persons.getPersonUser()
                    
                    for item in getPersons{
                        if item.like != true && item.isFriend != true{
                            nPersons.append(item)
                        }
                    }
                    
                    self.users = nPersons
                    dispatch_async(dispatch_get_main_queue(), { () -> Void in
                        if self.users.isEmpty{
                            self.viewButtons.hidden = true
                            self.labelTitle.text = "Lo sentimos"
                            self.labelMessage.text = "No encontramos personas compatibles por el momento, te invitamos a ampliar tus categorias ¿Deseas hacerlo?"
                            self.showConfirmModal()
                        }else {
                            self.viewButtons.hidden = false
                            self.kolodaView.reloadData()
                        }
                    })
            })
            self.indicator.stopAnimating()
            
            
        }) { (msj) in
            self.indicator.stopAnimating()
            Commonds.showMessage(msj!, title: "Lo sentimos", content: self)
        }
    }
    
    func addGesture(){
        let tapGestureRecognizerSF = UITapGestureRecognizer()
        tapGestureRecognizerSF.numberOfTapsRequired       = 1
        tapGestureRecognizerSF.numberOfTouchesRequired    = 1
        tapGestureRecognizerSF.addTarget(self, action: #selector(self.btnLike))
        self.imgLike.userInteractionEnabled = true
        self.imgLike.addGestureRecognizer(tapGestureRecognizerSF)
        
        let tapGestureRecognizer = UITapGestureRecognizer()
        tapGestureRecognizer.numberOfTapsRequired       = 1
        tapGestureRecognizer.numberOfTouchesRequired    = 1
        tapGestureRecognizer.addTarget(self, action: #selector(self.btnUnlike))
        self.imgUnlike.userInteractionEnabled = true
        self.imgUnlike.addGestureRecognizer(tapGestureRecognizer)
        
        let tapGestureRecognizerR = UITapGestureRecognizer()
        tapGestureRecognizerR.numberOfTapsRequired       = 1
        tapGestureRecognizerR.numberOfTouchesRequired    = 1
        tapGestureRecognizerR.addTarget(self, action: #selector(self.reload))
        self.imgReload.userInteractionEnabled = true
        self.imgReload.addGestureRecognizer(tapGestureRecognizerR)
    }
    
    func btnLike() {
        if self.stateLike == false{
            kolodaView?.swipe(SwipeResultDirection.Right)
            self.stateLike = !self.stateLike
        }
        
        if let item = itemSelected{
            self.stateLike = !self.stateLike
            self.resultSwipe(item)
            if let index = self.users.indexOf({$0.id == item.id}) where index + 1 == self.users.count{
                self.viewButtons.hidden = true
                showConfirmModal()
            }
        }
        
        self.itemSelected = nil
        
    }
    
    func btnUnlike() {
        if self.stateUnlike == false{
            kolodaView?.swipe(SwipeResultDirection.Left)
            self.stateUnlike = !self.stateUnlike
        }
        
        if let item = itemSelected {
            auxItemSelected = item
            self.stateUnlike = !self.stateUnlike
            item.isView = true
            item.isFriend = false
            item.like = false
            persons.updateItem(item)
            if let index = self.users.indexOf({$0.id == item.id}) where index + 1 == self.users.count{
                self.viewButtons.hidden = true
                showConfirmModal()
            }
        }
        
        self.itemSelected = nil
        
    }
    
    func unLike() {
        if let item = itemSelected {
            auxItemSelected = item
            item.isView = true
            item.isFriend = false
            self.persons.updateItem(item)
            item.like = false
            persons.updateItem(item)
            if let index = self.users.indexOf({$0.id == item.id}) where index + 1 == self.users.count{
                self.viewButtons.hidden = true
                showConfirmModal()
            }
        }
    }
    
    func showConfirmModal(){
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.itemSelected = nil
            self.users = []
            self.kolodaView.reloadData()
        })
        
        self.viewConfirm.transform = CGAffineTransformMakeScale(1.2, 1.2)
        self.viewConfirm.alpha = 0
        self.viewConfirm.hidden = false
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.viewConfirm.alpha = 1;
            self.viewConfirm.transform = CGAffineTransformMakeScale(1, 1);
            }, completion: nil)
    }
    
    func actionCloseConfirmModal(){
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.viewConfirm.transform = CGAffineTransformMakeScale(1.2, 1.2);
            self.viewConfirm.alpha = 0.0;
            }, completion: {
                (value: Bool) in
                self.viewConfirm.hidden = true
                self.labelEmpy.hidden = false
        })
    }
    
    func reload() {
        if !users.isEmpty && itemSelected?.like != true{
            kolodaView?.revertAction()
            if let aux = auxItemSelected {
                itemSelected = aux
            }
        }
    }
    
    func resultSwipe(person: Person){
        
        let lperson = ListPerson()
        
        FactoryRelationship.swipe((person.id)!, success: { (resp) in
            if resp == true {
                person.isView      = true
                person.isFriend    = true
                person.like        = true
                lperson.updateItem(person)
                self.itemSelected = person
                
                if let names = person.nombres, lastname = person.apellidos{
                    self.labModal.text = "Ahora \(names) \(lastname) y tu pueden vincularse"
                }
                self.itemAux = person
                self.showModal()
            }else {
                person.isView      = true
                person.like        = true
                lperson.updateItem(person)
                self.itemSelected = person
            }
            }, callError: { (error) in
                Commonds.showMessage(error, title: "Lo sentimos", content: self)
        })
    }
    
    func showModal(){
        self.viewModal.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.viewModal.alpha = 0
        self.viewModal.hidden = false
        self.imgGift.contentMode = .ScaleAspectFill
        if let jeremyGif = UIImage.gifWithName("animation"){
            self.imgGift.image = jeremyGif
        }
        
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.navigationController?.navigationBarHidden = true
            self.viewModal.alpha = 1;
            self.viewModal.transform = CGAffineTransformMakeScale(1, 1);
            }, completion: nil)
    }
    
    func openProfile(){
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("Perfil") as! UINavigationController
            rootVC.view.frame = UIScreen.mainScreen().bounds
            appDelegate.window?.rootViewController = rootVC
        })
    }
    
    func closeModal(){
        self.navigationController?.navigationBarHidden = false
        UIView.animateWithDuration(0.25, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.imgGift.image = nil
            self.viewModal.transform = CGAffineTransformMakeScale(2.3, 2.3);
            self.viewModal.alpha = 0.0;
            }, completion: {
                (value: Bool) in
                
                self.viewModal.hidden = true
        })
    }
    
    // -------------------------------------------------------
    // MARK: ibaction
    // -------------------------------------------------------
    
    @IBAction func btnConfirmAcept(sender: UIButton) {
        self.performSegueWithIdentifier("sw_complete_relation", sender: self)
    }
    
    @IBAction func btnConfirmCancel(sender: UIButton) {
        actionCloseConfirmModal()
    }
    
    @IBAction func actionCloseModal(sender: UIButton) {
        closeModal()
    }
    
    @IBAction func actionOpenChat(sender: UIButton) {
        
        if let friend = self.itemAux{
            closeModal()
            let rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("chatLog") as! UINavigationController
            rootVC.view.frame = UIScreen.mainScreen().bounds
            
                let targetController = rootVC.topViewController as! ChatLogController
                friend.messages = []
                targetController.friend = friend
                self.presentViewController(rootVC, animated: false, completion: nil)
                itemAux = nil
        }
        
    }
    
    @IBAction func actionMenuChats(sender: AnyObject) {
        self.reloadUsers()
    }
    
    @IBAction func menuAction(sender: UIBarButtonItem) {
        self.slideMenuController()?.openLeft()
    }
}

//MARK: KolodaViewDelegate
extension RelationshipViewController: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(koloda: KolodaView) {

    }
    
    func koloda(koloda: KolodaView, didSelectCardAtIndex index: UInt) {
        
    }
}

//MARK: KolodaViewDataSource
extension RelationshipViewController: KolodaViewDataSource {
    
    func kolodaNumberOfCards(koloda:KolodaView) -> UInt {
        return UInt(self.users.count)
    }
    
    func koloda(koloda: KolodaView, viewForCardAtIndex index: UInt) -> UIView {
        let view = NSBundle.mainBundle().loadNibNamed("View",owner: self, options: nil).first as? MyTinderCell
        view?.user = self.users[(Int(index))]
        view?.frame = self.kolodaView.frame
        setupLayerAttributes(view!)
        return view!
    }
    
    private func setupLayerAttributes(cell: AnyObject) {
        cell.layer.borderWidth = 2.0
        cell.layer.borderColor = UIColor.clearColor().CGColor
        cell.layer.shadowColor = UIColor.darkGrayColor().CGColor
        cell.layer.shadowOffset = CGSize(width: 0.0, height: 5.0)
        cell.layer.shadowOpacity = 0.5
        cell.layer.masksToBounds = false
    }
    
    func koloda(koloda: KolodaView, didSwipeCardAtIndex index: UInt, inDirection direction: SwipeResultDirection){
        self.itemSelected = self.users[(Int(index))]
        switch direction {
        case .Right:
            self.stateLike = true
            self.btnLike()
        case .Left:
            self.stateUnlike = true
            self.btnUnlike()
        case .TopRight:
            self.stateLike = true
            self.btnLike()
        case .TopLeft:
            self.stateUnlike = true
            self.btnUnlike()
        case .BottomRight:
            self.stateLike = true
            self.btnLike()
        case .BottomLeft:
            self.stateUnlike = true
            self.btnUnlike()
        default:
            break
        }
        
    }
    
    func koloda(koloda: KolodaView, viewForCardOverlayAtIndex index: UInt) -> OverlayView? {
        let view = NSBundle.mainBundle().loadNibNamed("View",owner: self, options: nil).first as? MyTinderCell
        return view
    }
}