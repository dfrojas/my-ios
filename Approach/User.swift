//
//  User.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 10/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class User: NSObject, NSCoding{
    
    static let fileUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("login.obj")
    
    var id                   :Int?
    var evento               :Int?
    var nombres              :String?
    var apellidos            :String?
    var foto                 :String?
    var email                :String?
    var fcm_tok              :String?
    var empresa              :String?
    var clave                :String?
    var logo                 :String?
    var cargo                :String?
    var giro                 :String?
    var telefono             :String?
    var linkedin             :String?
    var telefono_2           :String?
    var descripcion_servicios:String?
    var intereses            :[Int]?
    var sobre_mi             :[Int]?
    var userLogin            :User?
    
    override init(){
        super.init()
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init()
        
        if let cnombres = aDecoder.decodeObjectForKey("nombres") as? String{
            self.nombres = cnombres
        }
        if let capellidos = aDecoder.decodeObjectForKey("apellidos") as? String {
            self.apellidos = capellidos
        }
        if let cfoto = aDecoder.decodeObjectForKey("foto") as? String {
            self.foto = cfoto
        }
        if let cid = aDecoder.decodeObjectForKey("id") as? Int where cid > 0 {
            self.id = cid
        }
        if let cid = aDecoder.decodeObjectForKey("evento") as? Int where cid > 0 {
            self.evento = cid
        }
        if let cEmail = aDecoder.decodeObjectForKey("email") as? String{
            self.email = cEmail
        }
        if let cfcmTok = aDecoder.decodeObjectForKey("fcm_tok") as? String{
            self.fcm_tok = cfcmTok
        }
        if let cempresa = aDecoder.decodeObjectForKey("empresa") as? String{
            self.empresa = cempresa
        }
        if let cclave = aDecoder.decodeObjectForKey("clave") as? String{
            self.clave = cclave
        }
        if let clogo = aDecoder.decodeObjectForKey("logo") as? String{
            self.logo = clogo
        }
        if let ccargo = aDecoder.decodeObjectForKey("cargo") as? String{
            self.cargo = ccargo
        }
        if let cgiro = aDecoder.decodeObjectForKey("giro") as? String{
            self.giro = cgiro
        }
        if let ctelefono = aDecoder.decodeObjectForKey("telefono") as? String{
            self.telefono = ctelefono
        }
        if let ctelefono = aDecoder.decodeObjectForKey("telefono_2") as? String{
            self.telefono_2 = ctelefono
        }
        if let ctelefono = aDecoder.decodeObjectForKey("linkedin") as? String{
            self.linkedin = ctelefono
        }
        if let cdescripcion_servicios = aDecoder.decodeObjectForKey("descripcion_servicios") as? String{
            self.descripcion_servicios = cdescripcion_servicios
        }
        if let cdescripcion_servicios = aDecoder.decodeObjectForKey("intereses") as? NSArray{
            self.intereses = cdescripcion_servicios as? [Int]
        }
        if let cdescripcion_servicios = aDecoder.decodeObjectForKey("sobre_mi") as? NSArray{
            self.sobre_mi = cdescripcion_servicios as? [Int]
        }
        
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        
        if let cnombres = self.nombres{
            aCoder.encodeObject(cnombres, forKey: "nombres")
        }
        if let capellidos = self.apellidos{
            aCoder.encodeObject(capellidos, forKey: "apellidos")
        }
        if let cfoto = self.foto{
            aCoder.encodeObject(cfoto, forKey: "foto")
        }
        if let cid = self.id{
            aCoder.encodeObject(cid, forKey: "id")
        }
        if let cid = self.evento{
            aCoder.encodeObject(cid, forKey: "evento")
        }
        if let cEmail = self.email{
            aCoder.encodeObject(cEmail, forKey: "email")
        }
        if let cfcmTok = self.fcm_tok{
            aCoder.encodeObject(cfcmTok, forKey: "fcm_tok")
        }
        if let cempresa = self.empresa{
            aCoder.encodeObject(cempresa, forKey: "empresa")
        }
        if let cclave = self.clave{
            aCoder.encodeObject(cclave, forKey: "clave")
        }
        if let clogo = self.logo{
            aCoder.encodeObject(clogo, forKey: "logo")
        }
        if let ccargo = self.cargo{
            aCoder.encodeObject(ccargo, forKey: "cargo")
        }
        if let cgiro = self.giro{
            aCoder.encodeObject(cgiro, forKey: "giro")
        }
        if let ctelefono = self.telefono{
            aCoder.encodeObject(ctelefono, forKey: "telefono")
        }
        if let ctelefono = self.telefono_2{
            aCoder.encodeObject(ctelefono, forKey: "telefono_2")
        }
        if let ctelefono = self.linkedin{
            aCoder.encodeObject(ctelefono, forKey: "linkedin")
        }
        if let cdescripcion_servicios = self.descripcion_servicios{
            aCoder.encodeObject(cdescripcion_servicios, forKey: "descripcion_servicios")
        }
        
        if let cdescripcion_servicios = self.sobre_mi{
            aCoder.encodeObject(cdescripcion_servicios, forKey: "sobre_mi")
        }
        
        if let cdescripcion_servicios = self.intereses{
            aCoder.encodeObject(cdescripcion_servicios, forKey: "intereses")
        }
    }
}

extension User{
    
    func getUser(item:Dictionary<String,AnyObject>) -> User {
        self.id                    = item["id"] as? Int
        self.evento                = item["evento"] as? Int
        self.nombres               = item["nombres"] as? String
        self.apellidos             = item["apellidos"] as? String
        self.foto                  = item["foto"] as? String
        self.email                 = item["email"] as? String
        self.empresa               = item["empresa"] as? String
        self.clave                 = item["clave"] as? String
        self.logo                  = item["logo"] as? String
        self.cargo                 = item["cargo"] as? String
        self.giro                  = item["giro"] as? String
        self.telefono              = item["telefono"] as? String
        self.telefono_2            = item["telefono_2"] as? String
        self.linkedin              = item["linkedin"] as? String
        self.descripcion_servicios = item["descripcion_servicios"] as? String
        self.fcm_tok               = item["fcm_tok"] as? String
        self.intereses             = item["intereses"] as? [Int]
        self.sobre_mi              = item["sobre_mi"] as? [Int]
        self.userLogin = self
        
        persistenceItems()
        return self
    }
    
    func saveU() {
        self.userLogin = self;
        persistenceItems()
    }
    
    func persistenceItems()  {
        let vUser = self.userLogin! as NSObject
        NSKeyedArchiver.archiveRootObject(vUser, toFile: User.fileUrl.path!)
    }
}
