//
//  ListAgenda.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 9/08/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation

class ListAgenda: NSObject{
    
    // ------------------------------------------------------------------
    // CONSTANTES
    // ------------------------------------------------------------------
    
    private var agenda:Agenda?
    
    private let fileUrl = NSURL(fileURLWithPath: NSTemporaryDirectory()).URLByAppendingPathComponent("AgendaList.plist")
    
    // ------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------
    
    var items:[Agenda] = []
    var idEvent = Commonds.getIdEvent()
    var userLogin = Commonds.getUser()
    
    // ------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------
    
    override init() {
        super.init()
        self.loadItemsOnStart()
    }
    
    // ------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------
    
    // agregr item
    func addItem(pitem:Agenda){
        self.loadItemsOnStart()
        
        if let index = self.items.indexOf({$0.evento == pitem.evento && pitem.id == $0.id && $0.idUsiario == pitem.idUsiario}){
            pitem.isFavorite = self.items[index].isFavorite
            self.items[index] = pitem
            persistenceItems()
        } else {
            items.append(pitem)
            persistenceItems()
        }
    }
    
    func persistenceItems()  {
        let itemsArray = self.items as NSArray
        NSKeyedArchiver.archiveRootObject(itemsArray, toFile: self.fileUrl.path!)
    }
    
    func getItems() ->[Agenda]{
        return self.items
    }
    
    func getAgendaEvent() -> [Agenda]{
        
        return self.items.filter{ $0.evento! == idEvent && $0.idUsiario == userLogin?.id}
    }
    
    // Cargar items al iniciar
    func loadItemsOnStart() {
        if let itemsArray = NSKeyedUnarchiver.unarchiveObjectWithFile(self.fileUrl.path!) {
            self.items = itemsArray as! [Agenda]
            
        }
    }
    
    // Dar item
    func getItem(index:Int) -> Agenda {
        return items[index]
    }
    
    
    func saveList(itemsAg:[Dictionary<String,AnyObject>]) -> [Agenda]{
        
        var result: [Agenda] = []
        
        for item in itemsAg {
            self.agenda          = Agenda()
            agenda?.id           = item["id"] as? Int
            agenda?.fecha        = item["fecha"] as? String
            agenda?.hora_inicio  = item["hora_inicio"] as? String
            agenda?.hora_fin     = item["hora_fin"] as? String
            agenda?.titulo       = item["titulo"] as? String
            agenda?.descripcion  = item["descripcion"] as? String
            agenda?.salon        = item["salon"] as? String
            agenda?.link_encuesta = item["link_encuesta"] as? String
            agenda?.evento       = self.idEvent
            agenda?.idUsiario    = userLogin?.id
            agenda?.isFavorite   = false
            
            if let array = item["nombre_conferencistas"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
                var nSpeaker = Speaker()
                agenda?.speakers = nSpeaker.addItems(pSpeakers)
            }
            self.addItem(agenda!)
            result.append(agenda!)
        }
        
        return result
    }
    
    func saveItem(item:Dictionary<String,AnyObject>) -> Agenda{
        self.agenda          = Agenda()
        agenda?.id           = item["id"] as? Int
        agenda?.fecha        = item["fecha"] as? String
        agenda?.hora_inicio  = item["hora_inicio"] as? String
        agenda?.hora_fin     = item["hora_fin"] as? String
        agenda?.titulo       = item["titulo"] as? String
        agenda?.descripcion  = item["descripcion"] as? String
        agenda?.salon        = item["salon"] as? String
        agenda?.link_encuesta = item["link_encuesta"] as? String
        agenda?.evento       = item["evento"] as? Int
        agenda?.idUsiario    = userLogin?.id
        agenda?.isFavorite   = false
        
        if let array = item["nombre_conferencistas"], pSpeakers = array as? [Dictionary<String,AnyObject>]{
            var nSpeaker = Speaker()
            agenda?.speakers = nSpeaker.addItems(pSpeakers)
        }
        self.addItem(agenda!)
        return self.agenda!
    }
    
    
    func getFavorites() -> [Agenda]{
        
        var result:[Agenda] = []
        
        for item in items {
            if item.evento! == idEvent && item.isFavorite == true && item.idUsiario == userLogin!.id {
                result.append(item)
            }
        }
        return result
    }
    
    func getAgenda(id: Int) -> Agenda?{
        for item in items {
            if item.id! == id && item.idUsiario == userLogin!.id && item.evento! == idEvent{
                return item
            }
        }
        return nil
    }
    
    func updateItem(person:Agenda) {
        self.loadItemsOnStart()
        if let index = self.items.indexOf({$0.id == person.id && $0.evento == person.evento && $0.idUsiario == person.idUsiario}){
            self.items[index] = person
            persistenceItems()
        }
    }
}
