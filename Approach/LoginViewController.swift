//
//  LoginViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 11/06/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit
import SlideMenuControllerSwift

class LoginViewController: UIViewController {
    
    var window: UIWindow?
    
    var event:Event?
    var statePerfil:Bool?
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtClave: UITextField!
    @IBOutlet weak var indicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBarHidden = false
        self.indicator.stopAnimating()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginAction(sender: UIButton) {
        if let pass = txtClave.text, email = txtEmail.text where pass.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != "" && email.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) != ""{
            
            self.indicator.startAnimating()
            var txt = "email=\(email)&password=\(pass)&evento=\(Commonds.getIdEvent())&platform=ios&fcm_tok="
            
            if let fcm = Commonds.getToken() {
                txt = txt + fcm
            }
            
            login(txt, state: statePerfil)
        }else {
            Commonds.showMessage("Por favor ingresa todos los campos", title: "Espera un momento!", content: self)
        }
    }
    
    func login(txt:String, state: Bool?){
        FactoryUser.login(txt, successBlock: { (user) in
            
            self.txtClave.resignFirstResponder()
            self.txtEmail.resignFirstResponder()
            
            self.indicator.stopAnimating()
            
            if let v = state where v == true{
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.performSegueWithIdentifier("sw_complete_p1", sender: self)
                })
            }else{
                self.chageHome()
            }
            
            }, andFailure: { (error) in
                self.indicator.stopAnimating()
                Commonds.showMessage("El email y/o contraseña son incorrectos", title: "Lo sentimos", content: self)
        })
    }
    
    func chageHome()  {
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let mainViewController = storyboard.instantiateViewControllerWithIdentifier("HomeNavigationController") as! UINavigationController
            
            mainViewController.view.frame = UIScreen.mainScreen().bounds
            let leftViewController = storyboard.instantiateViewControllerWithIdentifier("LeftMenu") as! UINavigationController
            let rightViewController = storyboard.instantiateViewControllerWithIdentifier("RightMenu") as! UINavigationController
            let slideMenuController = SlideMenuController(mainViewController: mainViewController, leftMenuViewController: leftViewController, rightMenuViewController: rightViewController)
            
            SlideMenuOptions.contentViewScale = 1
            //SlideMenuOptions.hideStatusBar = false;
            appDelegate.window?.rootViewController = slideMenuController
        })
    }
    
    /*
    // ------------------------------------------------------------------------------------
    // MARK: - Navigation
    // ------------------------------------------------------------------------------------
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let homeViewController = segue.destinationViewController as? HomeViewController{
            homeViewController.event = self.event
        }
    }*/

}
