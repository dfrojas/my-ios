//
//  PerfilChatViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class PerfilChatViewController: UIViewController {

    let transition = PopAnimator()
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var info: UILabel!
    @IBOutlet weak var giroL: UILabel!
    @IBOutlet weak var company: UILabel!
    
    @IBOutlet weak var cargo: UILabel!
    @IBOutlet weak var names: UILabel!
    
    @IBOutlet weak var companyFirst: UILabel!
    
    
    internal var imageP:UIImage?
    internal var friend:Person?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Perfil"
        
        if let user = self.friend {
            if let company = user.empresa {
                self.company.text = company
                self.companyFirst.text = company
            }
            
            if let info = user.descripcion_servicios {
                self.info.text = info
            }
            
            if let giro = user.giro {
                self.giroL.text = giro
            }
            
            
            
            if let cargo = user.cargo {
                self.cargo.text = cargo
            }
            
            if let name = user.nombres, lastname = user.apellidos {
                self.names.text = "\(name) \(lastname)"
            }
            
            if let tel2 = user.telefono_2{
                self.names.text = tel2
            }
            
            if let linkedin = user.linkedin{
                self.names.text = linkedin
            }
            
            profileImage.layer.cornerRadius = profileImage.frame.size.width / 2;
            profileImage.clipsToBounds = true;
            profileImage.contentMode = .ScaleAspectFill
            
            if let photo = user.foto {
                let trimmedString = photo.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                if !trimmedString.isEmpty{
                    self.profileImage.loadImageUsingCacheWithUrlString(photo)
                }else {
                    self.profileImage.image = UIImage(named: "profile_default")
                }
            }else {
                self.profileImage.image = UIImage(named: "profile_default")
            }
        }
        
        let tapGestureRecognizerPrev = UITapGestureRecognizer()
        tapGestureRecognizerPrev.numberOfTapsRequired       = 1
        tapGestureRecognizerPrev.numberOfTouchesRequired    = 1
        tapGestureRecognizerPrev.addTarget(self, action: #selector(self.openPreview))
        self.profileImage.userInteractionEnabled = true
        self.profileImage.addGestureRecognizer(tapGestureRecognizerPrev)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func openPreview(){
        
        if let img = self.profileImage.image {
            let imgPrev = storyboard!.instantiateViewControllerWithIdentifier("ImagePreviewViewController") as! ImagePreviewViewController
            imgPrev.image = img
            imgPrev.transitioningDelegate = self
            presentViewController(imgPrev, animated: true, completion: nil)
        }
        
    }

}

extension PerfilChatViewController: UIViewControllerTransitioningDelegate{
    func animationControllerForPresentedController(
        presented: UIViewController,
        presentingController presenting: UIViewController,
                             sourceController source: UIViewController) ->
        UIViewControllerAnimatedTransitioning? {
            
            transition.originFrame = profileImage!.superview!.convertRect(profileImage!.frame, toView: nil)
            transition.presenting = true
            
            return transition
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        transition.presenting = false
        return transition
    }
}
