//
//  ExtensionUITextView.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 30/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import Foundation
import UIKit

extension UITextView {
    
    func requiredHeight() -> CGFloat{
        let textView = UITextView(frame: CGRectMake(0, 0, self.frame.width, CGFloat.max))
        textView.font = self.font
        textView.text = self.text
        textView.sizeToFit()
        return textView.frame.height
    }
    
}