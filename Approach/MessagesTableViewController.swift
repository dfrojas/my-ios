//
//  MessagesTableViewController.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/07/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit

class MessagesTableViewController: UITableViewController{
    
    // ------------------------------------------------------------------------------------
    // VARIABLES
    // ------------------------------------------------------------------------------------
    private let idEvent = Commonds.getIdEvent()
    private var contacts:[Person] = []
    
    var resultSearchController    = UISearchController()
    var listMessages              = ListMessages()
    var persons:ListPerson?
    var itemSelected:Person?
    var filteredPerson:[Person] = []
    
    // ------------------------------------------------------------------------------------
    // CONSTRUCTOR
    // ------------------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.listMessages.loadItemsOnStart()
        
        if let nc = self.tabBarController?.viewControllers![1] as? UINavigationController, tab = nc.topViewController as? ContactTableViewController{
            tab.listMessage = self.listMessages
        }
        
        self.resultSearchController = UISearchController(searchResultsController: nil)
        self.resultSearchController.searchResultsUpdater = self
        self.resultSearchController.dimsBackgroundDuringPresentation = false
        self.resultSearchController.searchBar.sizeToFit()
        self.resultSearchController.hidesNavigationBarDuringPresentation = false
        self.resultSearchController.searchBar.placeholder = "Buscar"
        definesPresentationContext = true
        self.resultSearchController.searchBar.tintColor = UIColor.whiteColor()
        self.resultSearchController.searchBar.backgroundColor = UIColor(rgba: "#335F6D")
        self.resultSearchController.searchBar.translucent = false
        self.tableView.tableHeaderView = self.resultSearchController.searchBar
        loadContacts()
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        NSNotificationCenter.defaultCenter().postNotificationName("update-false", object: nil)
    }
    
    // ------------------------------------------------------------------------------------
    // METODOS
    // ------------------------------------------------------------------------------------
    
    func loadContacts() {
        
        self.persons = ListPerson()
        self.contacts = self.persons!.getContacts()
        var pContacts:[Person] = []
        
        
        for item in contacts {
            if !listMessages.getMessagesOfUsers(item.id!).isEmpty {
                pContacts.append(item)
            }
        }
        self.contacts = pContacts
    }

    // ------------------------------------------------------------------------------------
    // MARK: Table view data source
    // ------------------------------------------------------------------------------------
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if self.resultSearchController.active{
            itemSelected = self.filteredPerson[indexPath.row]
            self.resultSearchController.active = false
        }else {
            itemSelected = self.contacts[indexPath.row]
        }
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("sw_message", sender: self)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.resultSearchController.active{
            return filteredPerson.count
        }else {
            return contacts.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! MessageTableViewCell
        
        var entry = contacts[indexPath.row]
        cell.idEvent = self.idEvent
        cell.nMessages = self.listMessages
        
        if self.resultSearchController.active{
            entry = self.filteredPerson[indexPath.row]
        }
        cell.person = entry
        return cell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 75
    }
    
    // ------------------------------------------------------------------------------------
    // MARK: navigation
    // ------------------------------------------------------------------------------------

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let destination = segue.destinationViewController as! UINavigationController
        if let chat = destination.topViewController as? ChatLogController {
            if let person = self.itemSelected{
                person.messages = []
                chat.friend = person
            }
        }
        
    }

}

extension MessagesTableViewController: UISearchResultsUpdating{
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.filteredPerson = self.contacts.filter({ (exhibitor) -> Bool in
            let name = "\(exhibitor.nombres!) \(exhibitor.apellidos!)"
            let stringMatch = name.lowercaseString.rangeOfString(searchController.searchBar.text!.lowercaseString)
            return stringMatch != nil
        })
        
        self.tableView.reloadData()
    }
}
