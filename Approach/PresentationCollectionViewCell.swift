//
//  PresentationCollectionViewCell.swift
//  Approach
//
//  Created by Andres Felipe Lozano on 14/09/16.
//  Copyright © 2016 andrelo. All rights reserved.
//

import UIKit


class PresentationCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labTexto: UILabel!
    
    @IBOutlet weak var constraintHeightLabText: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let device = Device()
        switch device {
        case .Simulator(.iPhone4s):
            
            self.constraintHeightLabText.constant = 10
            break
        case .iPhone4s:
            self.constraintHeightLabText.constant = 10
            break
        default:
            self.constraintHeightLabText.constant = 30
            break
        }
    }
}
